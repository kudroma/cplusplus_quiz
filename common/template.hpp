#pragma once

namespace common {

	class Template
	{
	public:

		/** Variadic templates example. */
		static void variadic_template();

		/** Undefined behavior of template. */
		static void undefined_behavior_only_empty_specialization();

		/** Variadic template deduction order. */
		static void variadic_template_deduction_order();

		/** Order of template instantiation. */
		static void order_of_template_instantiation();

		/** SFINAE */
		static void sfinae_concept_20();
	};
}