#include "types.h"

#include <chrono>
#include <iostream>
#include <mutex>
#include <numeric>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include "memory_model.hpp"

using namespace std;
using namespace std::chrono;

std::mutex mutp;
template<typename ... T>
void print(T&& ... args) {
    std::lock_guard<std::mutex> lock(mutp); // for print
    ((cout << args << " "), ...);
    cout << endl;
}

/*
   const int& and int const& are the same.
*/
void common::MemoryModel::const_reference()
{
    int c = 5;
    const int& b = c;
    int const& s = c;

    print(c, b, s);
}

/**
* int var1 : 3; declares a bit-field, and you can not apply operator& to a bit-field.
* �12.2.4 in the C++ standard:
* The address-of operator & shall not be applied to a bit-field, so there are no pointers to bit-fields.
*/
void common::MemoryModel::address_cant_be_applied_to_bitfield()
{
    struct X {
        int var1 : 3;
        int var2;
    };
    X x;
    // std::cout << (&x.var1 < &x.var2); // uncomment here
}

/**
* Compare reference vs pointer.
* 1. Not possible to create array of refs.
* 2. There is no ref arithmetics.
* 3. Pointer can be dangling or have nullptr value.
* 4. No const qualifier for reference.
*/
void common::MemoryModel::ref_vs_pointer()
{
    int a[10] = { 1,2,3,4,5,6,7,8,9,10 };
    //int& a_ref[10] = a; // impossible
    int* a_p[10]; // array of pointers is ok

    int* a_p2 = &a[0];
    a_p2 += 1;
    print(*a_p2); // there is pointer arithmetics. Should print 2.

    int* p3;
    //print(*p3); // run-time error of invalid pointer.

    int b = 0;
    int& const b1 = b; // impossible
    int* const b2 = &b;
    int c = 0;
    (*b2)++; // It is possible to change value.
    //b2 = &c; // It is impossible to change pointer.
    const int* b3 = &b;
    //(*b3)++; // It is impossible to change content of the pointer.
    b3 = &c; // It is possible to change pointer itself.
}

/**
* Each operation a += 1 has actually 3 stages:
* 1. Load var from the memory to the register;
* 2. Increment it;
* 3. Store new value to the memory from the register.
* When multiple threads try to change it then data race occurs.
* You most probably won't take 30 000 000 in the example below.
*/
void common::MemoryModel::data_race()
{
    int a = 0; // shared variable
    auto thread_func = [&]() { for (auto i = 0; i < 10000000; ++i) a += 1; };
    thread t1(thread_func), t2(thread_func), t3(thread_func);
    t1.join();
    t2.join();
    t3.join();
    print(a);
}

/**
* a will be always 30 000 000.
* In x86_64 processors each triple of low-level operations will be done before any other such operations will be done from other threads.
*/
void common::MemoryModel::atomic_variables_11()
{
    atomic<int> a = 0; // shared variable
    auto thread_func = [&]() { for (auto i = 0; i < 10000000; ++i) a += 1; };
    thread t1(thread_func), t2(thread_func), t3(thread_func);
    t1.join();
    t2.join();
    t3.join();
    print(a);
}

/** 
* The differences are printed.
*/
void common::MemoryModel::volatile_vs_atomic()
{
    print("1",
        "Possible optimizations for atomic but not for volatile: ",
        "a=1; b=a; --> a=1; b=1   AND  a=10; a=20; --> a=20;"
    );

    print("2",
        "Atomic could limit operations reordering around.",
        "Volatile doesn't restrict. But all volatile operations must go in the order they declared."
    );

    print("3",
        "Atomic causes spilling: remove from processor register all other variables."
    );

    print("4",
        "Atomic vars are aligned to force it to be read by only one CPU operation. It is not so for volatile."
    );

    print("5",
        "Atomic operations have atomic RMW (read-modify-write) while volatile not."
    );

    print("6",
        "Each operation for atomic and volatile always go to the memory. These values are not cached."
    );

}

/** Operations are atomic: 
* ++, 
* --, 
* +=, 
* -=, 
* *=, 
* /=, 
* CAS, 
* exchange 
*/
void common::MemoryModel::atomic_operations()
{
    std::atomic<int> a = 1;
    int b = 1;
    auto increment = [&]() { for (auto i = 0; i < 10000000; ++i) { a++; b++; } };
    thread t1(increment), t2(increment), t3(increment);
    t1.join();
    t2.join();
    t3.join();
    print("++ a=", a, "b=", b, "expected=", 30000001);

    a = 1;
    b = 1;
    auto decrement = [&]() { for (auto i = 0; i < 10000000; ++i) { a--; b--; } };
    thread t4(decrement), t5(decrement), t6(decrement);
    t4.join();
    t5.join();
    t6.join();
    print("-- a=", a, "b=", b, "expected=", -29999999);

    a = 1;
    auto exchange = [&]() { for (auto i = 0; i < 10000000; ++i) { a = a.exchange(5); } };
    thread t7(exchange), t8(exchange), t9(exchange);
    t7.join();
    t8.join();
    t9.join();
    print("exchange a=", a, "expected=", 5);

    print("another operation is cas.");
}

/** 
* Bloking vs Non-Blocking algorithms
* The primary difference between blocking and non-blocking algorithms 
* is which threads are able to make progress in the code execution. 
* In a blocking model, a single thread is allowed to prevent all other threads from making any progress. 
* There is no guarantee that this single thread is making progress itself or that it will ever stop blocking other threads. 
* All locks are blocking, regardless of the implementation. 
* A thread that has taken a lock can prevent others from continuting for an extended period of time for a variety of reasons. 
* Some of these include being swapped out due to OS scheduling, experiencing a page fault, 
* or terminating (e.g. due to a crash) without releasing the lock. 
* On the other hand, a non-blocking algorithm guarantees that there exists a thread that is making progress. 
* However, nothing is specified about which thread makes progress, meaning that starvation is still possible.
* 
* Pros. and Cons. of Lock-free structures
* By guaranteeing that some thread will be making progress, 
* you don't have to worry about a single thread preventing the entire program from moving forward. 
* Moreover, the overhead associated with acquiring and releasing locks is eliminated--even when there is no contention, 
* locks still need to be taken and released. However, 
* lock-free code is generally more difficult to write or reason about its correctness. 
* Systems with relaxed memory consistency will also require the use of memory fences in the implementation.
* 
* As we saw in lecture, lockfree data structures don't necessarily show better performance, 
* despite the apparent benefits. Performance is particularly bad with a dequeue. 
* In cases where a CAS operation will fail many times before succeeding, 
* you end up doing a lot of extra busy waiting, similar to a spin lock. 
* For this reason, higher contention is less favorable for lock-free operations, 
* as evidenced by the insertions only examples in the lecture slides. 
* It is also interesting to note that in a linked list, 
* fine grain locks don't necessarily perform better than the pthread mutex lock. 
* In these cases, the overhead of acquiring/releasing locks is too large for small and large processor counts.
* 
* Lock free structures use special algorithms and atomic variables to avoid mutex locks.
* Mutex can be more effective for small amount of threads. 
* But it is crucial to use lock free structures with increase of threads amount.
*/
template <typename T>
class LockFreeStack {
private:
    struct Node {
        T data;
        Node* next;

        Node(const T& value) : data(value), next(nullptr) {}
    };

public:
    std::atomic<Node*> top;

    LockFreeStack() : top(nullptr) {}

    void push(const T& value) {
        while (true)
        {
            Node* newNode = new Node(value); // Create new node. It is done once per push() call from any thread.
            newNode->next = top.load(std::memory_order_relaxed); // set new node under current stack top.
            if (top.compare_exchange_weak(newNode->next, newNode, std::memory_order_relaxed)) {
                return;
            }
        }
    }

    bool pop() {
        while (true) {
            Node* currentTop = top.load(std::memory_order_relaxed); // Load current top.
            if (currentTop == nullptr) {
                return false; // stack is empty
            }
            auto* next = currentTop->next; // Unsafe according to the threads place.

            // If CAS was successive then we were able to change stack top.
            if (top.compare_exchange_weak(currentTop, next, std::memory_order_relaxed)) { 
                //delete currentTop; // There can be an error. There should be a mechanism to physically delete current Top from only one place.
                                     // We do not place it there.
                return true;
            }
        }
    }

    ~LockFreeStack() {
        while (top.load(std::memory_order_relaxed) != nullptr) {
            Node* currentTop = top.load(std::memory_order_relaxed);
            top.store(currentTop->next, std::memory_order_relaxed);
            delete currentTop;
        }
    }
};

template <typename T>
class LockStack {
private:
    struct Node {
        T data;
        Node* next;

        Node(const T& value) : data(value), next(nullptr) {}
    };

public:
    Node* top;
    std::mutex mutex;

    LockStack() : top(nullptr) {}

    void push(const T& value) {
        std::lock_guard<std::mutex> lock(mutex);
        Node* newNode = new Node(value); 
        newNode->next = top;
    }

    bool pop() {
        std::lock_guard<std::mutex> lock(mutex);
        Node* currentTop = top;
        if (currentTop == nullptr) {
            return false;
        }
        delete currentTop;
        return true;
    }

    ~LockStack() {
        std::lock_guard<std::mutex> lock(mutex);
        while (top != nullptr) {
            Node* currentTop = top;
            top = currentTop->next;
            delete currentTop;
        }
    }
};

void common::MemoryModel::lock_free_stack()
{
    for (auto k = 2; k < 30; ++k) {
        auto start = high_resolution_clock::now();

        LockFreeStack<float> stack;
        std::vector<std::thread> threads;
        for (auto i = 0; i < k; ++i) {
            if (i % 2) {
                threads.emplace_back([&]() {
                    for (auto i = 0; i < 1e6; ++i) {
                        stack.push(1.0);
                    }
                    });
            }
            else {
                threads.emplace_back([&]() {
                    while (stack.pop()) {}
                    });
            }
        }

        for (std::thread& thread : threads) {
            thread.join(); // Wait for each thread to complete
        }

        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<microseconds>(stop - start);
        print("Number of threads", k, "StackFreeLock: ", duration.count());

        start = high_resolution_clock::now();
        LockStack<float> stack2;
        std::vector<std::thread> threads2;
        for (auto i = 0; i < k; ++i) {
            if (i % 2) {
                threads2.emplace_back([&]() {
                    for (auto i = 0; i < 1e6; ++i) {
                        stack2.push(1.0);
                    }
                    });
            }
            else {
                threads2.emplace_back([&]() {
                    while (stack2.pop()) {}
                    });
            }
        }

        for (std::thread& thread : threads2) {
            thread.join(); // Wait for each thread to complete
        }

        stop = high_resolution_clock::now();
        duration = duration_cast<microseconds>(stop - start);
        print("Number of threads", k, "StackLock", duration.count());
    }
}

/** Implementation was given from there https://www.eecs.yorku.ca/~eruppert/papers/lfll.pdf 
* 
*/
template<typename T, typename V>
struct Node
{
    Node(T key, V val) : key(key), value(val) {}

    struct Succ
    {
        Succ() = default;
        Succ(Node<T,V>* right, bool mark, bool flag) : right(right), mark(mark), flag(flag) {}

        Node<T, V>* right = nullptr;
        
        // If true then Node is to be deleted.
        bool mark = false;

        // flag is true if the next node of the given node was logically removed
        // The flag is usefule in order to set state for the node when right is to be removed 
        // but not yet removed actually. If one thread is in process of removing next node it should
        // immediately atomical set this flag in order to notify other threads.
        bool flag = false;

        // Logically flag and mark are the same. If some node is to be deleted then its mark=true and flag of the predessor is also true.
    };
    T key;
    V value;
    Node<T, V>* backlink = nullptr;
    std::atomic<Succ> succ;
};

template <typename T, typename V>
using AtomicNode = std::atomic<Node<T, V>*>;

template<typename T, typename V> 
struct SortedList
{
    Node<T,V>* head = nullptr;
    Node<T, V>* noSuchKey = nullptr;

    std::mutex printm;

    SortedList() 
    {
        head = new Node<T, V>(std::numeric_limits<T>::min(), std::numeric_limits<T>::min());
        auto* last = new Node<T, V>(std::numeric_limits<T>::max(), std::numeric_limits<T>::max());
        typename Node<T, V>::Succ succ(last, false, false);
        head->succ.store(succ);
    }

    Node<T, V>* search(T k)
    {
        auto res= searchFrom(head.load(), k);
        prev = res.first;
        next = res.second;
        if (prev->key == k) {
            return prev;
        }
        else {
            return noSuchKey;
        }
    }

    std::pair< Node<T,V>*, Node<T,V>*> searchFrom(Node<T, V>* startNode, T k)
    {
        /** 
        * Searchs nodes n1 and n2 such taht n1.key <= k <= n2.key.
        */
        auto* curNode = startNode;
        auto curNodeSucc = curNode->succ.load();
        auto* nextNode = curNodeSucc.right;
        auto nextNodeSucc = nextNode->succ.load();
        while (nextNode->key <= k)
        {
            // Ensure that either next node is unmarked,
            // or both curr node and next node are
            // marked and curr node was marked earlier.
            // The next while cycle removes any marked nodes that it sees in the list.
            while (nextNodeSucc.mark && (!curNodeSucc.mark || curNodeSucc.right != nextNode)) // if condition after || satisfied then 
                                                                                     // curNode was logically removed since last 
                                                                                     // curNode = startNode;
            {
                if (curNodeSucc.right == nextNode)
                {
                    helpMarked(curNode, nextNode); // Change right member of cur node and set flag to false back
                }
                curNodeSucc = curNode->succ.load();
                nextNode = curNodeSucc.right;
                nextNodeSucc = nextNode->succ.load();
            }

            if (nextNode->key <= k)
            {
                curNode = nextNode;
                curNodeSucc = curNode->succ.load();
                nextNode = curNodeSucc.right;
                nextNodeSucc = nextNode->succ.load();
            }
        }
        return std::make_pair(curNode, nextNode);
    }

    /**
    * This function is to remove node from list by excluding it from pointers chain.
    * 
    * Changes right for the prev to the del->right.
    * Sets flag of the right to false.
    * Function succeeds only if flag is true.
    * It guaranties that only one thread will change the right node for prev.
    *
    * del node is to be removed.
    */
    void helpMarked(Node<T, V>* prev, Node<T, V>* del)
    {
        auto* next = del->succ.load().right;
        typename Node<T,V>::Succ newSucc = typename Node<T, V>::Succ( next, false, false );
        typename Node<T,V>::Succ oldSucc = typename Node<T, V>::Succ( del, false, true );
        prev->succ.compare_exchange_weak(oldSucc, newSucc); // set flag=false and right=next_node
    }

    /** 
    * This function tries to set mark flag for the deleted node and after this calls helpMarked() to actually exclude this node from the list.
    * It works recursively with tryMark() and performs excluding from the list several removed by different threads nodes.
    * 
    * 
    * Attemts to mark del node and physically remove it 
    * if his predessor prev was flagged.
    */
    void helpFlagged(Node<T, V>* prev, Node<T, V>* del)
    {
        del->backlink = prev; // Set backlink that can be used by other threads to achieve not-deleted node.
        if (!del->succ.load().mark)
        {
            tryMark(del);
        }
        helpMarked(prev, del);
    }

    /** 
    * Atomically sets mark flag for the given data structure. 
    * 
    * Attempts to mark the given node. It means that it should be removed.
    * The result of the function:
    *   del not will have as right first right Node that is not supposed to be removed.
    */
    void tryMark(Node<T, V>* del)
    {
        Node<T, V>* nextNode;
        do
        {
            nextNode = del->succ.load().right;
            auto oldSucc = typename Node<T, V>::Succ(nextNode, 0, 0);
            auto newSucc = typename Node<T, V>::Succ(nextNode, 1, 0);
            del->succ.compare_exchange_weak(oldSucc, newSucc);
            if( del->succ.load().flag )
            {
                // It means that node to be deleted was marked as flagged and some thread tries to remove next node after the given.
                // In this case we call helpFlagged() and recursively call tryMark() if some thread tried to remove further nodes.
                // In each helpFlagged() call in call stack we will call helpMarked() in the end that will guarantee that 
                // del will be connected directly with the first right node with mark=False.
                // After this function the right of the del Node will be first right Node that is not supposed to be deleted (mark=false).
                // helpFlagged also calls helpMarked that sets flag to false for del and we can continue on the next iteration successive CAS operations.
                helpFlagged(del, del->succ.load().right);
            }
        } while (!del->succ.load().mark);
    }

    /**
    * Inserts node to the list.
    */
    void insert(T key, V val)
    {
        auto res = searchFrom(head, key);
        auto* prev = res.first;
        auto* next = res.second;
        if (prev->key == key) {
            return; // Duplicate key
        }
        
        // Create new node.
        auto* newNode = new Node<T, V>(key, val);
        while (true) // try to insert
        {
            auto prevSucc = prev->succ.load();
            if (prevSucc.flag) {
                helpFlagged(prev, prevSucc.right); // Mark right to be deleted and connect prev with next node that is not to be deleted
                                                   // Continue while cycle after this and try to insert again.
                                                   // We will go to this if block again if some thread will initiate deletion of new right node but won't finish it by this time. 
            }
            else {
                // The next node is not to be removed.
                newNode->succ = typename Node<T, V>::Succ(next, false, false);
                typename Node<T, V>::Succ oldSucc = typename Node<T, V>::Succ(next, false, false);
                typename Node<T, V>::Succ newSucc = typename Node<T, V>::Succ(newNode, false, false);
                if (prev->succ.compare_exchange_weak(oldSucc, newSucc))
                    // If we did this successively then it guaranties that at the time of insertion:
                    //   - next was next node of the prev
                    //   - flag was false and it means that next node is not be removed
                    //   - mark flag was false and it means that prev is not to be removed
                    return; // Successive attemt to insert.
                else {
                    auto prevSucc = prev->succ.load();
                    if (prevSucc.flag) {
                        // It means that next node is to be deleted but is not deleted yet.
                        // In this case we should connect prev with first right node that is not to be removed.
                        helpFlagged(prev, prevSucc.right);
                    }
                    while (prev->succ.load().mark) {
                        // If prev node was marked as removed by another thread then we should find first left node
                        // that is not to be removed.
                        prev = prev->backlink;
                    }
                }
            }
            res = searchFrom(prev, key); // update prev and next node.
            prev = res.first;
            next = res.second;
        }
    }

    /** 
    * Sets flag to true for the previous node.
    */
    std::pair<Node<T,V>*,bool> tryFlag(Node<T,V>*  prev, Node<T,V>* target)
    {
        Node<T, V>* prev_node = prev;
        while (true) {
            typename Node<T, V>::Succ oldSucc = typename Node<T, V>::Succ(target, 0, 0);
            typename Node<T, V>::Succ newSucc = typename Node<T, V>::Succ(target, 0, 1);
            if (prev_node->succ.compare_exchange_weak(oldSucc, newSucc)) {

                // Prev node was already flagged.
                return std::make_pair(prev, true);
            }
            else {
                typename Node<T, V>::Succ succ2 = prev->succ.load();
                if (succ2.flag) {
                    // Other thread flagged the given node.
                    return std::make_pair(prev, false);
                }
                else {
                    // Node was marked and therefore to be deleted by other thread.
                    // In this case find first left node that is not to be deleted.
                    while (prev_node->succ.load().mark) {
                        prev_node = prev_node->backlink;
                    }
                }
            }
            auto res = searchFrom(prev_node, target->key - 0.001); // think about int. Should compare prev.
            prev_node = res.first;
            auto* next_node = res.second;
            if (next_node != target) {
                return std::make_pair(nullptr, false); // target was removed by other thread.
            }
        }
    }

    Node<T,V>* remove(T key)
    {
        auto res = searchFrom(head, key - 0.001); // Think about epsilon for different types (float with int)

        auto* prev = res.first;
        auto* del = res.second;
        if (del->key != key)
            return nullptr; // No such key in the list.

        auto res2 = tryFlag(prev, del);

        prev = res2.first;
        bool del_res = res2.second;
        if (prev != nullptr) {
            helpFlagged(prev, del); // Actually delete del from list and delete flag from the prev.
        }
        if (del_res) {
            return nullptr; // Key was already removed by other thread.
        }
        return del;
    }
};

/** 
* 
*/
void common::MemoryModel::lock_free_linked_list()
{
    SortedList<float,float> list;
    std::vector<std::thread> threads1;
    std::vector<std::thread> threads;

    // Insert odd numbers to the linked list using multiple threads.
    for (auto i = 0; i < 30; ++i) {
        threads1.emplace_back([&, i=i]() {
            // Insert odd numbers only.
            for (int k = i*1e3; k < (i+1)*1e3; ++k) {
                if( k % 2)      
                    list.insert(k, k);
            }
        });
    }
    for (auto& thread : threads1)
        thread.join();

    // Check that all numbers differ by 2.
    {
        auto* curNode = list.head->succ.load().right;
        int curVal = curNode->key;
        int prevVal;
        while (curNode->succ.load().right->key < 1e6) {
            //print(curNode->key);
            curNode = curNode->succ.load().right;
            if (curNode->key - curVal != 2)
                print("InsertError", "Prev node: ", curVal, "Cur node: ", curNode->key);
            curVal = curNode->key;
        }
    }

    // Remove all odd numbers but insert even numbers simulteneously.
    for (auto i = 0; i < 30; ++i) {
        threads.emplace_back([&, i=i]() {
            // Remove odd numbers only.
            for (int k = i*1e3; k < (i+1)*1e3; ++k) {
                if(k%2)      
                    list.remove(k);
            }
        });
    }
    for (auto i = 0; i < 30; ++i) {
        threads.emplace_back([&, i = i]() {
            // Insert even numbers only.
            for (int k = i * 1e3; k < (i + 1) * 1e3; ++k) {
                if (!(k%2))
                    list.insert(k, k);
            }
            });
    }
    for (auto& t : threads)
        t.join();

    // Check that only even numbers exist in a list.
    {
        auto* curNode = list.head->succ.load().right;
        int curVal = curNode->key;
        int prevVal;
        while (curNode->succ.load().right->key < 1e6) {
            //print(curNode->key);
            curNode = curNode->succ.load().right;
            if (curNode->key - curVal != 2)
                print("InsertDeleteError", "Prev node: ", curVal, "Cur node: ", curNode->key);
            curVal = curNode->key;
        }
    }

    print("The end: if no other messages where printed then all works fine.");
}

/**
* https://habr.com/ru/articles/328362/
* https://ru.cppreference.com/w/cpp/atomic/memory_order
* 
* 1. Compiler could apply optimizations and reorder instructions comparing with source code.
* 2. Memory barrier prevents reordering in one of directions:
* 
* std::memory_order (������������ ������� � ������) ����������, ��� �������, ����������� ������ � ������, ��������������� ������ ��������� ��������. ��� ���������� �����-���� �����������, �� ������������ ��������, ����� ��������� ������� ������������ ������ � ����� � ��������� ����������, ���� ����� ����� ��������� ��������� �������� ���������� � �������, ������������ �� ����, � ������� ������ ����� ���������� ��. �� ����� ����, ������� ������� ��������� ����� ���������� ���� ����� ���������� �������� �������.

������� ���������� ��������� ��������
    
    std::memory_order (������������ ������� � ������) ����������, ��� �������, ����������� ������ � ������, 
    ��������������� ������ ��������� ��������. ��� ���������� �����-���� �����������, 
    �� ������������ ��������, ����� ��������� ������� ������������ ������ � ����� � ��������� ����������, 
    ���� ����� ����� ��������� ��������� �������� ���������� � �������, 
    ������������ �� ����, � ������� ������ ����� ���������� ��. 
    �� ����� ����, ������� ������� ��������� ����� ���������� ���� ����� ���������� �������� �������.

    ��� ��������� �������� �� ��������� ����������� ��������������� 
    ��������������� ������������� ������������ (sequentially consistent ordering) (�� ���������� ����). 
    ����� ��������� ����� ��������� ��������������, �� ��������� ��������� ���������� ����� ���� 
    ������� �������������� std::memory_order ��������, ����� ������� ������ �����������, ������ �����������, 
    ������� ���������� � ��������� ������ ���������� ��� ���� ��������.




memory_order_relaxed	
    Relaxed operation: 
    there are no synchronization or ordering constraints imposed on other reads or writes, 
    only this operation's atomicity is guaranteed (see Relaxed ordering below).

memory_order_acquire	
        �������� �������� � ���� ������������� ������ ��������� �������� ������� (acquire) ��� ��������������� �������� ������: 
        ���������� ������, ��������� � ��������� �� ������ ������� ������ �������, ������� �������� ������������ (release), 
        ���������� �������� � ������ ������.

memory_order_release	
        �������� ���������� � ���� ������������� ������ ��������� �������� ������������ (release): 
        ���������� ������ � ������ ������� ������, ���������� �������� ��� �������, 
        ������� ��������� �������� ���������� (consume) ��� ������� (acquire) ��� ��� �� �������� ������.

memory_order_acq_rel	
        �������� �������� � ���� ������������� ������ ��������� �������� ������� (acquire) ��� ��������������� �������� ������. 
        �������� ���������� � ���� ������������� ������ ��������� �������� ������������ (release).

memory_order_seq_cst	
    (sequentially-consistent - ��������������� �������������) �� ��, ��� � memory_order_acq_rel, 
    ���� ���������� ������ ����� �������, ��� ������� ��� ������ ����� ��� ��������� (��. ����) � ���������� �������.


����������� �����
    � ����� � ��� �� ������, ���������� A �����������-����� ����������� B, ���� ��� ������� �� evaluation order.

������� ���������
    ��� ��������� ����� ����������� ��������� ���������� ���������� � ����� �������, ������� �������� ��� ���� ��������� ����������.

���������� ������
(Happens-before)
    � ������������� �� �������, ���������� A ����������-������ ���������� B, ���� ����������� ����� �� ��������� �����������:
        1) A �����������-����� B
        2) A ���������� ����������-������ B
    ���� ���� ���������� ������������ ������� ������, � ������ ������ ��� ������������ ��� �� ������� ������, 
    � ���� ���� �� ���� �� ���������� �� �������� ��������� ���������, ��������� ��������� �� ���������� 
    (� ��������� ������������ ����� �� �������) ����� �������, 
    ����� ���������� ��������� ����������-������ ����� ����� ����� ������������.


������������ ������������-������
    ���� ��������� ���������� � ������ A �������� ������������� std::memory_order_release 
    � ��������� �������� � ������ B �� ���� �� ���������� �������� ������������� std::memory_order_acquire, 
    ��� ������ ������ (�� ��������� � � ����������� �������������), ������� ����������-������ ��������� ������ � ����� ������ ������ A, 
    ���������� �������� ��������� ��������� � ������ B, �� ����, ����� ����, ��� ��������� �������� ���������, 
    ����� B �������������� ������ ��, ��� ����� A ������� � ������.

    ����� B ����� ���, ��� ���� ������� � A �� ���������� ��������� ����������.

    ������������� ��������������� ������ ����� ������������� � ������������� ���� � �� �� ��������� ���������� ��������. 
    ������ ������ ����� ������ ������ ������� ������� � ������, ��� ���� ��� ��� ���������������� ������.

    �� �������� �� ������� ������������� (x86, SPARC TSO, IBM) ������������ ������������-������ ������������ ������������� 
    ��� ����������� ��������. ��� ����������� ������� ������ ������������� �� ��������� �������������� ���������� ����������, 
    ������ ��������� ����������� ����������� ����� ��������� ������� (�� ������� ����������) (��������, ����������� ��������� 
    ���������� �� ��������� �������� ������ ����� (�� �������) ��������� �������� ������-������������ ��� ��������� �� ��������� 
    �������� �������� �� ��������� �������� ��������-�������). �� �������� � ����������� ������������� (ARM, Itanium, PowerPC), 
    ������ �������������� ����������� ���������� ���������� ��� �������� (load) ��� ��� ������� �������� ������.
    
* 
* We will implement spin-lock. It is main purpose is that only one thread can do the given code part.
* 
* seq_cst. 
    �������� ������� (Clang � MSVC) �� GCC � ��� ������������� �������� Store ��� ��������� Sequential Consistency, � ������: 
    a.store(val, memory_order_seq_cst); � � ���� ������ Clang � MSVC ���������� ���������� [LOCK] XCHG reg, [addr], 
    ������� ������� CPU-Store-buffer ��� ��, ��� � ������ MFENCE. � GCC � ���� ������ ���������� ��� ���������� MOV [addr], reg � MFENCE.

* RMW (CAS, ADD�) always seq_cst. 
    �.�. ��� ��������� RMW (Read-Modify-Write) ���������� �� x86_64 ����� ������� LOCK, ������� ������� Store-Buffer, 
    �� ��� ��� ������������� ��������� Sequential-Consistency �� ������ ������������� ����. 
    ����� memory_order ��� RMW ���������� ���������� ���, � ��� ����� � memory_order_acq_rel.

* LOAD(acquire), STORE(release). 
    ��� �����, �� x86_64 ������ 4 ������� ������ (relaxed, consume, acquire, release) ���������� ���������� ������������ ��� 
    � �.�. ����������� x86_64 ������������ ��������� acquire-release ������������� � � ��� ����� ��� �������������� ����������� 
    ���-������������� MESIF(Intel) / MOESI (AMD). ��� ����������� ������ ��� ������ ���������� �������� ���������� ����������� 
    ������� �� ��������� �������� ��� Write Back (�� �� ����������� ��� ������, ���������� ��� Un-cacheable ��� Write Combined 
    � ������� ����������� ��� ������ � Mapped Memory Area from Device � � ��� ������������� �������������� ������ Acquire-semantic).

��� �� ����� ����� � ������� �� ����� ���� ��������������� ��������� ��������, ��������: (Read-X, Write-X) ��� (Write-X, Read-X)

Acuire-Release:
� �������� ��������� ������� ������� ���������� � ����������� ������������
� �������� ��������� ������� ��������� ���������� � Acquire-Release-����������
Sequential:
� �������� ��������� ������� ������� ���������� � ����������� ������������
� ���������� ��������� ������� ��������� ���������� � Sequential-����������
* 
*/
class SpinLock {
public:
    void lock() {
        while (flag.test_and_set(std::memory_order_acquire)) {
            // Spin until we acquire the lock
        }
    }

    void unlock() {
        flag.clear(std::memory_order_release); // All operations between lock and unlock will be visible to next thread that will call lock.
    }

private:
    std::atomic_flag flag = ATOMIC_FLAG_INIT;
};

class SpinLockBad {
public:
    void lock() {
        while (flag.test_and_set(std::memory_order_relaxed)) { // on x86 as release/acquire
            // Spin until we acquire the lock
        }
    }

    void unlock() {
        flag.clear(std::memory_order_relaxed); // All operations between lock and unlock will be visible to next thread that will call lock.
    }

private:
    std::atomic_flag flag = ATOMIC_FLAG_INIT;
};


void common::MemoryModel::memory_barriers()
{
    /**
    * Relaxed memory model.
    * It is possible in the example below that r1 = r2 = 42 because D can be reordered befor A.
    * But VS2022 compiler and current x64_86 processor give always 1. For non-ARM it is ok.
    * 
    * ���� ��� ����������� ������ ������, ������������ ��������� �� ��������� ���������� �������� 
    * �� ���������� ����� ����, ��������, ��� x � y ���������� ������ ����,
    */
    for (auto i = 0; i < 10; ++i)
    {
        {
            auto start = high_resolution_clock::now();
            /**
            * Relaxed works much faster then sequential.
            */
            std::atomic<int> x(1), y(1);
            for (auto i = 0; i < 1e8; ++i) {
                x.store(1);
                y.store(1);
                // Thread 1:
                int r1 = y.load(memory_order_relaxed); // A
                x.store(r1, memory_order_relaxed); // B
                // Thread 2:
                int r2 = x.load(memory_order_relaxed); // C
                y.store(42, memory_order_relaxed); // D

                if (r1 == 42 and r2 == 42)
                    print("r1=", r1, "r2=", r2);
            }
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>(stop - start);
            print("Relaxed time: ", duration.count());
        }

        {
            auto start = high_resolution_clock::now();

            std::atomic<int> x(1), y(1);
            for (auto i = 0; i < 1e8; ++i) {
                x.store(1, memory_order_seq_cst);
                y.store(1, memory_order_seq_cst);
                // Thread 1:
                int r1 = y.load(memory_order_seq_cst); // A
                x.store(r1, memory_order_seq_cst); // B
                // Thread 2:
                int r2 = x.load(memory_order_seq_cst); // C
                y.store(42, memory_order_seq_cst); // D

                if (r1 == 42 and r2 == 42)
                    print("r1=", r1, "r2=", r2);
            }
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>(stop - start);
            print("Sequential time: ", duration.count());
        }
    }
}

void common::MemoryModel::spin_lock()
{
    {
        SpinLock spinLock;

        int val = 0;

        std::vector<std::thread> threads;
        for (auto i = 0; i < 30; ++i) {
            threads.emplace_back(thread([&]() {
                for (auto k = 0; k < 1e6; ++k) {
                    spinLock.lock();
                    val++;
                    spinLock.unlock();
                }
                }));
        }

        for (auto& thread : threads)
            thread.join();

        print("val is ", val);
    }
    {
        SpinLockBad spinLock;

        int val = 0;

        std::vector<std::thread> threads;
        for (auto i = 0; i < 30; ++i) {
            threads.emplace_back(thread([&]() {
                for (auto k = 0; k < 1e6; ++k) {
                    spinLock.lock();
                    val++;
                    spinLock.unlock();
                }
                }));
        }

        for (auto& thread : threads)
            thread.join();

        print("val is ", val);
    }
    {
        int val = 0;

        std::vector<std::thread> threads;
        for (auto i = 0; i < 30; ++i) {
            threads.emplace_back(thread([&]() {
                for (auto k = 0; k < 1e6; ++k) {
                    val++;
                }
                }));
        }

        for (auto& thread : threads)
            thread.join();

        print("val is ", val);
    }
}

void common::MemoryModel::atomic_interface()
{
    /**
    * �++ 17
    * is_lock_free() returns true if the given atomic is lock-free data structure.
    * It can be non lock-free and cause blocking on some platforms and hardware.
    */
    atomic<int> a = 0;
    atomic<bool> b = false;
    print("a.is_lock_free()", a.is_lock_free());
    print("b.is_lock_free()", b.is_lock_free());

    /** 
    * C++ 17
    * returns true if lock-free is guaranteed for the atomic type in all platforms and compilers.
    */
    print("a.is_always_lock_free", a.is_always_lock_free);
    print("b.is_always_lock_free", b.is_always_lock_free);

    /**
    * C++ 20 
    * std::atomic<T>::wait, std::atomic<T>::notify_one, std::atomic<T>::notify_all.
    * 
    * Threads from 0 to 14 will wait until thread 15 will make c=15.
    * 
    */
    std::atomic<int> c = 0;
    std::vector<std::thread> threads;
    for (auto i = 0; i < 20; ++i) {
        threads.emplace_back(thread([&, i=i]() {
            if (i == 15) {
                print("c will be non-zero soon");
                c.store(15, std::memory_order_relaxed);
                c.notify_one();
            }
            else {
                c.wait(0, std::memory_order_relaxed);
                {
                    print("thread", i, "wake up!");
                    c.notify_one(); // notifies each thread in chain manner.
                }
            }
        }));
    }

    for (auto& thread : threads)
        thread.join();

    /**
    * C++ 20
    * std::atomic_ref
    * It allows to do atomic operations on non-atomic variables using reference.
    * We avoid non-necessary copying and assignment operations.
    * 
    * DATA RACE
    *   When several threads modify the same memory part.
    */
    int d = 0; // non atomic but synchronized
    int e = 0; // non atomic
    std::vector<std::thread> threads2;
    for (auto i = 0; i < 20; ++i) {
        threads2.emplace_back(thread([&, i = i]() {
            for (auto i = 0; i < 1e6; ++i)
            {
                std::atomic_ref<int> atomicRef(d);
                atomicRef.fetch_add(1, std::memory_order_relaxed);
                e++; // data race
            }
        }));
    }

    for (auto& thread : threads2)
        thread.join();

    print("d = ", d);
    print("e = ", e);

    /**
    * C++ 20
    * std::atomic_wait is an efficient way to wait the atomic variable to reach the given value.
    * It is efficient from the CPU point of view.
    * These functions are guaranteed to return only if value has changed, even if the underlying implementation unblocks spuriously.
    */
    std::atomic<int> f = 0;
    std::vector<std::thread> threads3;
    for (auto i = 0; i < 20; ++i) {
        threads3.emplace_back(thread([&, i = i]() {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            if (i == 15) {
                print("Value becomes 15. All threads will wake up soon.");
                f.store(15, std::memory_order_relaxed);
                std::atomic_notify_one<int>(&f);
            }
            else {
                std::atomic_wait<int>(&f, 0); // waits while f == 0
                {
                    print("thread", i, "wake up!");
                    std::atomic_notify_one<int>(&f); // notifies each thread in chain manner.
                }
            }
            })
        );
    }
    for (auto& thread : threads3)
        thread.join();
}

/** 
* https://habr.com/ru/post/142662/
* 
*
*/
class Base // 8 bytes
{
    int base;    // 4 bytes
    char otherB; // 4 bytes alignemnt
};

class Derived : public Base // 16 bytes (8 from Base) // on clang it can be 12 as otherB and derived both chars.
{
    char derived; // 4 bytes alignment
    int otherD;   // 4 bytes
};
struct Currency
{
    char firstCurrency; // 8 bytes because of memory alignment
    double firstValue; // 8 bytes
    char secondCurrency; // 8 bytes - alignment
    double secondValue; // 8 bytes
    char baseCurrency; // 4 bytes - alignment
    int baseCurrencyId; // 4 bytes
};
struct CurrencyOpt
{
    double firstValue; // 8 bytes
    double secondValue; // 8 bytes
    int baseCurrencyId; // 4 bytes
    char firstCurrency; // 4 bytes alignment
    char secondCurrency; // 4 bytes alignment
    char baseCurrency; // 4 bytes alignment
};
struct CurrencyExcessive
{
    double firstValue; // 8 bytes
    double secondValue; // 8 bytes
    int baseCurrencyId; // 4 bytes
    char firstCurrency; // 4 bytes alignment
    char secondCurrency; // 4 bytes alignment
    int baseCurrency; // 4 bytes
};
struct CurrencyPair
{
    std::pair<char, double> firstCurrency;
    std::pair<char, double> secondCurrency;
    std::pair<char, int> baseCurrency;
};
// Triviality
struct Trivial
{
    int a;
    int b;

    //������� �� ���������
    int mul()
    {
        return a * b;
    }
};
struct TrivialDefault
{
    int a;
    int b;

    TrivialDefault() = default;
    ~TrivialDefault() = default;
    //������� ������ ��� ����� �� ��������!
    TrivialDefault(const TrivialDefault&) = delete;
    TrivialDefault(TrivialDefault&&) = default;
    TrivialDefault& operator=(const TrivialDefault&) = default;
    TrivialDefault& operator=(TrivialDefault&&) = delete;
};

struct TrivialComposition
{
    Trivial a;
    TrivialDefault b;
};
struct TrivialDerived : public Trivial, public TrivialDefault
{
    //����������� ����� �� ���������
    static int c;
};
struct TrivialDerivedPrivate : private TrivialDerived
{
    int d;
};
struct NonTrivialVirtual
{
    int a;
    int b;
    virtual void foo()
    {
    }
};
struct NonTrivialVirtualBase : public virtual Trivial
{
    int c;
};
struct NonTrivialComposition
{
    NonTrivialVirtual a;
};
struct NonTrivialDtor
{
    ~NonTrivialDtor()
    {
    }
};

// Standard placement.
struct StandardEmpty // Standard layout
{
};
struct Standard // Standard layout
{
protected:
    int a;
    int b;
};
struct StandardDerived : public StandardEmpty, Standard // static members are ok in derived 
{
    static int c;
};
struct StandardDerivedMembers : public StandardEmpty
{
    int c;
    StandardEmpty empty;
};
struct NonStandard // References are not standard layout
{
    int& r;
};
struct NonStandardMember // No non-standard layout members
{
    NonStandard nonStadard;
};
struct NonStandardFirstMember : public StandardEmpty // First member is base class member - forbidden
{
    StandardEmpty empty;
    int c;
};
struct NonStandardVirtual
{
    virtual void foo()
    {
    }
};
struct NonStandardVirtualBase : public virtual Standard
{
};
struct NonStadardMembersInBoth : public Standard
{
    int d;
};
struct NonStadardDifferentAccess
{
    int a;
private:
    int b;
protected:
    int c;
};
void common::MemoryModel::memory_alignment()
{
    /** 
    * Compiler aligns data to the 4 bytes or 8 bytes.
    * Therefore it is necessary to be very attentive when using memcpy with these data structures.
    */
    print("Base", sizeof(Base));
    print("Derived", sizeof(Derived));
    print("Currency", sizeof(Currency));
    print("CurrencyOpt", sizeof(CurrencyOpt));
    print("CurrencyExcessive", sizeof(CurrencyExcessive));
    print("CurrencyPair", sizeof(CurrencyPair));

    /**
    * POD class (Plain-Old-Data)
    *   should be trivial
    *   �������� ����������� �����������
    * 
    * It is used to be compatible with C and to accelerate work with data.
    * 
    * TRIVIAL CLASS
    *   - it is trivially copyable;
    *   - it has trivial default constructor;
    * 
    * ���������� ���������� ����� ������������ ��������� �������:
    *   ����� �� ������ ����� �������������� ������������ �����������
    *   ����� �� ������ ����� �������������� ������������ �����������
    *   ����� �� ������ ����� �������������� ��������� �����������
    *   ����� �� ������ ����� �������������� ��������� �����������
    *   ���������� ������ ������ ���� �����������
    * 
    * �����������, �����������(���������� � operator=), ��������� ���� �� ���� �� �������� ��� ��, 
    *   ���� �� �������� � =default � ����������� ��������� �������:
    *      ����� �� ����� ����������� �������
    *      ����� �� ����� ����������� ������� �������
    *      ����� �� ����� ������������� ������, ������� �������� ��������������
    */
    print("Is trivial Trivial", std::is_trivial<Trivial>::value);
    print("Is trivial TrivialDefault", std::is_trivial<TrivialDefault>::value);
    print("Is trivial TrivialComposition", std::is_trivial<TrivialComposition>::value);
    print("Is trivial TrivialDerived", std::is_trivial<TrivialDerived>::value);
    print("Is trivial TrivialDerivedPrivate", std::is_trivial<TrivialDerivedPrivate>::value);
    print("Is trivial NonTrivialVirtual", std::is_trivial<NonTrivialVirtual>::value);
    print("Is trivial NonTrivialVirtualBase", std::is_trivial<NonTrivialVirtualBase>::value);
    print("Is trivial NonTrivialComposition", std::is_trivial<NonTrivialComposition>::value);
    print("Is trivial NonTrivialDtor", std::is_trivial<NonTrivialDtor>::value);

    /** 
    * �������� ����������� �����������
    *   ����� �������� ����������� �����������, ���� ��������� ������������� ���������:
    *       ����� �� �������� ������������� ������ � ������������� �����������
    *       ����� �� ����� ����������� ������� � ����������� ������� �������
    *       ��� ������������� ����� ������ ����� ���� � ��� �� ������� ������� (private, public ��� protected)
    *       ����� �� ����� ������� ������� � ������������� �����������
    *       ������ ���� �����, �� ���� ��������, �������� ������������� �����. �.�. ���� ��� ���� �� ������� �������, ���� ��� �����.
    *       ����� �� ����� ����� ������ ������������� ������ ������, ������� ����� ��� ������ �� ������� �������.
    *       ����� �� ����� ��������� ������ � �������� ������������� ������.
    */
    print("Is standard layout StandardEmpty", std::is_standard_layout<StandardEmpty>::value);
    print("Is standard layout Standard", std::is_standard_layout<Standard>::value);
    print("Is standard layout StandardDerived", std::is_standard_layout<StandardDerived>::value);
    print("Is standard layout StandardDerivedMembers", std::is_standard_layout<StandardDerivedMembers>::value);
    print("Is standard layout NonStandard", std::is_standard_layout<NonStandard>::value);
    print("Is standard layout NonStandardMember", std::is_standard_layout<NonStandardMember>::value);
    print("Is standard layout NonStandardFirstMember", std::is_standard_layout<NonStandardFirstMember>::value);
    print("Is standard layout NonStandardVirtual", std::is_standard_layout<NonStandardVirtual>::value);
    print("Is standard layout NonStandardVirtualBase", std::is_standard_layout<NonStandardVirtualBase>::value);
    print("Is standard layout NonStadardMembersInBoth", std::is_standard_layout<NonStadardMembersInBoth>::value);
    print("Is standard layout NonStadardDifferentAccess", std::is_standard_layout<NonStadardDifferentAccess>::value);
}

/** 
* http://scrutator.me/post/2014/06/02/objects_memory_layout_p2.aspx
* 
* ��������� �� ����� ��������������� �������
*   ��������� �� ��������� ������� ������ ����� ������� ��������� ��������� �������� �����������, 
*   � ���������� �� ����� �� ������� ������� � ����������� ��� ������, � ����� ������, 
*   �� �������������� ��������� � ��� ������� �� ����������.
* 
* ������� ����� ����� �������� 0 ���� � �����������
*   ���������� ����� �������������� ������� �����, ������� �� �������� ����������� ������ ���, 
*   ��� �� �� ����� �������� �� ������� ����� � ���� �������
*/
class BaseA
{
public:
    int fieldA = 0;
};
class BaseB
{
public:
    int fieldB = 0;
};
class DerivedA : public BaseA, public BaseB
{
public:
    int fieldD = 0;
};
class Gun // Size is 16 bytes
{
public:
    virtual void fire() = 0; // vfptr is also 8 bytes
    virtual void reload()
    {}
    virtual void putOnSafe()
    {}
    virtual ~Gun()
    {}
private:
    unsigned short m_Calibre = 0; // 8 bytes
};
class Machinegun : public Gun // 16 bytes
{
public:
    void fire() override // vfptr indicates to the current implementation in virtual functions table.
    {}
    void putOnSafe() override
    {}
};
#pragma pack(push, 1)
struct Foo
{
    char a;
    char b;
    double d = 1.0f;
};
#pragma pack(pop)
struct Foo2 {
    char a; 
    char b;
    double d = 1.0f;
};
#pragma pack(push, 1)
struct BitFieldIpHeader // 48 bits or 6 bytes
{
    uint8_t header_length : 4; 
    uint8_t version : 4;
    uint8_t type_of_service;

    // Flags
    uint8_t _reserved : 1;
    uint8_t dont_fragment : 1;
    uint8_t more_fragments : 1;
    uint8_t fragment_offset_part1 : 5;
    uint8_t fragment_offset_part2;
    uint8_t time_to_live;
    uint8_t protocol;
};
#pragma pack(pop)
void common::MemoryModel::class_in_memory()
{
    /** 
    * POINTERS WITH MULTIPLE INHERITANCE
    */
    DerivedA* pDer = new DerivedA; // origin of DerivedA
    BaseA* pBaseA = pDer;          // origin of DerivedA that is first Base class
    BaseB* pBaseB = pDer;          // 4 bytes after pBaseA, pointer to the part of the object after second pointer.
    print(pDer, pBaseA, pBaseB);

    /** 
    * SLICING (����)
    *   If you copy info from the pointers above then copy will contain only base class fields.
    */
    auto pDerCopy = *pDer;
    auto pBaseACopy = *pBaseA;
    auto pBaseBCopy = *pBaseB;
    print(sizeof(pBaseACopy), sizeof(pBaseBCopy)); // 4 bytes and not 8 bytes
    print(sizeof(pDerCopy)); // 12 bytes
    print(pBaseACopy.fieldA); // exists
    //print(pBaseACopy.fieldD); // error because of slicing

    /** 
    * OBJECTS WITH VIRTUAL FUNCTIONS IN MEMORY
    * 
    * Table to what vfptr points contains function addresses.
    * vfptr is initialized in class constructor.
    * Each class in virtual hierarchy contains its own table of virtual functions that differs from the other class.
    * The order of functions in VTable repeats their order in class declaration.
    * If there is multiple inheritence with virtual functions then two vfptr exist.
    * 
    */
    print("Size of Gun", sizeof(Gun));
    print("Size of Machinegun", sizeof(Machinegun));

    /** 
    * pragma pack
    * Allows to not pack structure members.
    * Packed structure has smaller size but calculation with packed structure is much slower.
    */
    print("Size of Foo", sizeof(Foo));
    print("Size of Foo2", sizeof(Foo2));
    constexpr size_t size = 1e7;
    for(auto k = 0; k < 20; ++k)
    {
        double res = 0.0;
        std::vector<Foo> arr1(size); // not aligned
        auto start = std::chrono::high_resolution_clock::now();
        for (auto i = 0; i < size; ++i)
            res += arr1[i].d;
        auto end = std::chrono::high_resolution_clock::now();
        print("Time of cal for not aligned structure: ", std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
    }
    for (auto k = 0; k < 20; ++k)
    {
        double res = 0.0;
        std::vector<Foo2> arr2(size); // not aligned
        auto start = std::chrono::high_resolution_clock::now();
        for (auto i = 0; i < size; ++i)
            res += arr2[i].d;
        auto end = std::chrono::high_resolution_clock::now();
        print("Time of cal for aligned structure: ", std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
    }

    /** 
    * BIT FIELDS IN STRUCTS
    * The following struct has 6 bytes in size.
    * First member header_length occupies 4 bytes even if it is 8-bit int.
    */
    print("Size of BitFieldIpHeader", sizeof(BitFieldIpHeader));
    BitFieldIpHeader header;
    header.header_length = 11;
    header.version = 11;
    print("version=", static_cast<int>(header.version), "header_length=", static_cast<int>(header.header_length));
    header.header_length = 255; // version won't be changed but header_length will use only first 4 bits or 1111 = 15.
    print("version=", static_cast<int>(header.version), "header_length=", static_cast<int>(header.header_length));

}

/** 
* https://overcoder.net/q/382040/%D1%87%D1%82%D0%BE-%D0%BE%D1%82%D0%BB%D0%B8%D1%87%D0%B0%D0%B5%D1%82%D1%81%D1%8F-%D0%BC%D0%B5%D0%B6%D0%B4%D1%83-join-%D0%B8-detach-%D0%B4%D0%BB%D1%8F-%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE%D0%BF%D0%BE%D1%82%D0%BE%D1%87%D0%BD%D0%BE%D1%81%D1%82%D0%B8-%D0%B2-c
* join() blocks calling thread till thread will return. It is a good way to know when thread will finish.
* detach() continue calling thread.
*/
void common::MemoryModel::join_vs_detach()
{
    {
        thread([]() {
            for (auto i = 0; i < 10; ++i) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                print("thread works", i);
            }
            }).join();
            print("This line should be after another thread as it was joined.");
    }

    {
        thread([]() {
            for (auto i = 0; i < 10; ++i) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                print("thread works", i);
            }
            }).detach();
            print("This line should be at the time of another thread as it was detached.");
    }
}
