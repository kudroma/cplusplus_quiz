#include "smartpointers.hpp"

#include <iostream>
#include <thread>
#include <vector>
#include <chrono>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
	((cout << args << " "), ...);
	cout << endl;
}

int gCounter{};

void common::SmartPointers::unique_ptr_size()
{
	struct A { int a{}; };

	print("\n\n\n UNIQUE PTR SIZE");

	{
		int* ptr = new int;
		print("Raw: check program size in task manager");
		this_thread::sleep_for(5s);
		print("Raw int pointer                 ", sizeof(ptr));
		delete ptr;
	}

	{
		unique_ptr<int> uPtr = make_unique<int>(0);
		print("Unique: check program size in task manager");
		this_thread::sleep_for(5s);
		print("Unique int pointer              ", sizeof(uPtr));
	}

	{
		/** Deleter in unique_ptr is a part of the type and no additional memory is used. */
		vector< unique_ptr<A, void(*)(A*)> > ptrs;
		for (auto i = 0; i < 5; ++i) {
			ptrs.push_back(unique_ptr<A, void(*)(A*)>(new A(), [](A* ptr) {
				int arr[100000];
				print("Delete inside unique ptr");
				}));
		}
		print("Unique with deleter: check program size in task manager.");
		this_thread::sleep_for(5s);
		print("Unique int pointer with deleter ", sizeof(ptrs[0]));
	}

	{
		print("After unique with deleter.");
		this_thread::sleep_for(5s);
		/** Shared pointer usually contains two pointers: 
		* one to the control block and one to the data itself.
		* Control block of each object contains in memory deleter.
		*/
		vector< shared_ptr<A> > ptrs;
		vector<int> s;
		s.resize(1e6);
		for (auto i = 0; i < 5; ++i) {
			shared_ptr<A> sPtr(new A(), [s = s](A* ptr) {
				int arr[100000];
				print("Delete inside shared ptr ", arr[0]);
				});
		}
		print("Shared: check program size in task manager");
		this_thread::sleep_for(5s);
		print("Shared int pointer              ", sizeof(ptrs[0]));
	}

	{
		shared_ptr<int> sPtr = make_shared<int>(0);
		weak_ptr<int>   wPtr = sPtr;
		print("Weak: check program size in task manager");
		this_thread::sleep_for(5s);
		print("Weak int pointer                ", sizeof(wPtr));
	}
}

void common::SmartPointers::weak_ptr_17()
{
	// Since C++ 17.
	/**
	* Weak pointer allows to avoid circular references and allows to implement observer pattern.
	*/
	print("Create std::shared_ptr<int>.");
	shared_ptr<int> p = std::make_shared<int>(67);
	print("Create std::weak_ptr<int> of the same object.");
	weak_ptr<int> wP = p;
	print("Size of weak ptr is ", sizeof(wP));
	print("wP.use_count() = ", wP.use_count());
	print("wP.expired() = ", wP.expired());

	print("Create another two shared_ptr<int> to the same object.");
	auto p2 = p;
	auto p3 = p;
	print("wP.use_count() = ", wP.use_count());
	print("wP.expired() = ", wP.expired());

	print("Remove second shared pointer");
	p2 = nullptr;
	print("wP.use_count() = ", wP.use_count());
	print("wP.expired() = ", wP.expired());

	print("Use lock() if you want to use content of weak_ptr");
	if (auto pl = wP.lock(); pl)
		print("content = ", *pl.get());

	print("Remove third shared pointer");
	p3 = nullptr;
	print("wP.use_count() = ", wP.use_count());
	print("wP.expired() = ", wP.expired());

	print("Remove first shared pointer");
	p = nullptr;
	print("wP.use_count() = ", wP.use_count());
	print("wP.expired() = ", wP.expired());

	print("If weak ptr expired then lock operation returns empty shared ptr.");
	if (auto pl = wP.lock(); pl)
		print("It won't be printed.");

	print("The end.");
}

void common::SmartPointers::compare_ptr_20()
{
	print("Suppose there are two shared_ptr that allocates memory.");
	shared_ptr<int> p1 = std::make_shared<int>(42);
	shared_ptr<int> p2 = std::make_shared<int>(42);
	print("p1 == p2", p1 == p2); // Returns false as p1 and p2 actually reference different memory locations.
	auto p3 = p2;
	print("p3 == p2", p3 == p2); // Returns true as p3 and p2 reference the same memory location.
	auto w2 = p2;
	auto w3 = p3;
	print("w3 == w2", w3 == w2); // Returns true as w3 and w2 reference the same memory location.
}

void common::SmartPointers::atomic_with_shared_20()
{
	// C++20 introduced atomic operations on shared pointers that allow to 
	// provide thread safety of its content.
	// ???
}

template<typename T>
class CustomAllocator : public std::allocator<T> {
public:
	using size_type = size_t;
	using pointer = T*;
	using const_pointer = const T*;

	template<typename U>
	struct rebind {
		using other = CustomAllocator<U>;
	};

	CustomAllocator() = default;

	template<typename U>
	CustomAllocator(const CustomAllocator<U>&) {}

	pointer allocate(size_type n, const void* hint = 0) {
		std::cout << "Allocating " << n << " elements of type T." << std::endl;
		return std::allocator<T>::allocate(n);
	}

	void deallocate(pointer p, size_type n) {
		std::cout << "Deallocating " << n << " elements of type T." << std::endl;
		std::allocator<T>::deallocate(p, n);
	}
};

void common::SmartPointers::make_shared_custom_allocator_20()
{
	// Create a shared_ptr with custom allocator.
	// You will see text from allocator code.
	auto customAllocatedSharedPtr = std::allocate_shared<int>(CustomAllocator<int>(), 42);
	std::cout << "Value: " << *customAllocatedSharedPtr << std::endl;
}

void common::SmartPointers::make_shared_for_arrays_20()
{
	// It is possible to create arrays of 20 elements using std::make_shared.
	std::shared_ptr<int[]> new_ptr = std::make_shared<int[]>(10);
	print("You could use this type as regular array.");
	new_ptr[0] = 42;
	print(new_ptr[0]);
}

void common::SmartPointers::ring_memory_leak_with_shared_pointers()
{
	print("SHARED POINTER MEMORY LEAK.");

	struct A {
		vector<int> m_vec;
		A(shared_ptr<A> a = nullptr) : m_vec(vector<int>(1e3)), m_a(a) {}
		shared_ptr<A> m_a;
	};

	{
		vector<shared_ptr<A> > ptrs;
		ptrs.push_back(make_shared<A>());
		for (auto i = 0; i < 1e3; ++i) {
			ptrs.push_back(make_shared<A>(ptrs.back()));
		}
		ptrs[0]->m_a = ptrs.back();
		print("Check memory now. It should be with data!");
		this_thread::sleep_for(5s);
	}
	print("But there data should be deleted but memory leak takes place.");
	this_thread::sleep_for(5s);
}

void common::SmartPointers::move_unique_ptr()
{
	print("MOVE UNIQUE POINTER.");
	struct A {};
	unique_ptr<A> ptr1 = make_unique<A>();
	unique_ptr<A> ptr2(move(ptr1));
	print("ptr1 that was moved is       ", static_cast<bool>(ptr1));
	print("ptr2 to what ptr was moved is", static_cast<bool>(ptr2));
}

/** 
* std::make_shared does allocation of control block and object memory in one step. It is more efficient.
* std::make_shared throws bad_alloc exception if memory is not allocates.
*/
void common::SmartPointers::make_shared_efficiency_and_safety()
{
	// Efficiency
	struct A { double a{ 0.0 }; };

	auto start = std::chrono::high_resolution_clock::now();
	for (auto i = 0; i < 1e7; ++i) {
		std::shared_ptr<A> a = std::shared_ptr<A>(new A);
	}
	auto end = std::chrono::high_resolution_clock::now();
	print("not make_shared - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());

	start = std::chrono::high_resolution_clock::now();
	for (auto i = 0; i < 1e7; ++i) {
		std::shared_ptr<A> a = std::make_shared<A>();
	}
	end = std::chrono::high_resolution_clock::now();
	print("not make_shared - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());

	// exception safety
	struct B
	{
		B() {
			throw std::runtime_error("Problem init.");
		}
		double b{ 0.0 };
	};
}


/** 
* Shared pointer data is destroyed when reference counter became 0.
* Control block is destroyed when weak pointer and shared pointer counter became zero.
*/
void common::SmartPointers::make_shared_control_block_removed()
{
	struct A {
		A() = default;
		~A()
		{
			print("!A()");
		}
	};

	auto p1 = std::make_shared<A>();
	auto p2 = p1;
	std::weak_ptr<A> pw(p1);
	p1.reset();
	p2.reset(); // object is destroyed there.
	pw.reset(); // control block is destroyed there.
}

/**
* Resource Management: Sometimes, the default delete operator may not be appropriate 
for deallocating resources associated with an object. For example, if an object was allocated using a custom allocator, 
or if it requires a special cleanup routine, a custom deallocator allows you to specify the correct mechanism 
for releasing the associated resources.

* Non-heap Allocations: Smart pointers are commonly used for managing dynamic memory allocated 
on the heap. However, there are cases where objects are allocated on different memory regions 
(e.g., stack, shared memory). Custom deallocation allows you to manage such scenarios.

* Integration with External APIs: When working with libraries or APIs that provide their 
own memory allocation mechanisms, you may need to use custom deallocators to ensure proper integration. 
For example, if a library allocates resources that need to be deallocated using a specific function 
provided by the library, a custom deallocator can be used to call that function appropriately.

* Performance Optimization: In certain cases, custom deallocators can be used to optimize 
memory management and reduce overhead. For example, if you know that a particular type of object 
is frequently allocated and deallocated in a specific pattern, a custom deallocator can be tailored 
to better suit that pattern, potentially improving performance.

* Debugging and Instrumentation: Custom deallocators can be useful for debugging and instrumenting 
memory usage. By implementing custom deallocation routines, you can track memory allocations and deallocations, 
log information, or perform other diagnostic tasks.

* Memory Pools and Custom Allocators: Custom deallocators can be used in conjunction with custom allocators 
or memory pools to manage memory in a more efficient or specialized way. This is particularly 
useful in scenarios where you need fine-grained control over memory allocation and deallocation, 
or when you want to implement specific memory allocation strategies (e.g., caching, preallocation).
*/
// Custom deallocator function
template<typename T>
struct CustomDeleter {
	void operator()(T* ptr) {
		if (ptr) {
			ptr->cleanup();
			delete ptr; // Using delete here for demonstration, you can replace it with any custom deallocation method
		}
	}
};

void common::SmartPointers::custom_deallocator()
{
	class CustomObject {
	public:
		CustomObject(int data) : data(data) {}
		~CustomObject() { std::cout << "Destroying CustomObject with data: " << data << std::endl; }
		void cleanup() { std::cout << "Custom cleanup for CustomObject with data: " << data << std::endl; }
	private:
		int data;
	};

	std::shared_ptr<CustomObject> sp(new CustomObject(42), CustomDeleter<CustomObject>());
}

void common::SmartPointers::check()
{
	/*unique_ptr_size();
	ring_memory_leak_with_shared_pointers();
	move_unique_ptr();*/
}
