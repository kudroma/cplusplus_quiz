#include "lambdas.hpp"
#include "lambdas.hpp"

#include <algorithm>
#include <iostream>
#include <numeric>
#include <utility>
#include <vector>

using namespace std;

int globalVar{ 1 };

struct Modifiable {
    int val{};
};

template<typename ... T>
void print(T&& ... args) {
    ((cout << args << " "), ...);
    cout << endl;
}

/** 
* Static local variables and global variables are captured by default.
* There are some other classes too.
*/
void common::Lambdas::default_capture()
{
    print("\n\n\n DEFAULT CAPTURES");

    int localVar{ 2 };
    const int& ref = localVar;
    static int staticLocalVar{ 3 };
    auto lambda = []() {
        print(__FUNCTION__, globalVar, staticLocalVar);
    };
    lambda();
}

void common::Lambdas::mutable_lambda()
{
    print("\n\n\n MUTABLE LAMBDAS");
    Modifiable s;
    auto funcMut = [=]() mutable {
        s.val++; // It is allowed to modify copy of object s.
        print("Increment val in mutable lambda capturing by value...");
        print(__FUNCTION__, "val =", s.val);
    };
    /*
    auto funcConst = [=]() {
        s.val++; // ERROR! It is not allowed to modify copy of the object s.
        print("Increment val in mutable lambda capturing by value...");
        print(__FUNCTION__, "val =", s.val);
    };
    */
    funcMut();
    s.val++;
    print("Increment val in after lambda...");
    print(__FUNCTION__, "val =", s.val);
}

void common::Lambdas::constexpr_lambda()
{
    print("\n\n\n CONSTEXPR LAMBDAS");
    constexpr int val{ 6 };
    auto func = []() constexpr { return val + 7; }; // is calculated in compile time
    print(__FUNCTION__, func());
    constexpr int res = func();
    static_assert(res == 13);
}

void common::Lambdas::consteval_lambdas()
{
    // Intbu why doesn't this code work?
    /**
    auto sqr = [](int n) consteval { return n * n; };
    constexpr int val1 = sqr(5); // OK for constexpr var.
    const int     val2 = sqr(5); // OK for const var.
    int           val3 = sqr(5); // Error. Not const var.
    */
}

void common::Lambdas::trailing_return_type()
{
    print("\n\n\n TRAILING RETURN TYPE");

    auto func = [](int n) -> double {
        return n * n;
    };

    print(__FUNCTION__, "func returns result of type", typeid(func(5)).name());
}

void common::Lambdas::template_lambda()
{
    print("\n\n\n TEMPLATE LAMBDAS");
    auto func = []<typename T>(T num) { return num + num; };
    print(__FUNCTION__, "func returns result of type", typeid(func(5)).name());
    print(__FUNCTION__, "func returns result of type", typeid(func(5.0f)).name());
    print(__FUNCTION__, "func returns result of type", typeid(func(5.0)).name());
}

void common::Lambdas::nullary_lambda()
{
    print("\n\n\n NULLARY LAMBDA");
    auto func = [] { return "lambda"; };
    print(__FUNCTION__, "func returns ", func());
}

void common::Lambdas::generic_lambda()
{
    print("\n\n\n GENERIC LAMBDA");
    auto func = [](auto size, auto&& cont) {
        cont.resize(size);
        print(__FUNCTION__, "VEC size", cont.size());
    };
    vector<int> vec;
    vec.resize(10);
    print(__FUNCTION__, "VEC size", vec.size());
    func(100, vec);
}

void common::Lambdas::generic_template_lambda()
{
    print("\n\n\n GENERIC TEMPLATE LAMBDA");
    auto func = []<typename T>(vector<T> && cont, auto size) { // additional tempplate parameter is automatically added for auto.
        cont.resize(size);
        print(__FUNCTION__, "VEC size", cont.size());
    };
    vector<int> vec;
    vec.resize(10);
    print(__FUNCTION__, "VEC size", vec.size());
    func(move(vec), 100);
}

void common::Lambdas::this_in_lambda()
{
    print("\n\n\n GENERIC TEMPLATE LAMBDA");
    struct X {
        int val{ 2 };
        void f() {
            [=]() {
                print(__FUNCTION__, this->val);
            }();
        }
    };

    X x;
    x.f();
}

void wrap(int (*f)(int), int a) {
    print(__FUNCTION__, f(a));
}

void common::Lambdas::lambda_as_function_argument()
{
    print("\n\n\n LAMBDA AS FUNCTION ARGUMENT");
    auto func = [](auto a) {return 2 * a; };
    wrap(func, 4);
}

void common::Lambdas::lambda_capture_rules()
{
    print("\n\n\n LAMBDA CAPTURE RULES");
    struct S2 {
        void f(int i) {
            [&] {};            // OK: by-reference capture default
            [&, i] {};         // OK: by-reference capture, except i is captured by copy
            // [&, &i] {};     // Error: by-reference capture when by-reference is the default
            [&, this] {};      // OK, equivalent to [&]
            [&, this, i] {};   // OK, equivalent to [&, i]
            //[i, i] {};        // Error: i repeated
            //[this, *this] {}; // Error: "this" repeated (C++17)
        }
    };
}

template<typename ... Types>
void variadicF1(Types&& ... args) {
    auto lambda = [args ...]() mutable { 
        print("variadicF1");
        (args++, ...); 
        (print(__FUNCTION__, args), ...); 
    };
    lambda();
}

template<typename ... Types>
void variadicF2(Types&& ... args) {
    auto lambda = [&args ...]() { 
        print("variadicF2");
        (args++, ...); 
        (print(__FUNCTION__, args), ...); 
    };
    lambda();
}

template<typename ... Types>
void variadicF3(Types&& ... args) {
    auto lambda = [... args = 2 * args] () mutable {
        print("variadicF3");
        (args++, ...);
        (print(__FUNCTION__, args), ...);
    };
    lambda();
}

template<typename ... Types>
void variadicF4(Types&& ... args) {
    auto lambda = [&... args = args]{
        print("variadicF4");
        (args++, ...);
        (print(__FUNCTION__, args), ...);
    };
    lambda();
}

void common::Lambdas::lambda_capture_types()
{
    print("\n\n\n LAMBDA CAPTURE TYPES");

    // capture by copy single var
    double vald{ 3.0 };
    auto fCopy = [vald] () mutable { print(__FUNCTION__, ++vald); };
    fCopy();
    print(__FUNCTION__, vald);
    print("");

    // capture by ref single var
    float  valf{ 2.0f };
    auto fRef = [&valf] {print(__FUNCTION__, ++valf); };
    fRef();
    print(__FUNCTION__, valf);
    print("");

    // capture by copy pack extension
    int vali1{}, vali2{}, vali3{};
    variadicF1(vali1, vali2, vali3);
    print(__FUNCTION__, vali1, vali2, vali3);
    print("");

    // capture by ref pack extension
    variadicF2(vali1, vali2, vali3);
    print(__FUNCTION__, vali1, vali2, vali3);
    print("");

    // capture with initializer (C++14)
    int vali4{}, vali5{}, vali6{};
    auto fInit = [&vali4 = vali4, &vali5 = vali5, vali6 = vali6] () mutable {
        print(__FUNCTION__, ++vali4, ++vali5, ++vali6);
    };
    fInit();
    print(__FUNCTION__, vali4, vali5, vali6);
    print("");

    // capture by copy pack extenstion with initializer (C++20)
    int vali7{ 1 }, vali8{ 1 }, vali9{ 1 };
    variadicF3(vali7, vali8, vali9);
    print(__FUNCTION__, vali7, vali8, vali9);
    print("");

    // capture by ref pack extenstion with initializer (C++20)
    int vali10{ 1 }, vali11{ 1 }, vali12{ 1 };
    variadicF4(vali10, vali11, vali12);
    print(__FUNCTION__, vali10, vali11, vali12);
    print("");

    // capture all by copy or ref.
    int a{}, b{}, c{};
    auto fCopy2 = [=] () mutable {
        print(__FUNCTION__, ++a, ++b, ++c);
    };
    auto fRef2 = [&] {
        print(__FUNCTION__, ++a, ++b, ++c);
    };
    fCopy2();
    fRef2();
    print(__FUNCTION__, a, b, c);
}

void common::Lambdas::lambda_capture_this()
{
    print("\n\n\n LAMBDA THIS CAPTURE");

    struct A {
        int val{};
        void fRef() {
            val = 0;
            auto func = [this]() mutable {
                this->val++;
                print(__FUNCTION__, this->val);
            };
            func();
            print(__FUNCTION__, val);
        }
        void fCopy() {
            val = 0;
            auto func = [*this]() mutable { // there is copy of this is captured. Not this itself.
                this->val++;
                print(__FUNCTION__, this->val);
            };
            func();
            print(__FUNCTION__, val);
        }
    };
    
    A ob;
    ob.fRef();
    ob.fCopy();
    print("");
}

auto func4(int& val) {
    return [&]() {print(__FUNCTION__, val); };
}

void common::Lambdas::lambda_return_type()
{
    print("\n\n\n LAMBDA AS RETURN TYPE");

    int i{ 3 };
    auto f = func4(i);
    i = 5;
    f();
}

void common::Lambdas::lambda_as_argument()
{
    print("\n\n\n LAMBDA AS ARGUMENT");

    const size_t n = 10;
    vector<int> v;
    v.resize(n);
    iota(v.begin(), v.end(), -5);
    for_each(v.begin(), v.end(), [](int el) {print(__FUNCTION__, el); });
}

void common::Lambdas::lambda_structured_bindings()
{
    // Structured bindings in lambda captures
    int x = 5;
    int y = 6;
    auto lambda = [x, y]() {
        auto [a, b] = std::tie(x, y); // Structured binding
        return a + b;
    };
    print(x, y, lambda());
}

void common::Lambdas::capture_non_static_class_members()
{
    // Capturing non-static data members
    class MyClass {
    public:
        int memberVar = 42;
    };

    int x = 5;
    int y = 10;
    MyClass obj;

    auto lambda = [&x, y, obj = obj.memberVar]() {
        return x + y + obj;
    };

    print("Sum:", lambda());
}

void common::Lambdas::check()
{
    default_capture();
    mutable_lambda();
    constexpr_lambda();
    //consteval_lambdas();
    trailing_return_type();
    template_lambda();
    nullary_lambda();
    generic_lambda();
    generic_template_lambda();
    lambda_as_function_argument();
    lambda_capture_rules();
    lambda_capture_types();
    lambda_capture_this();
    lambda_return_type();
    lambda_as_argument();
}