#pragma once

namespace common {

	class Standard
	{
	public:

		/** What std::min and std::max return. */
		static void min_max();

		/** String end. */
		static void string_end();

		/** Basic string with c-style string. */
		static void basic_vs_cstyle_string();

		/** Vector construction. */
		static void vector_construction();
	};
}