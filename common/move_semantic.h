#pragma once

namespace common {
	class MoveSemantic
	{
	public:

		static void why_rvalues_are_useful();

		static void rvalue_pointer();

		static void when_rvalue_overload_is_called();

		static void when_move_ctor_is_called();

		static void which_copy_ctor_is_called();

		static void const_lvalue_ref_accepts_temporal_objects_too();

		static void perfect_forwarding();

		static void move_is_absent_where_no_move_ctor();
	};
}