#include "template.hpp"

#include <cstddef>
#include <concepts>
#include <iostream>
#include <typeinfo>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
    ((cout << args << " "), ...);
    cout << endl;
}

/**
* Elegant way of sum calculation. If there are more than 1 argument then second overload is used.
* I only one arg remains then first overload is used.
* sum is recursively calculated.
* But the number of function calls is equal to the number of arguments.
*/
template<typename T>
T sum(T arg) { print("1st overload"); return arg; }
template<typename T, typename ...Args> T sum(T arg, Args... args) { print("2nd overload"); return arg + sum<T>(args...); }
void common::Template::variadic_template()
{
    auto n1 = sum(0.5, 1, 0.5, 1);
    auto n2 = sum(1, 0.5, 1, 0.5);
    print(n1, n2);
}

/** 
* If only empty parameter specialization is valid then it is undefined behavior according to the standard.
*/
template <typename ...Ts>
struct X {
    X(Ts ...args) : Var(0, args...) {}
    int Var;
};
void common::Template::undefined_behavior_only_empty_specialization()
{
    X<> x;
    //X<double> x2(1.0); // Impossible as Var(0, 1.0) is not initializable. 
    print(x.Var);
}

/**
* Variadic function with parameters of the same type vs variadic template. Which will be selected?
* 
*/
template<typename T>
void foo(T...) { std::cout << 'A'; }
template<typename... T>
void foo(T...) { std::cout << 'B'; }
void common::Template::variadic_template_deduction_order()
{
    foo(1); // A will be printed.
    foo(1, 2); // B will be printed.
}

/*
* Summary: more specialized variant is selected when overloading occurs.
* 
    The name f is overloaded by the two function templates void f(T) and void f(T*).
    Note that overload resolution only considers the function templates, not the explicit specialisation void f<>(int*)!
    For overload resolution, first we deduce the template arguments for each function template,
    and get T = int * for the first one, and T = int for the second.

    Both function templates are viable, but which one is best?
    According to
    �16.3.3, a function template is a better match than another function template if it's more specialised:
        a viable function F1 is defined to be a better function than another viable function F2
        if (...) the function template for F1 is more specialized than the template for F2
        according to the partial ordering rules described in 17.5.6.2
    The process of partial ordering is a bit long to quote here, and not key to this question.
    But in summary, anything accepted by f(T*) would also be accepted by f(T),
    but not the other way around. So f(T*) is more specialised.
    Now we know that the function template void f(T*) is selected by overload resolution,
    and we can start thinking about specialisations.
    Which of the function templates is void f<>(int*) a specialisation of?
    �17.7.3:
    A declaration of a function template (...) being explicitly specialized
    shall precede the declaration of the explicit specialization.
    So the explicit specialisation void f<>(int*) is a specialisation of void f(T),
    since that's the only function template declaration that precedes it.
    Overload resolution however selected the other function template,
    void f(T*), and we instead call an implicitly instantiated a specialisation of that, printing 3.
*/
template<class T> void f(T) { std::cout << 1; }
template<> void f<>(int*) { std::cout << 2; }
template<class T> void f(T*) { std::cout << 3; }
void common::Template::order_of_template_instantiation()
{
    int* p = nullptr;
    f(p);
}


/** 
* Concepts is very effective instrument to allow template type specialization.
*/
template<typename T>
concept Integral = std::is_integral_v<T>;

template<typename T>
concept NotIntegral = !std::is_integral_v<T>;

template<typename T>
concept Float = std::is_floating_point_v<T>;

template<Integral A, typename B> 
void foo() { print("foo<Integral A, typename B>"); }

template<NotIntegral A, Float B>
void foo() { print("foo<NotIntegral A, Float B>()"); }

template<typename A, typename B>
void foo() { print("foo<typename A, typename B>()"); }

void common::Template::sfinae_concept_20()
{
    struct A {};
    struct B {};

    foo<int, float>();
    foo<A, float>();
    foo<A, B>();
}


/**
* We could easily set type constraints using C++ 20 concepts.
* For example below we require that class should have either print() method either void convert(int).
* We use concepts disjunction to create final MethodRequirements concept from 
* two other: HasMethodPrint and HasConvertTo.
*/
template<typename T>
concept HasMethodPrint = requires(T t)
{
    // { t.print() } -> std::convertible_to<void>; // requires void print()
    { t.print() }; // any print() signature is available.
};

template<typename T>
concept HasConvertToInt = requires(T t)
{
    { t.convert(int{}) } -> std::convertible_to<void>;
};

template<typename T>
concept MethodRequirements = HasConvertToInt<T> || HasMethodPrint<T>;

template<MethodRequirements T>
class Printable {};

void common::Template::class_structure_concept_20()
{
    struct A {
        void print() {};
    };

    struct B {
        bool print() {};
    };

    struct C {
        void convert(int a) {}
    };

    struct D {};

    Printable<A> a;
    Printable<B> b;
    Printable<C> c;
    //Printable<D> d; // doesn't match
}
