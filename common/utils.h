﻿#pragma once
#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#define _USE_MATH_DEFINES
#include <math.h>
#include <numeric>
#include <random>
#include <ratio>
#include <regex>
#include <set>
#include <string>
#include <tuple>
#include <unordered_set>
#include <variant>
#include <vector>
#define NOMINMAX
#include <winsock.h>

using namespace std;

template <typename ... T>
void print(const T& ... args)
{
    using Tuple = std::tuple<T...>;
    Tuple t(args...);
    std::apply([](auto&&... args) {((std::cout << args << '\n'), ...); }, t);
}

/** This sfinae doesn't work. */
template <typename, typename = void>
struct is_printable : false_type {};
template<typename T>
struct is_printable<T, void_t<decltype(&T::toString)>> : is_same< remove_cvref<decltype(declval<T>().toString()) >, string > {};

/** SFINAE
* https://habr.com/ru/post/205772/
*/
template<typename T> 
struct is_printable_2 {
private:
    template<typename U> static decltype(declval<U>.toString()) detect(const U&);
    static void detect(...);
public:
    static constexpr bool value = !std::is_same_v<void, decltype(detect(declval<T>()))>;
};

template<typename T> struct has_foo {
private:  // Спрячем от пользователя детали реализации.
    static int detect(...);  // Статическую функцию и вызывать проще.
    template<typename U> static decltype(std::declval<U>().foo(42)) detect(const U&);
public:
    static constexpr bool value = std::is_same<void, decltype(detect(std::declval<T>()))>::value;  // Вот видите, готово.
};

namespace duff_cucle
{
    /*
    https://stackoverflow.com/questions/514118/how-does-duffs-device-work
    */

    class A {
        int foo = 0;

    public:
        int& getFoo() { return foo; }
        void printFoo() { std::cout << foo; }
    };

    struct S
    {
    };

    void check()
    {
        int n = 3;
        int i = 0;

        switch (n % 2) {
        case 0:
            do {
                ++i;
        case 1: ++i;
            } while (--n > 0);
        }

        std::cout << i;
    }
}

namespace template_lookup_rules
{
    /**
    [temp.res]§17.69 states: "When looking for the declaration of a name used in a template definition, the usual lookup rules (§6.4.1, §6.4.2) are used for non-dependent names. The lookup of names dependent on the template parameters is postponed until the actual template argument is known (§17.6.2)."

    The first call to adl is a non-dependent call, so it is looked up at the time of definition of the function template. The resolution of the second call is deferred until the template is instantiated because it depends on a template parameter.

    template<typename T> void call_adl_function(T t)
    {
        adl(S()); // Independent, looks up adl now.
        adl(t); // Dependent, looks up adl later.
    }
    When adl is being looked up at the time of definition of the function template, the only version of adl that exists is the templated adl(T). Specifically, adl(S) does not exist yet, and is not a candidate.

    Note: At the time of writing, this program does not confirm to the standard in some recent versions of Visual Studio's C++ compiler.
    */

    template<typename T>
    void adl(T)
    {
        cout << "T";
    }

    struct S
    {
    };

    void adl(S)
    {
        cout << "S";
    }

    template<typename T>
    void call_adl(T t)
    {
        adl(S());
        adl(t);
    }

    void check()
    {
        call_adl(S());
    }
}

namespace varian_instantiation {
    /*
    What does the std::variant default constructor do? 
    Which of the types does it pick, or is the variant empty? 
    And if it picks one, how is the value initialized?

    std::variants default constructor constructs a variant holding the value-initialized value of the first alternative. So it picks C as the type and calls the C::C() default constructor initializing it with i==1, which gets printed.

    [variant.ctor]§23.7.3.1:

    In the descriptions that follow, let i be in the range [0, sizeof...(Types)), and Ti be the ith type in Types....


    constexpr variant() noexcept(see below );
    Effects: Constructs a variant holding a value-initialized value of type T0.

    Now what does value-initialized mean?

    [dcl.init]§11.6¶8:

    To value-initialize an object of type T means:
    — if T is a (possibly cv-qualified) class type (Clause 12) with either no default constructor (15.1) or a default constructor that is user-provided or deleted, then the object is default-initialized;

    C has a user-provided constructor, so it's default-initialized. Now what does default-initialized mean?

    [dcl.init]§11.6¶7:

    To default-initialize an object of type T means:
    — If T is a (possibly cv-qualified) class type (Clause 12), constructors are considered. The applicable constructors are enumerated (16.3.1.3), and the best one for the initializer () is chosen through overload resolution (16.3). The constructor thus selected is called, with an empty argument list, to initialize the object.

    There's only one default constructor , it gets called and initializes the C object with i == 1.

    You can explore this question further on C++ Insights or Compiler Explorer!
    */

    struct C
    {
        C() : i(1) {}
        int i;
    };

    struct D
    {
        D() : i(2) {}
        int i;
    };

    void check()
    {
        const std::variant<C, D> v;
        std::visit([](const auto& val) { std::cout << val.i; }, v);
    }
}

namespace number_of_bits_in_type
{
    /*
        Although this program probably outputs 8 on your computer, it's not guaranteed to do so by the standard. 
        All we can be sure of is what's specified in [basic.fundamental]§6.9.1¶1:
        "Objects declared as characters (char) shall be large enough to store any member of the implementation’s basic character set. 
        (...) A char, a signed char, and an unsigned char occupy the same amount of storage."
        So unsigned char is the same size as char, which is large enough to store any member of the implementation’s basic character set - but not necessarily 8.
    */
    void check() {
        std::cout << std::numeric_limits<unsigned char>::digits;
    }
}

namespace operations_order
{
    /*
        Integer division by zero is undefined behaviour. According to [expr.mul]§8.6¶4 in the standard: "If the second operand of / or % is zero the behavior is undefined."
    */
    void check()
    {
        int i = 42;
        int j = 1;
        std::cout << i / --j;
    }
}

namespace lambda_capture_by_value_rules
{
    void check()
    {
        int i, & j = i;
        [=]
            {
                cout << is_same<decltype    ((j)), int         >::value
                    << is_same<decltype   (((j))), int&  >::value
                    << is_same<decltype  ((((j)))), int const&  >::value
                    << is_same<decltype (((((j))))), int&& >::value
                    << is_same<decltype((((((j)))))), int const&& >::value;
            }();
    }
}

//118
namespace function_overloading
{
    /*
    (a) Because 0 is a null pointer constant[1], it can be converted implicitly into any pointer type with a single conversion.
    (b) Because 0 is of type int, it can be converted implicitly to a short with a single conversion too.
    In our case, both are standard conversion sequences with a single conversion of "conversion rank". Since no function is better than the other, the call is ill-formed.
    */
    void print9(char const* str) { std::cout << str; }
    void print9(short num) { std::cout << num; }

    void check() {
        print9("abc");
       // print9(0); // <----- uncomment here
        print9('A');
    }
}

//30
namespace function_prototype_problem
{
    /*
    This program has no output.
    X x(); is a function prototype, not a variable definition. 
    Remove the parentheses (or since C++11, replace them with {}), 
    and the program will output X.
    */
    struct X {
        X() { std::cout << "X"; }
    };

    void check() { X x(); }
}

//160
namespace inheritance_2
{
    /*
    On the next line, we call b->foo(), where b has the static type A, and the dynamic type B. 
    Since foo() is virtual, the dynamic type of b is used to ensure B::foo() gets called rather 
    than A::foo().
    However, which default argument is used for the int a parameter to foo()? [dcl.fct.default]§11.3.6¶10 in the standard:
    A virtual function call (§13.3) uses the default arguments in the declaration of the virtual function determined by the static type of the pointer or reference denoting the object. 
    An overriding function in a derived class does not acquire default arguments from the function it overrides.
    */
    struct A {
        virtual void foo(int a = 1) {
            std::cout << "A" << a;
        }
    };

    struct B : A {
        virtual void foo(int a = 2) {
            std::cout << "B" << a;
        }
    };

    void check() {
        A* b = new B;
        b->foo();
    }
}

//131
namespace special_member_functions_5
{
    /*
    classic example for explicit.
    */
    class C {
    public:
        explicit C(int) {
            std::cout << "i";
        };
        C(double) {
            std::cout << "d";
        };
    };

    void check() {
        C c1(7);
        C c2 = 7;
    }
}

//147
// didn't understand
namespace type_conversion_11
{
    /*
    ??/ is a trigraph which doesn't exist anymore in C++17, and as it is in a comment,
    it is ignored (as anything else).
    So the output is 1.
    */
    void check() {
        int x = 0; //What is wrong here??/
        x = 1;
        std::cout << x;
    }
}

//112
namespace special_member_functions_6
{
    /*
    First, b1 is default initialized. All members are initialized before the body of the constructor, so b1.a is default initialized first, and we get the output 14.
    [class.base.init]§15.6.2¶9 in the standard: "In a non-delegating constructor, if a given potentially constructed subobject designated by a
    mem-initializer-id (...) then if the entity is a non-static data member that has a default member initializer (§12.2), (...) the entity is initialized as specified in §11.6 (...) otherwise, the entity is default-initialized."
    Then, b2 is initialized with the move construcor (since std::move(b1)converts the reference to b1 to an xvalue, allowing it to be moved from.) In B's move constructor, a is initialized in the member initializer list. Even though b is an rvalue reference (and bound to an rvalue), b itself is an lvalue, and cannot be moved from. b2.a is then copy initialized, printing 2, 
    and finally the body of B's move constructor prints 6.
    (If the concept of rvalue references being lvalues is confusing, read http://isocpp.org/blog/2012/11/universal-references-in-c11-scott-meyers . Search for "In widget".)
    */
    struct A
    {
        A() { std::cout << "1"; }
        A(const A&) { std::cout << "2"; }
        A(A&&) { std::cout << "3"; }
    };

    struct B
    {
        A a;
        B() { std::cout << "4"; }
        B(const B& b) : a(b.a) { std::cout << "5"; }
        B(B&& b) : a(b.a) { std::cout << "6"; }
    };

    void check()
    {
        B b1;
        B b2 = std::move(b1);
    }
    // Didn't understand why a is copied.
}

//157
namespace typeid_1
{
    /*
    [expr.typeid]§8.2.8¶1: "The result of a typeid expression is an lvalue of static type const std::type_info",
    and
    [expr.unary.op]§8.3.1¶3: "The result of the unary & operator is a pointer to its operand",
    so we're comparing two pointers to const std::type_info.
    There is no guarantee that the same std::type_info instance will be referred to by all evaluations of the typeid expression on the same type, although std::type_info::hash_code of those type_info objects would be identical, as would be their std::type_index.
    (For more info on hash_code(), see [type.info]§21.7.2¶7 : "hash_code() (...)shall return the same value for any two type_info objects which compare equal")
    (For more info on type_index equality, see [type.index.members]§23.18.3 and [type.info]§21.7.2)
    */
    struct A {};
    void check()
    {
        std::cout << (&typeid(A) == &typeid(A));
    }
}

//121
namespace type_conversion_3
{
    /*
    The comma operator is applied on two expressions: a and b.
    According to [expr.comma]§8.19¶1 in the standard: 
    "A pair of expressions separated by a comma is evaluated left-to-right;
    the left expression is a discarded-value expression (...) 
    The type and value of the result are the type and value of the right operand".
    The right operand here being b, with the value 20. 
    This is then the resulting value of the expression (a, b), and 20 is assigned to x.
    */
    void check() {
        int a = 10;
        int b = 20;
        int x;
        x = (a, b);
        std::cout << x;
    }
}

//44
namespace inheritance_3
{
    /*
    arr is an array of X, not of pointers to X. 
    When an object of type Y is stored in it, 
    it is converted to X, and we lose the "Y part" of the object. 
    This is commonly known as "slicing".
    */
    struct X {
        virtual void f() const { std::cout << "X"; }
    };

    struct Y : public X {
        void f() const { std::cout << "Y"; }
    };

    void print9(const X& x) { x.f(); }

    void check() {
        X arr[1];
        Y y1;
        arr[0] = y1;
        print9(y1);
        print9(arr[0]);
    }
}

//119
namespace pointer
{
    /*
    As defined in [basic.scope.pdecl]§6.3.2¶1, the point of name declaration is after its
    complete declarator and before its initialisation. This
    means that line 4 is valid C++, because it's possible
    to initialise the variable p with the address of an existing
    variable, even if it is its own address.
    The value of p is unknown, but can not be a null pointer value. The
    cast must thus evaluate to 1 and initialise the temporary
    bool as true.
    */
    void check()
    {
        void* p = &p;
        std::cout << bool(p);
    }
}

//37 
namespace type_conversion_4
{
    /*
    According to [dcl.type.simple]§10.1.7.24 in the C++ standard:
    "The type denoted by decltype(e) is deﬁned as follows:
    — if e is an unparenthesized id-expression [...], 
    decltype(e) is the type of the entity named by e."
    The type of a is int, so the type of b is also int.
    */
    void check() {
        int a = 0;
        decltype(a) b = a;
        b++;
        std::cout << a << b;
    }
}

//15
namespace instantiation
{
    /*
    Static local variables are initialized the first time control passes through their declaration. 
    The first time foo() is called, b is attempted initialized. 
    Its constructor is called, which first constructs all member variables. 
    This means A::A() is called, printing a. A::A() then throws an exception, 
    the constructor is aborted, and neither b or B::a are actually considered constructed. 
    In the catch-block, c is printed, and then foo() is called again. 
    Since b was never initialized the first time, it tries again, this time succeeding, printing ab. 
    When main() exits, the static variable b is destroyed, 
    first calling the destructor printing B, and then destroying member variables, printing A.
    */
    int x = 0;

    class A {
    public:
        A() {
            std::cout << 'a';
            if (x++ == 0) {
                throw std::exception();
            }
        }
        ~A() { std::cout << 'A'; }
    };

    class B {
    public:
        B() { std::cout << 'b'; }
        ~B() { std::cout << 'B'; }
        A a;
    };

    void foo() { static B b; }

    void check() {
        try {
            foo();
        }
        catch (std::exception&) {
            std::cout << 'c';
            foo();
        }
    }

}

//188
namespace string_2
{
    /*
    Modifying string literals is undefined behavior. 
    In practice, the implementation can for instance store the string literal in read-only memory, 
    such as the code segment. Two string literals might even be stored in overlapping memory. 
    So allowing them to be modified is clearly not a good idea.
    */
    void check() {
        char* a = const_cast<char*>("Hello");
        a[4] = '\0';
        std::cout << a;
    }
}

//125
namespace templates_2
{
    /*
    Each function template specialization instantiated from a template has its own copy of any static variable.
    This means we get two instantiations of f, one for T=int, and one for T=double. Thus, i is shared between the two int calls, but not with the double call.
    */
    template <class T> void f(T) {
        static int i = 0;
        cout << ++i;
    }

    void check() {
        f(1);
        f(1.0);
        f(1);
    }
}

//132
namespace functions
{
    /*
    Is foo called both times or just once? The C++ standard says this in [dcl.fct.default]§11.3.6¶9: "A default argument is evaluated each time the function is called with no argument for the corresponding parameter."
    Thus, foo is called twice.
    */
    int foo() {
        cout << 1;
        return 1;
    }

    void bar(int i = foo()) {}

    void check() {
        bar();
        bar();
    }
}

//144
namespace type_conversion_8
{
    /*
    As the else part of the branch is obvious, we concentrate on the if part and make the assumptions present in the condition.
    [lex.icon]§5.13.2 in the standard: "The type of an integer literal is the first of the corresponding list in Table 7 in which its value can be represented." [Table 7: int, unsigned int, long int, unsigned long int ... for hexadecimal literals --end Table]"
    Since the literal 0xffffffff needs 32 digits, it can be represented as an unsigned int but not as a signed int, and is of type unsigned int. But what happens with the negative of an unsigned integer?
    [expr.unary.op]§8.3.1¶8 in the standard: "The negative of an unsigned quantity is computed by subtracting its value from 2^n, where n is the number of bits in the promoted operand." Here n is 32, and we get:
    2^32 - 0xffffffff = 4294967296 - 4294967295 = 1
    So i is initialised to 1, and N[1] is the only element accessed in the loop. (The second time around the loop, i is 0, which evaluates to false, and the loop terminates.)
    */
    void check()
    {
        /*
        int N[] = { 0,0,0 };

        if (std::numeric_limits<long int>::digits == 63 &&
            std::numeric_limits<int>::digits == 31 &&
            std::numeric_limits<unsigned int>::digits == 32)
        {
            for (long int i = -0xffffffff; i; --i)
            {
                N[i] = 1;
            }
        }
        else
        {
            N[1] = 1;
        }

        std::cout << N[0] << N[1] << N[2];
        */
    }
}

//291
namespace static_variable
{
    /*
    The standard defines some identifiers as reserved to the implementation.
    [lex.name]§5.10¶3:
    In addition, some identifiers are reserved for use by C++ implementations and shall not be used otherwise; no diagnostic is required.
    Each identifier that contains a double underscore __ or begins with an underscore followed by an uppercase letter is reserved to the implementation for any use.
    Each identifier that begins with an underscore is reserved to the implementation for use as a name in the global namespace.
    */
    int _global = 1;

    void check() {
        std::cout << _global;
    }
}

//178 
namespace operators
{
    /*
    Some might expect the lexer to parse the series of characters a+++++b as follows:
    a++ + ++b
    but according to the maximal munch principle, the lexer will take the longest sequence of characters to produce the next token(with a few exceptions).
    [lex.pptoken]§5.4¶3 in the C++ standard:
    "the next preprocessing token is the longest sequence of characters that could constitute a preprocessing token, even if that would cause further lexical analysis to fail (...)."
    So after parsing a++, it is not allowed to just parse +, it has to parse ++. The sequence is thus parsed as:
    a ++ ++ + b
    which is ill-formed since post-increment requires a modifiable lvalue but the first post-increment will produce a prvalue, as per [expr.post.incr]§8.2.6¶1 in the C++ standard:
    "The value of a postfix ++ expression is the value of its operand. (...) The result is a prvalue."
    */
    void check() {
        int a = 5, b = 2;
     //   std::cout << a+++++b;
    }
}

//105
namespace goto_label
{
    /*
    The Standard says this about jump statements:
    [stmt.jump]§9.6¶2 Transfer [...] back past an initialized variable with automatic storage duration 
    involves the destruction of objects with automatic storage duration 
    that are in scope at the point transferred from but not at the point transferred to.
    */
    class A {
    public:
        A() { cout << "a"; }
        ~A() { cout << "A"; }
    };

    int i = 1;

    void check() {
    label:
        A a;
        if (i--)
            goto label;
    }
}

//186
namespace pointer_3
{
    /*
    Functions taking pointers can also be called with arrays, and vice versa. So are arrays and pointers the same? No.
    If they aren't the same, why can both functions be called with both arguments?
    First let's look at takes_pointer(array);. What happens here is usually referred to as the array "decaying" to a pointer. To be a bit more precise, let's have a look at [conv.array]§7.2 in the C++ standard:
    "An lvalue or rvalue of type “array of N T” or “array of unknown bound of T” can be converted to a prvalue of type “pointer to T”."
    array is of type "array of 1 int", which converts to a prvalue (temporary) of type "pointer to int".
    So what happens with takes_array(pointer);, does the pointer convert to an array? No, it's actually the other way around. Let's look at [dcl.fct]§11.3.5¶5 about function parameters:
    "After determining the type of each parameter, any parameter of type “array of T” (...) is adjusted to be “pointer to T”".
    So in void takes_array(int array[]), the type of array is adjusted to be pointer to int.
    */
    void takes_pointer(int* pointer) {
        if (typeid(pointer) == typeid(int[])) std::cout << 'a';
        if (typeid(pointer) == typeid(int*)) std::cout << 'p';
    }

    void takes_array(int array[]) {
        if (typeid(array) == typeid(int[])) std::cout << 'a';
        if (typeid(array) == typeid(int*)) std::cout << 'p';
    }

    void check() {
        int* pointer = nullptr;
        int array[1];

        takes_pointer(array);
        takes_array(pointer);

        std::cout << (typeid(int*) == typeid(int[]));
    }
}

//187
namespace special_member_functions_10
{
    /*
    On the first line, C c1;, the object c1 is default initialized, so the default constructor is called, printing 1.
    On the second line, C c2 = c1;, the object c2 is copy initialized, so the copy constructor is called, printing 2. (Note that no assignment is taking place, even though there's a = involved.)
    Let's have a look at the C++ standard:
    [dcl.init]§11.6¶15:"The initialization that occurs in the form of a brace-or-equal-initializer (...) is called copy-initialization."
    The initialization of C c2 = c1; is the "equal" part of a braced-or-equal-initializer, so we have copy-initialization.
    */
    struct C {
        C() { std::cout << "1"; }
        C(const C& other) { std::cout << "2"; }
        C& operator=(const C& other) { std::cout << "3"; return *this; }
    };

    void check() {
        C c1;
        C c2 = c1;
    }
}
//188

// https://stackoverflow.com/questions/2354210/can-a-class-member-function-template-be-virtual#:~:text=No%2C%20template%20member%20functions%20cannot%20be%20virtual.&text=In%20the%20other%20answers%20the,common%20interface%20for%20different%20classes.
// template member functions can not be virtual

// c++ memory model
// https://en.cppreference.com/w/cpp/language/memory_model


// automatic, dynamic and static memory
// allocators, RAII
// static memory initializes memory by zero
// https://tproger.ru/articles/memory-model/

// Processor reordering in x86 (loads and stores description) http://www.cs.cmu.edu/~410-f10/doc/Intel_Reordering_318147.pdf

// Addembler commands https://www.cs.virginia.edu/~evans/cs216/guides/x86.html#:~:text=mov%20%E2%80%94%20Move%20(Opcodes%3A%2088,i.e.%20a%20register%20or%20memory).

// C++ memory model: atomic operaions, exclusive memory access, instructions reordering, sequential consistency, 
// memory models, volatile and compilation, memory ordering, SC-DRF programs, strong memeory model, weak memory model, 
// strong and relaxed memory barriers, why ARM processor can cause problems that are absent on x86, 
// thread-safe constructor
// https://habr.com/ru/company/jugru/blog/541362/

// CPU load and store https://chortle.ccsu.edu/AssemblyTutorial/Chapter-15/ass15_2.html#:~:text=A%20load%20operation%20copies%20data,a%20register%20into%20main%20memory%20.

// About internals of mutex: https://stackoverflow.com/questions/11770571/how-do-mutexes-really-work#:~:text=The%20idea%20behind%20mutexes%20is,until%20the%20first%20one%20unlocks.&text=To%20lock%20itself%2C%20the%20mutex,says%20that%20it%20is%20locked.
// About mutex again. compare-and-set CPU function. 
// Problem of system calls and process schedulers.
// Linux futex operation. 
// Use-cases of using mutex.
// Not only do you suffer the context switch overhead, the kernel now spends some time in its scheduling code.
// https://mortoray.com/2019/02/20/how-does-a-mutex-work-what-does-it-cost/

// How to avoid synchronization: exclusive memory for each thread, read-only common data. https://habr.com/ru/post/517918/

// memory synchronization ways in c++: relaxed, release/acquire, sequential consistency.
// relaxed: only atomicity: ref counters, completion flags, but not locks.
// sequential consistency: good work but can be too expensive for multicore processors. Especially with ARM architecture. This is by default.
// release/acquire https://habr.com/ru/post/517918/

// double check singleton anti-pattern that will fail on ARM where partial initialization is possible.
// flag can be initialized before new operation and segmentation fault can occur.
// https://habr.com/ru/post/517918/
#include <mutex>
namespace double_check_singleton_antipattern {
    struct Singleton {
        // ...
    };

    static Singleton* singleton = nullptr;
    static std::mutex mtx;
    static bool initialized = false;

    void lazy_init() {
        if (initialized) // early return to avoid touching mutex every call
            return;

        std::unique_lock<mutex> l(mtx); // `mutex` locks here (acquire memory)
        if (!initialized) {
            singleton = new Singleton();
            initialized = true;
        }
        // `mutex` unlocks here (release memory)
    }
}

// std::thread::(constructor), std::thread::join, std::mutext::lock, unlock, std::promise::set_value/std::future::wait
// https://habr.com/ru/post/517918/

// C++ memory model: https://en.cppreference.com/w/cpp/language/memory_model

// evaluation order: https://en.cppreference.com/w/cpp/language/eval_order
// Order of evaluation of any part of any expression, including order of evaluation of function arguments 
// is unspecified (with some exceptions listed below). The compiler can evaluate operands and other subexpressions in any order,
// and may choose another order when the same expression is evaluated again.
//...
//There is no concept of left-to-right or right-to-left evaluation in C++. 
// This is not to be confused with left-to-right and right-to-left associativity of operators: 
// the expression a() + b() + c() is parsed as (a() + b()) + c() due to left-to-right associativity of operator+,
// but the function call to c may be evaluated first, last, or between a() or b() at run time
// ...
// 
namespace evaluation_order {
#include <cstdio>
    int a() { return std::puts("a"); }
    int b() { return std::puts("b"); }
    int c() { return std::puts("c"); }
    void z(int, int, int) {}
    void check() {
        z(a(), b(), c());       // all 6 permutations of output are allowed
        auto d = a() + b() + c(); // all 6 permutations of output are allowed
    }

    int f(int i1, int i2) { return i1 + i2; }

    void check2()
    {
        int i = 1;
        i = ++i + 2;       // undefined behavior until C++11
        i = i++ + 2;       // undefined behavior until C++17
        f(i = -2, i = -2); // undefined behavior until C++17
        f(++i, ++i);       // undefined behavior until C++17, unspecified after C++17
        i = ++i + i++;     // undefined behavior

        cout << i << i++; // undefined behavior until C++17
       // a[i] = i++;       // undefined behavior until C++17
       // n = ++i + i;      // undefined behavior
    }
}

// RELAXED-ORDERING
// https://en.cppreference.com/w/cpp/atomic/memory_order
// sequenced before, carries dependency, modification order of atomic: 4 types of coherence,
// release sequence headed by A, dependency-ordered, inter-thread happens-before, happens before,
// data race, simply happens before, strongly happens before, release/acquire/consume operations, 
// 
#include <atomic>
namespace relaxed_memory_ordering
{
     // Atomic operations tagged memory_order_relaxed are not synchronization operations; 
    // they do not impose an order among concurrent memory accesses.
    // They only guarantee atomicityand modification order consistency.
    // For example, with xand y initially zero,

    void check()
    {
        std::atomic<bool> x, y;
        // Thread 1:
        bool r1 = y.load(std::memory_order_relaxed); // A
        x.store(r1, std::memory_order_relaxed); // B
        // Thread 2:
        auto r2 = x.load(std::memory_order_relaxed); // C 
        y.store(42, std::memory_order_relaxed); // D
    }
    //is allowed to produce r1 == r2 == 42 because, 
    //although A is sequenced - before B within thread 1 and C is sequenced before D within thread 2, 
    // nothing prevents D from appearing before A in the modification order of y, 
    // and B from appearing before C in the modification order of x.
    // The side - effect of D on y could be visible to the load A in thread 1 
    // while the side effect of B on x could be visible to the load C in thread 2. 
    // In particular, this may occur if D is completed before C in thread 2, either due to compiler reordering or at runtime.

    void check2() {
        // Even with relaxed memory model, out - of - thin - air values are not allowed to circularly depend on their own computations,
        // for example, with xand y initially zero,

        std::atomic_int x, y;
        // Thread 1:
        auto r1 = y.load(std::memory_order_relaxed);
        if (r1 == 42) x.store(r1, std::memory_order_relaxed);
        // Thread 2:
        auto r2 = x.load(std::memory_order_relaxed);
        if (r2 == 42) y.store(42, std::memory_order_relaxed);
        // is not allowed to produce r1 == r2 == 42 since the store of 42 to y is only possible if the store to x stores 42, 
        // which circularly depends on the store to y storing 42. 
        // Note that until C++14, this was technically allowed by the specification, but not recommended for implementors.
    }

    // Typical use for relaxed memory ordering is incrementing counters, 
    // such as the reference counters of std::shared_ptr, since this only requires atomicity, but not ordering or synchronization 
    // (note that decrementing the shared_ptr counters requires acquire-release synchronization with the destructor)
    std::atomic<int> cnt = { 0 };

    void f()
    {
        for (int n = 0; n < 1000; ++n) {
            cnt.fetch_add(1, std::memory_order_relaxed);
        }
    }

    void check3()
    {
        std::vector<std::thread> v;
        for (int n = 0; n < 10; ++n) {
            v.emplace_back(f);
        }
        for (auto& t : v) {
            t.join();
        }
        std::cout << "Final counter value is " << cnt << '\n';
    }
}

// RELEASE-ACQUIRE ORDERING
// https://en.cppreference.com/w/cpp/atomic/memory_order
// If an atomic store in thread A is tagged memory_order_release 
// and an atomic load in thread B from the same variable is tagged memory_order_acquire, 
// all memory writes (non-atomic and relaxed atomic) that happened-before the atomic store from the point of view of thread A, 
// become visible side-effects in thread B. That is, once the atomic load is completed, 
// thread B is guaranteed to see everything thread A wrote to memory.
// The synchronization is established only between the threads releasingand acquiring the same atomic variable.
// Other threads can see different order of memory accesses than either or both of the synchronized threads.
// On strongly - ordered systems — x86, SPARC TSO, IBM mainframe, etc.— release - acquire ordering i
// s automatic for the majority of operations.
// No additional CPU instructions are issued for this synchronization mode; 
// only certain compiler optimizations are affected
// (e.g., the compiler is prohibited from moving non - atomic stores past the atomic store - release 
// or performing non - atomic loads earlier than the atomic load - acquire).
// On weakly - ordered systems(ARM, Itanium, PowerPC), special CPU load or memory fence instructions are used.
// Mutual exclusion locks, such as std::mutex or atomic spinlock, 
// are an example of release - acquire synchronization : 
// when the lock is released by thread A and acquired by thread B, 
// everything that took place in the critical section(before the release) in the context of thread A 
// has to be visible to thread B(after the acquire) which is executing the same critical section.
#include <cassert>
namespace release_acquire {
    std::atomic<std::string*> ptr;
    int data;

    void producer()
    {
        std::string* p = new std::string("Hello");
        data = 42;
        ptr.store(p, std::memory_order_release);
    }

    void consumer()
    {
        std::string* p2;
        while (!(p2 = ptr.load(std::memory_order_acquire)))
            ;
        assert(*p2 == "Hello"); // never fires because *p2 is always "Hello"
        assert(data == 42); // never fires because data is always 42
    }

    void check()
    {
        std::thread t1(producer);
        std::thread t2(consumer);
        t1.join(); t2.join();
    }

    // The following example demonstrates transitive release - acquire ordering across three threads

    std::vector<int> vData;
    std::atomic<int> flag = { 0 };

    void thread_1()
    {
        vData.push_back(42);
        flag.store(1, std::memory_order_release);
    }

    void thread_2()
    {
        int expected = 1;
        while (!flag.compare_exchange_strong(expected, 2, std::memory_order_acq_rel)) {
            expected = 1;
        }
    }

    void thread_3()
    {
        while (flag.load(std::memory_order_acquire) < 2)
            ;
        assert(vData.at(0) == 42); // will never fire
    }

    void check2()
    {
        std::thread a(thread_1);
        std::thread b(thread_2);
        std::thread c(thread_3);
        a.join(); b.join(); c.join();
    }
}

// RELEASE-CONSUMING ORDERING
// https://en.cppreference.com/w/cpp/atomic/memory_order
// If an atomic store in thread A is tagged memory_order_release and an atomic load in thread B from the same variable 
// that read the stored value is tagged memory_order_consume, all memory writes (non-atomic and relaxed atomic)
// that happened-before the atomic store from the point of view of thread A,
// become visible side-effects within those operations in thread B into which the load operation carries dependency, 
// that is, once the atomic load is completed, 
// those operators and functions in thread B that use the value obtained from the load 
// are guaranteed to see what thread A wrote to memory.
// The specification of release-consume ordering is being revised, and the use of memory_order_consume is temporarily discouraged.
// Note that currently (2/2015) no known production compilers track dependency chains: consume operations are lifted to acquire operations.
// Difference with release-acquire is that in release-acquire all memory changes before release 
// are guaranted to be visible to the thread with that loads data with acquire.
namespace release_consume_ordering {
    // This example demonstrates dependency-ordered synchronization for pointer-mediated publication: 
    // the integer data is not related to the pointer to string by a data-dependency relationship, 
    // thus its value is undefined in the consumer.
    std::atomic<std::string*> ptr;
    int data;

    void producer()
    {
        std::string* p = new std::string("Hello");
        data = 42;
        ptr.store(p, std::memory_order_release);
    }

    void consumer()
    {
        std::string* p2;
        while (!(p2 = ptr.load(std::memory_order_consume)))
            ;
        assert(*p2 == "Hello"); // never fires: *p2 carries dependency from ptr
        assert(data == 42); // may or may not fire: data does not carry dependency from ptr
    }

    void check()
    {
        std::thread t1(producer);
        std::thread t2(consumer);
        t1.join(); t2.join();
    }
}

// SEQUENTIALY-CONSISTENT ORDERING
// Atomic operations tagged memory_order_seq_cst not only order memory the same way as release/acquire ordering 
// (everything that happened-before a store in one thread becomes a visible side effect in the thread that did a load), 
// but also establish a single total modification order of all atomic operations that are so tagged.
namespace sequentialy_consistent_ordering {
    // The single total order might not be consistent with happens-before. 
    // This allows more efficient implementation of memory_order_acquire and memory_order_release on some CPUs. 
    // can produce surprising results when memory_order_acquire and memory_order_release are mixed with memory_order_seq_cst.
    // For example, with xand y initially zero,
    void temp() {
        std::atomic_int x, y;
        // Thread 1:
        x.store(1, std::memory_order_seq_cst); // A
        y.store(1, std::memory_order_release); // B
        // Thread 2:
        auto r1 = y.fetch_add(1, std::memory_order_seq_cst); // C
        auto r2 = y.load(std::memory_order_relaxed); // D
        // Thread 3:
        y.store(3, std::memory_order_seq_cst); // E
        auto r3 = x.load(std::memory_order_seq_cst); // F
    }
    // is allowed to produce r1 == 1 && r2 == 3 && r3 == 0, 
    // where A happens-before C, but C precedes A in the single total order C-E-F-A of memory_order_seq_cst (see Lahav et al).
    // Note that :
    // 1) as soon as atomic operations that are not tagged memory_order_seq_cst enter the picture, 
    // the sequential consistency guarantee for the program is lost
    // 2) in many cases, memory_order_seq_cst atomic operations are reorderable with respect to other atomic 
    // operations performed by the same thread

   //  This example demonstrates a situation where sequential ordering is necessary.
   // Any other ordering may trigger the assert because it would be possible for the threads 
   // c and d to observe changes to the atomics xand y in opposite order.
    std::atomic<bool> x = { false };
    std::atomic<bool> y = { false };
    std::atomic<int> z = { 0 };

    void write_x()
    {
        x.store(true, std::memory_order_seq_cst);
    }

    void write_y()
    {
        y.store(true, std::memory_order_seq_cst);
    }

    void read_x_then_y()
    {
        while (!x.load(std::memory_order_seq_cst))
            ;
        if (y.load(std::memory_order_seq_cst)) {
            ++z;
        }
    }

    void read_y_then_x()
    {
        while (!y.load(std::memory_order_seq_cst))
            ;
        if (x.load(std::memory_order_seq_cst)) {
            ++z;
        }
    }

    void main()
    {
        std::thread a(write_x);
        std::thread b(write_y);
        std::thread c(read_x_then_y);
        std::thread d(read_y_then_x);
        a.join(); b.join(); c.join(); d.join();
        assert(z.load() != 0);  // will never happen
    }
}

// VOLATILE
// https://en.cppreference.com/w/cpp/atomic/memory_order
// Within a thread of execution, accesses (reads and writes) through volatile 
// glvalues cannot be reordered past observable side-effects (including other volatile accesses) 
// that are sequenced-before or sequenced-after within the same thread, 
// but this order is not guaranteed to be observed by another thread, 
// since volatile access does not establish inter-thread synchronization.
// In addition, volatile accesses are not atomic(concurrent readand write is a data race) 
// and do not order memory(non - volatile memory accesses may be freely reordered around the volatile access).
// One notable exception is Visual Studio, where, with default settings, 
// every volatile write has release semanticsand every volatile read has acquire semantics(Microsoft Docs), 
// and thus volatiles may be used for inter - thread synchronization.
// Standard volatile semantics are not applicable to multithreaded programming, 
// although they are sufficient for e.g.communication with a std::signal 
// handler that runs in the same thread when applied to sig_atomic_t variables.

/** C++ PROGRAM MEMORY (STATIC)
* 
* https://habr.com/ru/post/527044/
* 
* .data, .bss, stack, heap, text memory.
* Static data is initialized once and exist by the end of the program.
* 
* static usage: var inside function, class member, static object, static class function, usual static function.
* 
* 1. Static member function variables are saved inside .data or .bss but not in stack.
* 2. Static obejct behaves the same as static var.
* 3. Static class member initialization occurs not in constructor but earlier.
* 4. Static member of the class is common for all objects.
* 5. Static function is visible only in cpp file where it is defined. By default all functions are global and two 
* functions with the same name in two cpp files will cause problem. It is bad idea to define static function in header file.
* Each file that will include this function will have its definition. Conflict.
* 6. Static class functions are called without any objects.
* 7. Static vars are initialized by default as far as I know from my reviews (find documentation).
* 
* Disadvantages (check them in other origins):
* 1. often it is more effective to create local var in stack then go to the static segment of the memory from hardware point of view.
* 2. static vars are common for all threads and this can cause problems with synchronization.
*/
namespace static_object {
    class Base { // строка 3
    public:
        Base() { // строка 5
            std::cout << "Constructor" << std::endl;
        }
        ~Base() { // строка 8
            std::cout << "Destructor" << std::endl;
        }
    };

    void foo() {
        static Base obj; // строка 14
    } // строка 15

    void check() {
        foo(); // строка 18
        std::cout << "End of main()" << std::endl;
    }
}
namespace static_class_member {
    class A { // строка 3
    public:
        A() { std::cout << "Constructor A" << std::endl; }
        ~A() { std::cout << "Destructor A" << std::endl; }
    };

    class B { // строка 9
    public:
        B() { std::cout << "Constructor B" << std::endl; }
        ~B() { std::cout << "Destructor B" << std::endl; }

    private:
        static A a; // строка 15 (объявление)
    };

    int check() {
        B b; // строка 19
        return 0;
    }
    // output will be: Constructor B Desctructor B
    // because A is not defined.

    /** here output will be:
    * Constructor A
    * Constructor C
    * Desctructor C
    * Desctructor A
    */

    class C {
    public:
        C() { std::cout << "Constructor C" << std::endl; }
        ~C() { std::cout << "Destructor C" << std::endl; }

    private:
        static A a; // строка 15 (объявление)
    };

    A C::a; // строка 18 (определение)

    int main() {
        C c;
        return 0;
    }
}

/** DYNAMIC MEMORY C++ 
* 
* https://en.cppreference.com/w/cpp/memory
* 
* SMART POINTERS
* shared_ptr, unique_ptr, weak_ptr, enable_shared_from_this, owner_less, bad_weak_ptr (exception), default_delete (default unique_ptr deleter).
* 
* ALLOCATORS
* 
* MEMORY RESOURCES (since C++17) - intbu
* UNINITIALIZED STORAGE - intbu
* UNINITIALIZED MEMORY ALGORITHMS - intbu
* CONSTRAINED UNITIALIZED MEMORY ALGORITHMS - intbu
* GARBAGE COLLECTOR SUPPORT - intbu
* 
* C-STYLE MEMORY MANAGEMENT (std::malloc, std::free)
* 
* C++ LOW LEVEL ALLOCATION (new, delete)
* 
* https://www.cplusplus.com/doc/tutorial/dynamic/
* But there may be cases where the memory needs of a program can only be determined during runtime. 
* For example, when the memory needed depends on user input. 
* On these cases, programs need to dynamically allocate memory, 
* for which the C++ language integrates the operators new and delete.
* 
* There is a substantial difference between declaring a normal array and allocating dynamic memory for a block of memory using new. 
* The most important difference is that the size of a regular array needs to be a constant expression, 
* and thus its size has to be determined at the moment of designing the program, before it is run, 
* whereas the dynamic memory allocation performed by new allows to assign memory during runtime using any variable value as size.
* 
* The dynamic memory requested by our program is allocated by the system from the memory heap. 
* However, computer memory is a limited resource, and it can be exhausted. 
* Therefore, there are no guarantees that all requests to allocate memory using operator new 
* are going to be granted by the system. nothrow, bad_alloc.
* 
* This nothrow method is likely to produce less efficient code than exceptions, 
* since it implies explicitly checking the pointer value returned after each and every allocation. 
* Therefore, the exception mechanism is generally preferred, at least for critical allocations. 
* Still, most of the coming examples will use the nothrow mechanism due to its simplicity.
* 
* Note, though, that the memory blocks allocated by these functions 
* are not necessarily compatible with those returned by new, 
* so they should not be mixed; each one should be handled with its own set of functions or operators.
* 
*/
namespace default_allocator {
    // https://en.cppreference.com/w/cpp/memory/allocator
    void check()
    {
        // Code below generates errors in C++17
        // Intbu
        /*
        std::allocator<int> a1;   // default allocator for ints
        int* a = a1.allocate(1);  // space for one int
        a1.construct(a, 7);       // construct the int
        std::cout << a[0] << '\n';
        a1.deallocate(a, 1);      // deallocate space for one int

        // default allocator for strings
        std::allocator<std::string> a2;

        // same, but obtained by rebinding from the type of a1
        decltype(a1)::rebind<std::string>::other a2_1;

        // same, but obtained by rebinding from the type of a1 via allocator_traits
        std::allocator_traits<decltype(a1)>::rebind_alloc<std::string> a2_2;

        std::string* s = a2.allocate(2); // space for 2 strings

        a2.construct(s, "foo");
        a2.construct(s + 1, "bar");

        std::cout << s[0] << ' ' << s[1] << '\n';

        a2.destroy(s);
        a2.destroy(s + 1);
        a2.deallocate(s, 2);
        */
    }
}


/** MEMORY MANAGEMENT
* 
* https://dev.to/deepu105/demystifying-memory-management-in-modern-programming-languages-ddd
* 
* When a software runs on a target Operating system on a computer 
* it needs access to the computers RAM(Random-access memory) to:
* load its own bytecode that needs to be executed
* store the data values and data structures used by the program that is executed
* load any run-time systems that are required for the program to execute.
* 
* When a software program uses memory there are two regions of memory they use, 
* apart from the space used to load the bytecode, Stack and Heap memory.
* 
* Due to this nature, the process of storing and retrieving data from the stack is very fast as there is no lookup required, 
* you just store and retrieve data from the topmost block on it.
* 
* But this means any data that is stored on the stack has to be finite and static
* (The size of the data is known at compile-time).
* 
* For example, every time a function declares a new variable, 
* it is "pushed" onto the topmost block in the stack. 
* Then every time a function exits, the topmost block is cleared, 
* thus all of the variables pushed onto the stack by that function, are cleared. 
* These can be determined at compile time due to the static nature of the data stored here.
* 
* Multi-threaded applications can have a stack per thread.
* 
* Memory management of the stack is simple and straightforward and is done by the OS.
* 
* Typical data that are stored on stack are local variables
* (value types or primitives, primitive constants), 
* pointers and function frames.
* 
* Heap is used for dynamic memory allocation and unlike stack, the program needs to look up the data in heap using pointers.
* 
* It is slower than stack as the process of looking up data is more involved but it can store more data than the stack.
* 
* Heap is shared among threads of an application.
* 
* Differ stack overflow errors from out of memory errors.
* 
* Generally, there is no limit on the size of the value that can be stored on the heap. 
* Of course, there is the upper limit of how much memory is allocated to the application.
* 
* Stack has limitation on maximum size of elements that can be stored in it.
* 
* Memory management types:
* 1. Garbage collection. Reference-counter is example.
* 2. RAII (Resource acquistion in initialization).
* 3. Ownership semantics of RUST.
* 
* https://www.geeksforgeeks.org/memory-layout-of-c-program/
* 
* 1. Text Segment 
* A text segment, also known as a code segment or simply as text, 
* is one of the sections of a program in an object file or in memory, which contains executable instructions.
* As a memory region, a text segment may be placed below the heap or stack in order to prevent heaps and stack overflows from overwriting it. 
* Usually, the text segment is sharable so that only a single copy needs to be in memory for frequently executed programs, such as text editors, 
* the C compiler, the shells, and so on. 
* Also, the text segment is often read-only, to prevent a program from accidentally modifying its instructions.
* 
* 2. Initialized Data Segment: 
* Initialized data segment, usually called simply the Data Segment. 
* A data segment is a portion of the virtual address space of a program, 
* which contains the global variables and static variables that are initialized by the programmer.
* Note that, the data segment is not read-only, since the values of the variables can be altered at run time.
* 
* 3. Uninitialized Data Segment: 
* Uninitialized data segment often called the “bss” segment, 
* named after an ancient assembler operator that stood for “block started by symbol.” 
* Data in this segment is initialized by the kernel to arithmetic 0 before the program starts executing
* uninitialized data starts at the end of the data segment 
* and contains all global variables and static variables 
* that are initialized to zero or do not have explicit initialization in source code.
* 
* 4. Stack: 
* The stack area traditionally adjoined the heap area 
* and grew in the opposite direction; 
* when the stack pointer met the heap pointer, free memory was exhausted. 
* (With modern large address spaces and virtual memory techniques they may be placed almost anywhere, 
* but they still typically grow in opposite directions.)
* The stack area contains the program stack, a LIFO structure, 
* typically located in the higher parts of memory. On the standard PC x86 computer architecture, 
* it grows toward address zero; on some other architectures, it grows in the opposite direction. 
* A “stack pointer” register tracks the top of the stack; it is adjusted each time a value is “pushed” onto the stack. 
* The set of values pushed for one function call is termed a “stack frame”; A stack frame consists at minimum of a return address.
* Stack, where automatic variables are stored, along with information 
* that is saved each time a function is called. Each time a function is called, 
* the address of where to return to and certain information about the caller’s environment, 
* such as some of the machine registers, are saved on the stack. 
* 
* 5. size dwm.exe - this command allows to see memory of the program.
* 
* https://habr.com/ru/post/274827/
* 
* Problems with dynamic memory:
* 
* 1. Fragmentation
* 2. Internal implementation of malloc and free can be too cost. Memory tends to improve its state but this is not what is needed.
* 3. Risk of memory leaks.
* 4. Allocated memory pieces can be too far one from another.
* 
* Allocator types:
* 1. Linear allocator.
* 2. Stack.
* 3. Bidirectional stack.
* 4. Pool (pointer to the next free memory chunk). Linked list of free chunks.
* 
* https://stackoverflow.com/questions/30158192/difference-between-stduninitialized-copy-stdcopy
* Difference between malloc and new.
* What is unitialized and initialized memory.
* std::copy and std::unitialized_copy
*/

/** MEMORY ALIGNMENT
* https://habr.com/ru/post/142662/
* 
* sizeof(structure) depends on preprocessor directives and compiler options.
* 
* Why it is necessary: https://stackoverflow.com/questions/381244/purpose-of-memory-alignment#:~:text=So%2C%20as%20everyone%20thinks%20memory,the%20cost%20of%20wasted%20memory.&text=Fundamentally%2C%20the%20reason%20is%20because,smaller%20than%20the%20memory%20size.
* 
*/
namespace memory_alignment {
    struct Foo
    {
        char m1;
        int m2;
    };

    struct Foo2
    {
        char m1;
        short m2;
        int m3;
    };

    struct Foo3
    {
        char m1;
        short m2{ 2 };
        short m3;
        int   m4;
    };

#pragma pack(push, 1)
    struct Foo4
    {
        char m1;
        short m2{ 2 };
        short m3;
        int   m4;
    };
#pragma pack(pop)

    struct Foo5
    {
        char m1;
        double m2;
    };

    struct Foo6 
    {
        char m1;
        char m2;
        double m3;
    };

    struct Foo7
    {
        char m1;
        double m2;
        char m3;
    };

    void check()
    {
        // alignment 4, size 8
        std::cout << "sizeof Foo " << sizeof(Foo) << std::endl; // 8 because [m1 .. .. .. m2 m2 m2 m2]
        
        // alignment 4, size 8
        std::cout << "sizeof Foo2 " << sizeof(Foo2) << std::endl; // 8 because [m1 .. m2 m2 m3 m3 m3 m3]

        // alignment 4, size 12
        std::cout << "sizeof Foo3 " << sizeof(Foo3) << std::endl; // 12 because [m1 .. m2 m2 m3 m3 .. .. m4 m4 m4 m4]

        // alignment 0, size 9
        std::cout << "sizeof Foo3 " << sizeof(Foo4) << std::endl; // 9 because [m1 m2 m2 m3 m3 m4 m4 m4 m4]

        // alignment 8, size 16
        std::cout << "sizeof Foo5 " << sizeof(Foo5) << std::endl; // 16 because [m1 .. .. .. .. .. .. .. m2 m2 m2 m2 m2 m2 m2 m2]

        // alignment 8, size 16
        std::cout << "sizeof Foo6 " << sizeof(Foo6) << std::endl; // 16 because [m1 m2 .. .. .. .. .. .. m2 m2 m2 m2 m2 m2 m2 m2]

        // alignment 8, size 24
        std::cout << "sizeof Foo7 " << sizeof(Foo7) << std::endl; // 16 because [m1 .. .. .. .. .. .. .. m2 m2 m2 m2 m2 m2 m2 m2 m3 .. .. .. .. .. .. ..]
    struct Foo5
    {
        char m1;
        double m2;
    };

        // packing is helpfull in object initialization
        {
            Foo3 ob3;
            const auto in3 = std::chrono::system_clock::now();
         //   for (auto i = 0; i < 5e7; ++i)
         //       const auto p = std::make_shared<Foo3>(ob3);
            const auto out3 = std::chrono::system_clock::now();
            const auto dur3 = std::chrono::duration_cast<std::chrono::milliseconds>(out3 - in3).count();

            Foo4 ob4;
            const auto in4 = std::chrono::system_clock::now();
        //    for (auto i = 0; i < 5e7; ++i)
        //        const auto p = std::make_shared<Foo4>(ob4);
            const auto out4 = std::chrono::system_clock::now();
            const auto dur4 = std::chrono::duration_cast<std::chrono::milliseconds>(out4 - in4).count();

            std::cout << "Time: Foo3=" << dur3 << " Foo4=" << dur4 << std::endl;
        }

        // packing vs not packing for arithmetic operations
        {
            long int res{ 0 };

            Foo3 ob3;
            const auto in3 = std::chrono::system_clock::now();
           // for (auto i = 0; i < 5e8; ++i)
           //     res += ob3.m2;
            const auto out3 = std::chrono::system_clock::now();
            const auto dur3 = std::chrono::duration_cast<std::chrono::milliseconds>(out3 - in3).count();

            Foo4 ob4;
            const auto in4 = std::chrono::system_clock::now();
           // for (auto i = 0; i < 5e8; ++i)
           //     res += ob4.m2;
            const auto out4 = std::chrono::system_clock::now();
            const auto dur4 = std::chrono::duration_cast<std::chrono::milliseconds>(out4 - in4).count();

            std::cout << "Time: Foo3=" << dur3 << " Foo4=" << dur4 << " res=" << res << std::endl;
        }
    }
}

/** BUS LOCK
* 
* https://scc.ustc.edu.cn/zlsc/tc4600/intel/2017.0.098/vtune_amplifier_xe/help/GUID-004AFC99-EBBA-43D5-98A9-43A97997F9B5.html
* Intel processor signal.
*/

/** PROCESSOR CACHE ORGANIZATION 
* https://medium.com/software-design/why-software-developers-should-care-about-cpu-caches-8da04355bb8a
* 2 to 64 KB Level 1 (L1) cache very high speed cache
* ~256 KB Level 2 (L2) cache medium speed cache
* All cores also share a Level 3 (L3) cache. 
* The L3 cache tends to be around 8 MB.
* 
* Performance difference between L1, L2 and L3 caches
* L1 cache access latency: 4 cycles
* L2 cache access latency: 11 cycles
* L3 cache access latency: 39 cycles
* Main memory access latency: 107 cycles
* 
* Now lets look at performance implications of the cache structure. 
* This will be particularly important for high performance computing and game development.
* Cache line — the unit of data transfer between cache and memory.
* Typically the cache line is 64 bytes. 
* The processor will read or write an entire cache line when any location in the 64 byte region is read or written. 
* The processors also attempt to prefetch cache lines by analyzing the memory access pattern of a thread.
* Cache line — the unit of data transfer between cache and memory.
* 1. Prefer arrays and vectors stored by value.
* 2. Keep array entry validity flags in separate descriptor arrays.
* 3. Avoid cache line sharing between threads (false sharing).
* 4. Iterate by rows not columns.
* Lets have another look at the CPU die. 
* Notice that L1 and L2 caches are per core. 
* The processor has a shared L3 cache. 
* This three tier cache architecture causes cache coherency issues between the caches.
* Let’s assume that the left most processor reads a memory location that results 
* in loading of Cache line 0 into the L1 cache of the left most core. 
* If (say) the right most core attempts to access a memory location that also happens to be on Cache line 0. 
* When this access takes place, the processor will have to trigger the cache coherency procedure to ensure 
* that Cache line 0 is coherently represented across all caches. 
* This is an expensive procedure that wastes a lot of memory bandwidth, thus reducing the benefit of caching memory.
* The following example shows how a caching optimized application may suffer from the cache coherency induced lag. 
* Consider the array show below that has been optimized so that two cores (blue and green) 
* are operating on alternate entries in the array.
* 
* Code caching
* 1. Loops
* Code with tight for loops is likely to fit into the L1 or L2 cache. 
* This speeds up program executing as CPU cycles are not wasted in repeatedly fetching the same instructions from main memory.
* 2. Inlining
void bar(int index)
{
    // Code Block 1
    // Code Block 2: Uses the index parameter
}
void foo()
{
   for (int i=0; i < MAX_VALUE; i++)
   {
      bar(i);
      // More code
   }
}
* If the compiler in-lines the function bar() it will remove a function call from the loop, 
* thus improving the locality of the code. As an added bonus, the compiler might also bring 
* Code Block 1 outside the loop if it finds that the computation in the code block does not change in the loop.
void foo()
{
   // Code Block 1
   for (int i=0; i < MAX_VALUE; i++)
   {
      // Code Block 2: Uses the loop variable i
      // More code
   }
}
* 
*
*/
namespace cache_line_importance
{
    void check()
    {
        const int size = 1e4;

        // access to raws is much faster then to columns. In second case each access to element requires reading of cache line.
        // Cache line usually has 64 bytes. Double is 8 bytes. So there should be 8 times less cache line loads in first case 
        // then in second case. For double vectors we have approximately 8 times difference in time. For int it is approximately 16.
        // Because int is 4 bytes.
        {
            std::vector<std::vector<double>> vec;
            vec.reserve(1e4);
            for (auto i = 0; i < size; ++i)
            {
                vec.push_back(std::vector<double>());
                for (auto j = 0; j < size; ++j)
                {
                    vec[i].push_back(static_cast<double>(std::rand()) / INT_MAX);
                }
            }
            {
                double sumRow{ 0 };
                const auto inRow = std::chrono::system_clock::now();
                for (const auto& v : vec)
                {
                    for (const auto& el : v)
                    {
                        sumRow += el;
                    }
                }
                const auto outRow = std::chrono::system_clock::now();
                const auto durRow = std::chrono::duration_cast<std::chrono::milliseconds>(outRow - inRow).count();

                double sumCol{ 0 };
                const auto inCol = std::chrono::system_clock::now();
                for (auto j = 0; j < size; ++j)
                {
                    for (auto i = 0; i < size; ++i)
                    {
                        sumCol += vec[i][j];
                    }
                }
                const auto outCol = std::chrono::system_clock::now();
                const auto durCol = std::chrono::duration_cast<std::chrono::milliseconds>(outCol - inCol).count();

                std::cout << "double sumVal=" << sumRow << " durRow=" << durRow << std::endl;
                std::cout << "double sumShared=" << sumCol << " durCol=" << durCol << std::endl;
            }
        }
        {
            std::vector<std::vector<int>> vec;
            vec.reserve(1e4);
            for (auto i = 0; i < size; ++i)
            {
                vec.push_back(std::vector<int>());
                for (auto j = 0; j < size; ++j)
                {
                    vec[i].push_back(10 * std::rand() / RAND_MAX);
                }
            }
            {
                int sumRow{ 0 };
                const auto inRow = std::chrono::system_clock::now();
                for (const auto& v : vec)
                {
                    for (const auto& el : v)
                    {
                        sumRow += el;
                    }
                }
                const auto outRow = std::chrono::system_clock::now();
                const auto durRow = std::chrono::duration_cast<std::chrono::milliseconds>(outRow - inRow).count();

                int sumCol{ 0 };
                const auto inCol = std::chrono::system_clock::now();
                for (auto j = 0; j < size; ++j)
                {
                    for (auto i = 0; i < size; ++i)
                    {
                        sumCol += vec[i][j];
                    }
                }
                const auto outCol = std::chrono::system_clock::now();
                const auto durCol = std::chrono::duration_cast<std::chrono::milliseconds>(outCol - inCol).count();

                std::cout << "int sumVal=" << sumRow << " durRow=" << durRow << std::endl;
                std::cout << "int sumShared=" << sumCol << " durCol=" << durCol << std::endl;
            }
        }


        // operations for objects stored locally (by value) are faster then for objects allocated in heap, becauses they require 
        // calls to different memory locations.
        struct Foo {
            Foo(float m1, float m2) : m1(m1), m2(m2) {}
            float m1{};
            float m2{};
        };
        std::vector<Foo> value;
        std::vector<std::shared_ptr<Foo> > shared;
        for (auto i = 0; i < 5e7; ++i)
        {
            value.push_back(Foo(std::rand(), std::rand()));
            shared.push_back(std::make_shared<Foo>(std::rand(), std::rand()));
        }
        {
            double sumVal{ 0 };
            const auto inVal = std::chrono::system_clock::now();
            for (const auto& el : value)
            {
                sumVal += el.m1;
            }
            const auto outVal = std::chrono::system_clock::now();
            const auto durVal = std::chrono::duration_cast<std::chrono::milliseconds>(outVal - inVal).count();

            double sumShared{ 0 };
            const auto inShared = std::chrono::system_clock::now();
            for (const auto& el : shared)
            {
                sumShared += el->m1;
            }
            const auto outShared = std::chrono::system_clock::now();
            const auto durShared = std::chrono::duration_cast<std::chrono::milliseconds>(outShared - inShared).count();

            std::cout << "sumVal=" << sumVal << " durVal=" << durVal << std::endl;
            std::cout << "sumShared=" << sumShared << " durShared=" << durShared << std::endl;
        }

        // Cache coherency between two processors: avoid using two cache lines in multithreading.
        {
            std::cout << std::endl << " CACHE COHERENCE ISSUE " << std::endl;
            std::vector<int> vec;
            vec.resize(1e9); // we have int vec
            // Now we want to make all values as 1.
            // Way 1. Single thread iteration.
            const auto inSingle = std::chrono::system_clock::now();
            for (auto& el : vec)
            {
                el = 1;
            }
            const auto outSingle = std::chrono::system_clock::now();
            const auto durSingle = std::chrono::duration_cast<std::chrono::milliseconds>(outSingle - inSingle).count();
            std::cout << "durSingle=" << durSingle << std::endl;

            std::atomic_int durCoh{ 0 };
            std::atomic_bool end1{ false }, end2{ false };
            const auto inCoh = std::chrono::system_clock::now();
            // now use 2 threads with coherence
            auto changeEven = [&]() {
                for (auto i = 0; i < vec.size(); i += 2)
                    vec[i] = 2;
                const auto durTemp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - inCoh).count();
                if (durTemp > durCoh)
                    durCoh = durTemp;
                end1 = true;
            };
            auto changeUneven = [&]() {
                for (auto i = 1; i < vec.size(); i += 2)
                    vec[i] = 3;
                const auto durTemp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - inCoh).count();
                if (durTemp > durCoh)
                    durCoh = durTemp;
                end2 = true;
            };
            std::thread t1{ changeEven }, t2{ changeUneven };
            t1.detach();
            t2.detach();
            while (!end1 || !end2) {
                std::this_thread::sleep_for(2s);
            }
            std::cout << "durCoh=" << durCoh << std::endl;
        }
    }
}

/** C++ IDIOMS
* 
* https://en.wikibooks.org/wiki/C%2B%2B_Programming/Idioms
* https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms
* 
* Some important:
* 1) Handling self-assignment in assignment operator.
* 2) pImpl
* 3) Calling virtuals during initialization
* 4) RVO
* 5) Empty base optimization (EBO)
* 6) enable_if, SFINAE (https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/enable-if)
* 7) Interface class
* 8) Metafunction. ( constexpr ?? )
* 9) Move construction.
* 10) nullptr
* 11) RAII
* 12) Smart pointer
* 13) Type traits (intbu)
* Many C++ idioms become a standard.
*/

/** CLASS INITIALIZATION
* 
*/
namespace class_initialization
{
    struct A {
        A(int i) { std::cout << "A"; }
        ~A() { std::cout << "a"; }
    };
    struct B : A {
        B(int i) : A(i) { std::cout << "B"; }
        ~B() { std::cout << "b"; }
    };
    struct C {
        C(int i) { std::cout << "C"; }
        ~C() { std::cout << "c"; }
    };
    struct E {
        E() : b(1), a(1), c(1) { std::cout << "E"; }
        ~E() { std::cout << "e"; }
        A a;
        B b;
        C c;
    };
    void check() {
        E e;
    }
}

/** VIRTUAL FUNCTIONS 
* Virtual functions implement dynamic polimorphism in run time.
* 
* VFT during construction.
* Object slicing.
* Virtual destructor.
* Method covering.
*/
namespace virtual_functions
{
    // Virtual methods should not be called from ctor and dtor as when object created the following sequence takes place:
    // When A is called then VFT points to Virtual table for A
    // When B is called then VFT points to Virtual table for B
    // When C is called then VFT points to Virtual table for C
    // VFT locates in the beginning of the object.
    void check1()
    {
        struct A {
            A() { f(); }
            ~A() { f(); }
            virtual void f() { std::cout << "a"; }
        };
        struct B: A {
            B() { f(); }
            ~B() { f(); }
            virtual void f() { std::cout << "b"; }
        };
        struct C: B {
            C() { f(); }
            ~C() { f(); }
            virtual void f() { std::cout << "c"; }
        };

        C c;
    }

    // when virtual function is called
    void check2()
    {
        struct A {
            A() { }
            virtual ~A() { }
            virtual void f() { std::cout << "a"; }
        };
        struct B : A {
            B() {  }
            virtual ~B() {  }
            virtual void f() { std::cout << "b"; }
        };
        struct C : B {
            C() { }
            ~C() {  }
            virtual void f() { std::cout << "c"; }
        };

        auto doFunc = [](A* a) {a->f(); };

        A* a1 = new B();
        A* a2 = new C();
        C c;
        A& a3 = c;
        A* a4 = &c;
        A a5 = c; // object slicing
        a1->f();
        a2->f();
        a3.f();
        a4->f();
        a5.f();
        doFunc(a1);
        doFunc(a2);
        doFunc(&c);
    }

    // Virtual desctructor
    void check3()
    {
        struct A
        {
            virtual ~A() { std::cout << "A"; } // remove virtual from here and see program output.
        };

        struct B : A
        {
            ~B() { std::cout << "B"; }
        };

        A* A = new B;
        delete A;
    }
}

/** EMPTY CLASS IN C++
* https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Empty_Base_Optimization
* Empty class has size of 1 byte at least. 
* Hiearchy flattening optimization.
*/
namespace empty_class
{
    void check()
    {
        struct A {};

        std::cout << sizeof(A);
    }
}

/** INITIALIZATION
* https://en.cppreference.com/w/cpp/language/initialization
* Initialization of a variable provides its initial value at the time of construction.
* Types of initializers:
*   1. (expression_list)
*   2. = expression
*   3. {initializer_list}
*/
namespace initialization
{
    struct A {
        int a;
        struct B {
            float b;
            char c;
            int d;
        } b;
        char e;
    };
    struct C {
        virtual void type() const { std::cout << "C::type()" << std::endl; }
        void type2() const { std::cout << "C::type2()" << std::endl; }
        operator int& () { return val; }
    private:
        int val{ 1 };
    };
    struct D : C {
        void type() const override { std::cout << "D::type()" << std::endl; }
        void type2() const { std::cout << "D::type2()" << std::endl; }
        operator A() {
            return { 1, {1.0f, 'c', 3}, 'd' }
            ;
        }
    };
    struct F {
        int&& val;
    };
    void check()
    {
        /** Value initialization 
        * Object is constructed with an empty initializer.
        * Includes default ctor.
        */
        std::string s1{};

        /** Direct initialization
        * Object is constructed from explicit set of constructor arguments.
        */
        std::string s2("hello");

        /** Copy initialization
        * Object is constructed from another object.
        */
        std::string s3 = "hello";

        /** List initialization 
        * Object is constructed from braced init list.
        */
        std::string s4{ 'h', 'e', 'l', 'l', 'o' };

        /** Aggregate initialization */
        A a = { 1, {1.0f, 'c', 3}, 'd' }; // not possible if there are virtual functions???
        int ar2d1[2][2] = { {1, 2}, {3, 4} }; // fully-braced 2D array: {1, 2}
                                              //                        {3, 4}
        int ar2d2[2][2] = { 1, 2, 3, 4 };     // brace elision: {1, 2}
                                              //                {3, 4}
        int ar2d3[2][2] = { {1}, {2} };       // only first column: {1, 0}
                                              //                    {2, 0}

        /** Reference initialization. */
        int val1{};
        int& ref1 = val1; // reference to a
        const int& ref2 = val1; // reference to a (const)
        D d;
        C& ca = d;             // ra refers to C subobject in b
        const C& rca = d;      // rca refers to C subobject in b
        const D& rda = d;      // rca refers to D subobject in b
        ca.type();             // calls virtual function of d
        rca.type();            // calls virtual function of d
        rda.type();            // calls virtual function of d
        ca.type2();            // calls non-virtual function of c
        rca.type2();           // calls non-virtual function of c
        rda.type2();           // calls non-virtual function of d
        int& val = static_cast<int&>(d); // operator int&()
        C&& rda2 = D();        // rvalue reference to C suboject of D object
        rda2.type2();
        rda2.type();
        A&& ada = static_cast<A>(d); // binds to the result of conversion operator A()
        const A& ada2 = static_cast<A>(d); // binds to the result of conversion operator A()
        //A& ada3 = static_cast<A>(d); // wrong becaues lvalue reference refers to rvalue value
        
        // Lifetime of the temporary
        F f1{ 7 }; // OK, lifetime is extended
        // F f2(7); // well-formed, but dangling reference

        // temporary values
        const std::string& rs = "abc"; // rs refers to temporary copy-initialized from char array
        const double& rcd2 = 2;        // rcd2 refers to temporary with value 2.0
        int* i3 = new int(2);
        double&& rrd3 = static_cast<double>(*i3);  // rrd3 refers to temporary with value 2.0.
        int&& r3 = static_cast<int&&>(*i3);        // r3 refers to i3 data.
        delete i3;
        std::cout << rrd3 << std::endl;  // here is ok 2. Because double rvalue reference refers to temporal object.
        std::cout << r3 << std::endl;    // here is the undefined behavior.
    }
}

/** STANDARDS
* C++ 11
* https://en.cppreference.com/w/cpp/11
* 
*/
namespace standards_cpp_11
{
    /** 1. AUTO, DECLTYPE
    * auto     ---> applies special type interference rules
    * decltype ---> saves all type qualifiers
    * */
    struct B {};

    struct A {
        double a;
        const B& getB() const { return b; }
    private:
        B b;
    };
    template <typename T>
    std::string typeToText(T t)
    {
        std::string res;
        if (std::is_const<decltype(res)>::value)
            res += "const ";
        res += typeid(t).name();
        if (std::is_reference<decltype(res)>::value)
            res += "&";
        return res;
    }
    void check1()
    {
        auto a = A();
        decltype(a) c{};
        const auto* g = &a;
        const auto& b = a.getB();
    }

    /** 2. DAFAULTED AND DELETED FUNCTIONS */
    struct C
    {
        C() = default;
        C(const C&) = delete;
    };
    void check2()
    {
        C c; // norm. Default constructor exists.
        // auto d = c; // compilation error
    }

    /** 3. FINAL AND OVERRIDE KEYWORDS
    * If final virtual function can't be overriden and if final class can't be inherited.
    * If function is specified by override then it must override virtual function.
    */
    
    /** 4. FUNCTION RETURN TYPE TRAILING */
    auto f1() { return 1.0; }
    void check4() { std::cout << typeid(f1()).name() << std::endl; }

    /** 5. MOVE CONSTRUCTORS AND MOVE ASSIGNMENT OPERATORS 
    * https://en.cppreference.com/w/cpp/language/move_constructor
    * https://en.cppreference.com/w/cpp/language/move_assignment
    */
    struct A2 {
        A2() { data_.resize(1000); }
        const A2& operator=(A2&& o) noexcept
        {
            std::cout << "operator=(A2&&)" << std::endl;
            data_ = std::move(o.data_);
            return *this;
        }
        A2(A2&& o)
        {
            std::cout << "A2(A2&&)" << std::endl;
            data_ = std::move(o.data_);
        }
        A2(const A2& o)
        {
            std::cout << "A2(const A2&)" << std::endl;
            data_ = o.data_;
        }
        const A2& operator=(const A2& o)
        {
            std::cout << "operator=(const A2&)" << std::endl;
            data_ = o.data_;
            return *this;
        }
        std::vector<int> data_;
    };
    void check5()
    {
        A2 a, b;
        A2 c(a);                // copy ctor
        A2 d(std::move(a));     // move ctor
        c = b;                  // copy assignment 
        c = std::move(b);       // move assignment
        A2 e = c;               // copy ctor 
        A2 f = std::move(c);    // move ctor
    }

    /** 6. SCOPED ENUMS 
    * https://en.cppreference.com/w/cpp/language/enum
    */
    enum Unscoped { one, two, three};
    enum class Scoped {one, two, three};
    void check6()
    {
        Unscoped ob1{};
        Scoped ob2{};
        auto ob3 = ob1 + 1; // possible
    //    auto ob4 = ob2 + 1; // not possible
        int r = ob1; // ok
       // int r2 = ob2; // error
        int r3 = static_cast<int>(ob2); // ok
    }

    /** 7. CONSTEXPR AND LITERAL TYPES
    * https://en.cppreference.com/w/cpp/language/constexpr
    * https://en.cppreference.com/w/cpp/named_req/LiteralType
    * C++11 constexpr functions use recursion rather than iteration
    * C++11 constexpr functions had to put everything in a single return statement
    */
    constexpr int factorial(int n)
    {
        return n <= 1 ? 1 : n * factorial(n - 1);
    }
    // compile time output function
    template<int n>
    struct constN
    {
        constN() { std::cout << n << std::endl; }
    };
    // literal class
    class conststr {
        const char* p;
        std::size_t sz;
    public:
        template<std::size_t N>
        constexpr conststr(const char(&a)[N]) : p(a), sz(N - 1) {}

        // constexpr functions signal errors by throwing exceptions
        // in C++11, they must do so from the conditional operator ?:
        constexpr char operator[](std::size_t n) const
        {
            return n < sz ? p[n] : throw std::out_of_range("");
        }
        constexpr std::size_t size() const { return sz; }
    };
    void check7()
    {
        std::cout << "4! = ";
        constN<factorial(4)> out1; // at compile time

        volatile int k = 7; // forbid compiler to do changes
        std::cout << "7! = " << factorial(7) << std::endl; // at run-time
    }

    /** 8. LIST INITIALIZATION
    * https://en.cppreference.com/w/cpp/language/list_initialization
    * List initialization is performed in the following situations:
    * 1. direct-list-initialization (both explicit and non-explicit constructors are considered).
    * 2. copy-list-initialization (both explicit and non-explicit constructors are considered, but only non-explicit constructors may be called).
    */
    struct X {}; // aggregate

    struct Q { // non-aggregate
        Q() = default;
        Q(Q const&) = default;
        Q(std::initializer_list<Q>) {}
    };
    enum UNSCP{one1, two1, three1};
    void check8()
    {
        double d1{ 1.0 };
        // int a{ 1.0f }; // forbid. Narrowing conversion.
        // float b{ d1 }; // forbid. Narrowing conversion.
        int c{}; // initialization to zero
        //float f2{ c }; // forbid. Narrowing conversion.
        //UNSCP en{ 3 }; // forbid. Narrowing conversion.
        std::string s{ 'a', 'b', 'c', 'd' }; // initializer-list constructor call
        std::cout << "initializer list initialized " << c << " " << s << std::endl;
        int n2 = { 1 }; // copy-list initialization
        double d = double{ 1.2 }; // list-initialization of a prvalue, then copy-init
        auto s4 = std::string{ "HelloWorld" }; // same as above, no temporary created since C++17
        std::map<int, std::string> m = { // nested list-initialization
           {1, "a"},
           {2, {'a', 'b', 'c'} },
           {3, s4}
        };
        int&& r1 = { 1 }; // binds a rvalue reference to a temporary int

        // A braced-init-list is not an expression and therefore has no type. decltype({ 1,2 }) is ill - formed.

        X x;
        X x2 = X{ x }; // copy-constructor (not aggregate initialization)
        Q q;
        Q q2 = Q{ q }; // initializer-list constructor (not copy constructor)

        int ai[] = { 1, 2.0 }; // Narrowing conversion from double to int: error in C++11, okay in C++03.
                               // But in msvc2019 it works.
    }

    /** 9. DELEGATING AND INHERITING CONSTRUCTORS
    * https://en.cppreference.com/w/cpp/language/constructor#Delegating_constructor
    */
    class DelegatedCtor
    {
    public:
        DelegatedCtor(int k) : DelegatedCtor(k, 'c') { std::cout << "InheritedCtor(int k)" << std::endl; }
        DelegatedCtor(int k, char b) { std::cout << "InheritedCtor(int k, char b)" << std::endl; }
    };
    class InheritedCtorBase
    {
    public:
        InheritedCtorBase() { std::cout << "InheritedCtorBase()" << std::endl; }
        InheritedCtorBase(int k) { std::cout << "InheritedCtorBase(int)" << std::endl; }
    };
    class InheritedCtorDerived : InheritedCtorBase
    {
    public:
        using InheritedCtorBase::InheritedCtorBase;
    };
    void check9()
    {
        DelegatedCtor ob1{ 1 };
        DelegatedCtor ob2{ 1, 'c' };
        InheritedCtorDerived ob3{};
        InheritedCtorDerived ob4{3};
    }

    /** 10. BRACE OR EQUAL INITIALIZERS */
    struct A10
    {
        int a1;
        struct B {
            int a2;
            int a3;
            char c4;
        } b2;
        char c3;
    };
    void check10() {
        A10 a = { 1, {1, 2, 'c'}, 'd' };
    }

    /** 11. NULLPTR
    * https://en.cppreference.com/w/cpp/language/nullptr
    * It is prvalue literal of std::nullptr_t type.
    */
    template <typename T>
    constexpr T clone(const T& t) {
        return t;
    }
    void g(int*) {
        std::cout << "d called" << std::endl;
    }
    void check11() {
        g(NULL);
        g(0);
        g(nullptr);

        // intbu this example
        // Demonstrates that nullptr retains the meaning of null pointer constant even if it is no longer a literal.
        g(clone(nullptr));
        //g(clone(NULL)); // ERROR: non-literal zero cannot be a null pointer constant. 
                          // NULL causes T = int and not int*. When nullptr used it is ok.
        //g(clone(0));    // ERROR: non-literal zero cannot be a null pointer constant
    }

    /** 12. LONG LONG (at least 8 bytes) 
    *       CHAR16_T, CHAR32_T
    */
    void check12() {
        long long val{};
        std::cout << sizeof(val) << std::endl;
        char c{};
        wchar_t wc{};
        char16_t c16{};
        char32_t c32{};
        print(sizeof(c));   // 1
        print(sizeof(wc));  // 2
        print(sizeof(c16)); // 3
        print(sizeof(c32)); // 4
    }

    /** 13. ALIASES: TYPES AND TEMPLATES
    * https://en.cppreference.com/w/cpp/language/type_alias
    * 
    * A type alias declaration introduces a name which can be used as a synonym for the type denoted by type-id. 
    * It does not introduce a new type and it cannot change the meaning of an existing type name. 
    * There is no difference between a type alias declaration and typedef declaration.
    * 
    * An alias template is a template which, when specialized, 
    * is equivalent to the result of substituting the template arguments of the alias template for the template parameters.
    */
    using BigLong = long long;
    template<typename T>
    using Shp = std::shared_ptr<T>;
    // type alias can introduce a member typedef name
    template<typename T>
    struct PolyStruct { using value_type = T; };
    // using in generic programming
    template <typename T>
    using Type = typename T::type; // get type from T
    template <typename Condition>
    using EnableIfCondition = Type<std::enable_if<Condition::value>>; // enables only if condition value is true.

    template<typename T, typename = EnableIfCondition<std::is_polymorphic<T>>>
    void gForPolymorphic(T ob) {
       // print9(typeid(T).name() + std::string(" is polymorphic."));
    }

    struct NotPolymorphic {};
    struct Polymorphic { virtual ~Polymorphic() = default; };
    void check13() {
        NotPolymorphic ob1;
        Polymorphic ob2;
        //gForPolymorphic(ob1); // Error. Type is not polymorphic.
        gForPolymorphic(ob2);
    }

    /** 14. VARIADIC TEMPLATES
    * https://en.cppreference.com/w/cpp/language/parameter_pack
    * A template parameter pack is a template parameter that accepts zero or more template arguments (non-types, types, or templates). 
    * A function parameter pack is a function parameter that accepts zero or more function arguments.
    * A template with at least one parameter pack is called a variadic template.
    * 
    * In a primary class template, the template parameter pack must be the final parameter in the template parameter list. 
    * In a function template, 
    * the template parameter pack may appear earlier in the list provided 
    * that all following parameters can be deduced from the function arguments, or have default arguments.
    */
    template<typename ... Types> struct Tuple {};
    //template<typename ... Types, typename T> struct Error {}; // error. In template classes variadic parameter should be in the end.
    template<typename T, typename ... Types> struct Ok {};
    template<typename ... Us> void ptrFunc(Us ... args) {
     //  print9("This is ptrFunc()");
    };
    template<typename ... Ts> void valueFunc(Ts ... args) {
        ptrFunc(&args...); // pack expansion
                           // pattern of pack expansion - &args
    };
    //template<typename T1, typename T2> struct Pair {};
    //template<class ...Args1> struct zip {
    //    template<class ...Args2> struct with {
    //        using type = Tuple<Pair<Args1, Args2>...>;
    //               // Pair<Args1, Args2>... //is the pack expansion
    //                //Pair<Args1, Args2> is the pattern
    //    };
    //};
    //typedef zip<short>::with<unsigned short, unsigned>::type T2;
    // error: pack expansion contains parameter packs of different lengths

    // Example of pack extension in a base class.
    template <typename... bases>
    struct X2 : bases... {
        using bases::g...;
    };
    void check14() {
        Tuple<> t1;
        Tuple<int, double, long> t2;
        //Tuple<0> t3; // error. 1 is not type.

        /** PACK EXTENSION */
        valueFunc(1, 0.2, "a"); // Ts... args expand to int E1, double E2, const char* E3
                                // &args... expands to &E1, &E2, &E3
                                // Us... pargs expand to int* E1, double* E2, const char** E3
        //using T1 = zip<short, int>::with<unsigned short, unsigned>::type;
        //// Pair<Args1, Args2>... expands to
        //// Pair<short, unsigned short>, Pair<int, unsigned int> 
        //// T1 is Tuple<Pair<short, unsigned short>, Pair<int, unsigned>>
        //using T2 = zip<short>::with<unsigned short, unsigned>::type;
        //// error: pack expansion contains parameter packs of different lengths
        ///*
        //    f(&args...); // expands to f(&E1, &E2, &E3)
        //    f(n, ++args...); // expands to f(n, ++E1, ++E2, ++E3);
        //    f(++args..., n); // expands to f(++E1, ++E2, ++E3, n);
        //    f(const_cast<const Args*>(&args)...);
        //    // f(const_cast<const E1*>(&X1), const_cast<const E2*>(&X2), const_cast<const E3*>(&X3))
        //    f(h(args...) + args...); // expands to f(h(E1,E2,E3) + E1, h(E1,E2,E3) + E2, h(E1,E2,E3) + E3)
        //*/
        ///** Web-page https://en.cppreference.com/w/cpp/language/parameter_pack contains more 
        //* examples and data of pack extension.
        //*/
    };

    /** 15. GENERALIZED NON-TRIVIAL UNIONS 
    * https://en.cppreference.com/w/cpp/language/union
    * The union is only as big as necessary to hold its largest data member. 
    * The other data members are allocated in the same bytes as part of that largest member.
    * A union can have member functions (including constructors and destructors), but not virtual functions.
    * A union cannot have base classes and cannot be used as a base class.
    * A union cannot have non-static data members of reference types.
    */
    // Union-like class
    struct UnionLike {
        enum class Tag{ CHAR, DOUBLE, INT } tag;
        union {
            char c;
            int i;
            double d;
        };
    };
    void printByTag(const UnionLike& ob) {
        switch (ob.tag) {
        case UnionLike::Tag::CHAR: print(ob.c); break;
        case UnionLike::Tag::INT: print(ob.c); break;
        case UnionLike::Tag::DOUBLE: print(ob.c); break;
        default:break;
        }
    }
    void check15() {
        union U {
            int a{};
            double d;
        };

        U ob;
        ob.a = 5;
      //  print9(ob.a);
      //  print9(ob.d); // there is rubbish

        std::variant<int, double, char> ob2;
        ob2 = 1.7;
     //   print9(std::get<double>(ob2));
        ob2 = 1; // if it will be not int then next print9 will throw exception
      //  print9(std::get<int>(ob2));
        ob2 = 'c';
       // print9(std::get<char>(ob2));

        // anonymous union saves k and c in the same memory.
        union {
            int k{};
            char c;
        };
        k = 4;
     //   print9(k);
        c = 'a';
     //   print9(c);
     //   print9(k); // rubbish
//
        // union-like class
        // Doesn't work. It is necessary to create constructor.
        UnionLike ob3 = { UnionLike::Tag::CHAR, 'C' };
        UnionLike ob4 = { UnionLike::Tag::INT, 1 };
        UnionLike ob5 = { UnionLike::Tag::DOUBLE, 1.7 };
        printByTag(ob3);
        printByTag(ob4);
        printByTag(ob5);
    }

    /** 16. GENERALIZED PODs: TRIVIAL AND STANDARD LAYOUT TYPES
    * https://en.cppreference.com/w/cpp/named_req/StandardLayoutType
    * https://en.cppreference.com/w/cpp/named_req/TrivialType
    * https://en.cppreference.com/w/cpp/named_req/PODType
    * https://mariusbancila.ro/blog/2020/08/10/no-more-plain-old-data/
    */
    template<typename T> bool isTrivial() { return std::is_trivial<T>::value; }
    template<typename T> bool isStandardLayout() { return std::is_standard_layout<T>::value; }
    template<typename T> bool isScalar() { return std::is_scalar<T>::value; }
    enum class E15 { One, Two, Three };
    struct A16 {};
    void check16()
    {
        /** SCALAR TYPE
        * A scalar type is any of the following:
        * an arithmetic type
        * an enumeration type
        * a pointer type
        * a pointer-to-member type
        * the std::nullptr_t type
        * cv-qualified versions of the above types
        */
        int a{};
        std::cout << "int is scalar: " << isScalar<int>() << std::endl;
        std::cout << "enum is scalar: " << isScalar<E15>() << std::endl;
        std::cout << "pointer is scalar: " << isScalar<int*>() << std::endl;
        std::cout << "nullptr is scalar: " << isScalar<std::nullptr_t>() << std::endl;
        std::cout << "struct is scalar: " << isScalar<A16>() << std::endl;

        /** TRIVIAL TYPE
        * A trivial type is a type that is trivially copyable and has one or more default constructors, 
        * all of which are either trivial or deleted, and at least one of which is not deleted.
        * 
        * A trivially-copyable type is a type that has:
        * - only copy-constructors and copy-assignment operators that are either trivial or deleted;
        * - only move-constructors and move-assignment operators that are either trivial or deleted;
        * - at least one of these four special member functions is not deleted;
        * - a trivial non-deleted destructor;
        * - no virtual functions or virtual base classes.
        * 
        * In this definition, trivial means that the special member function belongs to a class that:
        * - it is not user-provided;
        * - has no virtual functions or virtual base classes;
        * - has no base classes with a non-trivial constructor/operator/destructor;
        * - has no data members of a type that has non-trivial constructor/operator/destructor.
        * 
        * Trivial types have some properties:
        * - They occupy a contiguous memory area.
        * - There can be padding bytes between members due to alignment requirements.
        * - Can use memcpy with objects of trivial types.
        * - Can be copied to an array of char or unsigned char and then back.
        * - They can have members with different access specifiers. 
        *   However, in this situation the compiler can decide how to order the members.
        * 
        * However, trivial types cannot be safely used to interop with code written in other programming languages. 
        * This is due to the fact that the order of the members is compiler-specific.
        */
        struct B1
        {
        };
        struct B2
        {
        private:
            int a;
        public:
            double b;
            void foo() {}
        };
        struct B3
        {
        private:
            int a;
        public:
            double b;
            B3(int const x, double const y) :
                a(x), b(y) {}
            B3() = default;
        };
        struct B4Base
        {
            int    a;
            double b;
            bool operator==(const B4Base& other) { return a == other.a && b == other.b; }
        };
        struct B4 : public B4Base
        {
        private:
            int a;
        };
        struct B5 : public B4Base
        {
        private:
            B5(int i) {}
            B5(const B5&) = delete;
            const B5& operator=(const B5&) = delete;
            B5(B5&&) = delete;
            const B5& operator=(B5&&) = delete;
        };
        struct B6 : public B4Base
        {
        private:
            virtual void foo() {}
        };
        std::cout << "B1 is trivial: " << isTrivial<B1>() << std::endl; // true
        std::cout << "B2 is trivial: " << isTrivial<B2>() << std::endl; // true
        std::cout << "B3 is trivial: " << isTrivial<B3>() << std::endl; // true
        std::cout << "B4 is trivial: " << isTrivial<B4>() << std::endl; // true
        std::cout << "B5 is trivial: " << isTrivial<B5>() << std::endl; // false. B5 doesn't have trivial special member function.
        std::cout << "B6 is trivial: " << isTrivial<B6>() << std::endl; // false. B6 has virtual functions.
        B4Base ob1;
        B4Base* pob2 = reinterpret_cast<B4Base*>(malloc(sizeof(ob1)));
        memcpy(pob2, &ob1, sizeof(ob1));
        std::cout << "memcmp(ob1, pob2) = " << memcmp(&ob1, pob2, sizeof(ob1)) << std::endl;

        /** STANDARD LAYOUT TYPES
        * In simple words, a standard-layout type is a type 
        * that has members with the same access control and does not have virtual functions or virtual base classes, 
        * or other features not present in the C language.
        * 
        * Formally defined, a standard-layout type is a type that:
        * - has the same access control for all the non-static data members;
        * - has no non-static data members of reference types;
        * - has no virtual functions or virtual base classes;
        * - all the non-static data members and base classes are standard-layout types;
        * - has no two base class sub-objects of the same type (no diamond problem due to multiple inheritance);
        * - has all non-static data members and bit-fields declared in the same class;
        * - has no base classes of the same type as the first non-static data member.
        * 
        * Standard-layout types have some properties, including the following:
        * - The memory layout of a standard-layout type is well defined so 
        *   that it can be used to interop with other programming languages, such as C.
        * - Objects of standard-layout types can be memcpy-ed.
        * - Facilitates empty base class optimization. This is an optimization that ensures base classes with no data members take up no space and, 
        *   therefore, have the size zero. Also, 
        *   such a base sub-object has the same address as the first data member of the derived class 
        *   (therefore, the last limitation in the preceding list).
        * - Can use the offsetof macro to determine the offset of a data member, in bytes, from the beginning of the object.
        */
        struct C1
        {
        };
        struct C2
        {
            C1    a;
            double b;
        };
        struct C3Base
        {
            void foo() {}
        };
        struct C3 : public C3Base
        {
            int    a;
            double b;
        };
        struct C4Base
        {
            int    a;
        };
        struct C4 : public C4Base
        {
            double b;
        };
        struct C5
        {
            int    a;
        private:
            virtual void foo() {};
        };
        struct C6Base {};
        struct X : public C6Base {};
        struct Y : public C6Base {};
        struct C6 : public X, Y {};
        struct C7
        {
            int    a;
        private:
            double b;
        };
        struct C8 : public C6Base
        {
            C6Base b;
            int    a;
        };
        std::cout << "C1 is standard layout: " << isStandardLayout<C1>() << std::endl; // true
        std::cout << "C2 is standard layout: " << isStandardLayout<C2>() << std::endl; // true
        std::cout << "C3 is standard layout: " << isStandardLayout<C3>() << std::endl; // true
        std::cout << "C4 is standard layout: " << isStandardLayout<C4>() << std::endl; // false. Not all members are in the same class. Part are in base class.
        std::cout << "C5 is standard layout: " << isStandardLayout<C5>() << std::endl; // false. Has virtual functions.
        std::cout << "C6 is standard layout: " << isStandardLayout<C6>() << std::endl; // false. Diamond problem.
        std::cout << "C7 is standard layout: " << isStandardLayout<C7>() << std::endl; // false. Members with different access control.
        std::cout << "C8 is standard layout: " << isStandardLayout<C8>() << std::endl; // false. Has the first non-static data member of the same type as the base class.
        for (auto i = 0; i < 5e7; ++i) {
            C5 ob3;
            C5* pob4 = reinterpret_cast<C5*>(malloc(sizeof(ob3)));
            memcpy(pob4, &ob3, sizeof(ob3));
            if (memcmp(&ob3, pob4, sizeof(ob3)))
                std::cout << "memcmp doesn't work!" << std::endl;
        }
    }

    /** 17. UNICODE STRING LITERAL
    * https://en.cppreference.com/w/cpp/language/string_literal
    */
    void check17() {
        char array1[] = "hello world!";
        char array2[] = { 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd' };

        // multiline-string
        char ayyay3[] = R"foo(Hello
            world!)foo";
        char array4[] = "Hello\nworld!";

        // String concatenation.
        char array5[] = "Hello"
            "\nworld!";

        wchar_t array6[] = L"Hello world!"; // unicode char string.
    }

    /** 18. USER-DEFINED LITERALS
    * https://en.cppreference.com/w/cpp/language/user_literal
    * Allows integer, floating-point, character, and string literals to produce objects of user-defined type 
    * by defining a user-defined suffix.
    * When the compiler encounters a user-defined literal with ud-suffix X, it performs unqualified name lookup, looking for a function with the name operator "" X. 
    * If the lookup does not find a declaration, the program is ill-formed.
    */
    // The function called by a user-defined literal is known as literal operator
    constexpr long double operator"" _km (long double d) {
        return 1e3 * d;
    }; // error: suffix must begin with underscore
    constexpr long double operator"" _dm (long double d) {
        return 10 * d;
    }; // error: suffix must begin with underscore
    struct kms {
        long double d{};
    };
    constexpr kms operator"" _kms(long double d) { return kms{ d }; }
    void check18() {
        print(1.0_km, 1.0_dm, 1.0_kms .d);
    }

    /** 19. Attributes
    * https://en.cppreference.com/w/cpp/language/attributes
    * Attributes provide the unified standard syntax for implementation-defined language extensions, 
    * such as the GNU and IBM language extensions __attribute__((...)), Microsoft extension __declspec(), etc.
    * An attribute can be used almost everywhere in the C++ program, and can be applied to almost everything: 
    * to types, to variables, to functions, to names, to code blocks, to entire translation units, 
    * although each particular attribute is only valid where it is permitted by the implementation.
    * There are standard attributes.
    */
    [[deprecated]]
    void check19() {
        int a[] = { 0, 1, 2, 3, 4, 5 };
        for (int n : a) 
            std::cout << n << ' ';
        std::cout << '\n';

        for ([[maybe_unused]] int n : a) // maybe_unused variable
            std::cout << 1 << ' '; 
    } // check19() is deprecated

    /** 20. LAMBDA EXPRESSIONS 
    * https://en.cppreference.com/w/cpp/language/lambda
    * Constructs a closure: an unnamed function object capable of capturing variables in scope.
    * [ captures ] ( params ) { body }
    * The lambda expression is a prvalue expression of unique unnamed non-union non-aggregate class type, 
    * known as closure type, which is declared (for the purposes of ADL) in the smallest block scope, class scope, 
    * or namespace scope that contains the lambda expression. 
    * The closure type has the following members: operator()
    * 
    * Dandling references: If a non-reference entity is captured by reference, implicitly or explicitly, 
    * and the function call operator of the closure object is invoked after the entity's lifetime has ended, undefined behavior occurs. 
    * The C++ closures do not extend the lifetimes of the captured references.
    * Same applies to the lifetime of the object pointed to by the captured this pointer.
    * 
    * Lambda expression can be nor volatile, nor virtual. But can be mutable.
    * 
    * The destructor of closure is implicitly defined.
    */
    /** PROPERTY 1. SCOPE
    * For the purpose of name lookup, determining the type and value of the this pointer 
    * and for accessing non-static class members, 
    * the body of the closure type's function call operator 
    * is considered in the context of the lambda-expression. 
    * 
    * PROPERTY 2. CAPTURES
    * The captures is a comma-separated list of zero or more captures, optionally beginning with the capture-default. 
    * The capture list defines the outside variables that are accessible from within the lambda function body. 
    * The only capture defaults are
    *  - & (implicitly capture the used automatic variables by reference) and
    *  - = (implicitly capture the used automatic variables by copy).
    * The current object (*this) can be implicitly captured if either capture default is present. 
    * If implicitly captured, it is always captured by reference, even if the capture default is =.
    * 
    * PROPERTY 3. CAPTURE ONLY IF ODR-USED
    */
    struct X20 {
        int x, y;
        int operator()(int s) { return s * 2; }
        int f()
        {
            // the context of the following lambda is the member function X::f
            auto lambda = [=]()->int
            {
                return operator()(this->x + y); // X::operator()(this->x + (*this).y)
                                                // this has type X*
            };
            return lambda();
        }
    };
    void check20() {
        // PROPERTY 1. SCOPE
        X20 x;
        x.x = 1;
        x.y = 2;
     //   print9(x.f());

        // PROPERTY 2. CAPTURES
        struct S2 {
            void f(int i) {
                [&] {};          // OK: by-reference capture default
                [&, i] {};       // OK: by-reference capture, except i is captured by copy
                //[&, &i] {};     // Error: by-reference capture when by-reference is the default
                [&, this] {};   // OK, equivalent to [&]
                [&, this, i] {}; // OK, equivalent to [&, i]
                [=] {}; // OK, by-value capture.
                //[i, i]() {}; // Error: any capture may appear only once.
            }
        };

        // PROPERTY 3. CAPTURE ONLY ODR-USED
        {
            float x, & r = x;
            [=]
            { // x and r are not captured (appearance in a decltype operand is not an odr-use)
                decltype(x) y1; // y1 has type float
                decltype((x)) y2 = y1; // y2 has type float const& because this lambda
                                       // is not mutable and x is an lvalue
                decltype(r) r1 = y1;   // r1 has type float& (transformation not considered)
                //decltype((r)) r2 = y2; // r2 has type float const&
            };
        }
    }

    /** 21. NOEXCEPT
        * https://en.cppreference.com/w/cpp/language/noexcept_spec
        * Specifies whether a function could throw exceptions.
        * https://en.cppreference.com/w/cpp/language/alignas
        */

    /** 22. ALIGNOF, ALIGNAS
    * https://en.cppreference.com/w/cpp/language/object#Alignment
    * https://en.cppreference.com/w/cpp/language/alignof
    * 
    * Alignment - number of bytes between successive addresses at which objects of this type can be allocated.
    * In order to satisfy alignment requirements of all non-static members of a class, padding may be inserted after some of its members.
    * 
    * Returns the alignment, in bytes, required for any instance of the type indicated by type-id,
    * which is either complete object type, an array type whose element type is complete, or a reference type to one of those types.
    * If the type is reference type, the operator returns the alignment of referenced type;
    * if the type is array type, alignment requirement of the element type is returned.
    * 
    * Pointers returned by allocation functions such as std::malloc are suitably aligned for any object, 
    * which means they are aligned at least as strictly as std::max_align_t.
    */
    void check22() {

        // objects of type S can be allocated at any address
        // because both S.a and S.b can be allocated at any address
        struct S {
            char a; // size: 1, alignment: 1
            char b; // size: 1, alignment: 1
        }; 

        // objects of type X must be allocated at 4-byte boundaries
        // because X.n must be allocated at 4-byte boundaries
        // because int's alignment requirement is (usually) 4
        struct X {
            int n;  // size: 4, alignment: 4
            char c; // size: 1, alignment: 1
            // three bytes padding
        }; // size: 8, alignment: 4 

        struct Foo {
            int   i;
            float f;
            char  c;
        };
        // Note: `alignas(alignof(long double))` below can be simplified to simply 
        // `alignas(long double)` if desired.
        struct alignas(long double) Foo2 {
            char c1;
            char c2;
            char c3;
        };
        struct alignas(1) Foo3 {
            char c1;
            char c2;
            char c3;
        };

        struct Empty {};

        struct alignas(64) Empty64 {};

        struct alignas(8) S22 {};
        struct alignas(1) U22 { S s; }; // error: alignment of U would have been 8 without alignas(1)
                                        // not in msvc2019

        std::cout << "Alignment and size of"  "\n"
            << "- char                       : " << alignof(char) << " " << sizeof(char) << "\n"
            << "- pointer                    : " << alignof(int*) << " " << sizeof(int*) << "\n"
            << "- class Foo                  : " << alignof(Foo) << " " << sizeof(Foo)   << "\n"
            << "- class Foo2                 : " << alignof(Foo2) << " " << sizeof(Foo2) << "\n"
            << "- class Foo3                 : " << alignof(Foo3) << " " << sizeof(Foo3) << "\n"
            << "- class S22                 : " << alignof(S22) << " " << sizeof(S22) << "\n"
            << "- class U22                 : " << alignof(U22) << " " << sizeof(U22) << "\n"
            << "- empty class                : " << alignof(Empty) << " " << sizeof(Empty) << "\n"
            << "- alignas(64) Empty          : " << alignof(Empty64) << " " << sizeof(Empty64) << "\n"
            << "- sizeof(int*)               : " << sizeof(int*) << " " << sizeof(int*) << "\n"
            << "- alignof(std::max_align_t)  : " << alignof(std::max_align_t) << " " << sizeof(std::max_align_t ) << "\n";
    }

    /** 23. MEMORY MODEL
    * https://en.cppreference.com/w/cpp/language/memory_model
    * 
    * Defines the semantics of computer memory storage for the purpose of the C++ abstract machine.
    * The memory available to a C++ program is one or more contiguous sequences of bytes. 
    * Each byte in memory has a unique address.
    * 
    * BYTE
    * Similar to C, C++ supports bytes of sizes 8 bits and greater.
    * 
    * MEMORY LOCATION
    * A memory location is
    *  - an object of scalar type (arithmetic type, pointer type, enumeration type, or std::nullptr_t);
    *  - or the largest contiguous sequence of bit fields of non-zero length.
    * Note: Various features of the language, such as references and virtual functions, 
    * might involve additional memory locations that are not accessible to programs but are managed by the implementation.
    * 
    * THREADS
    * Any thread can potentially access any object in the program 
    * (objects with automatic and thread-local storage duration may still be accessed by another thread through a pointer or by reference).
    * 
    * DATA RACES
    * Different threads of execution are always allowed to access (read and modify) different memory locations concurrently, 
    * with no interference and no synchronization requirements.
    * When an evaluation of an expression writes to a memory location and another evaluation reads 
    * or modifies the same memory location, the expressions are said to conflict. 
    * A program that has two conflicting evaluations has a DATA RACE unless
    *  - both evaluations execute on the same thread or in the same signal handler, or
    *  - both conflicting evaluations are atomic operations (see std::atomic), or
    *  - one of the conflicting evaluations happens-before another (see std::memory_order)
    * 
    * MEMORY ORDER
    * When a thread reads a value from a memory location, it may see the initial value, the value written in the same thread, 
    * or the value written in another thread. 
    * See std::memory_order for details on the order in which writes made from threads become visible to other threads.
    * 
    * FORWARD PROGRESS
    * intbu here https://en.cppreference.com/w/cpp/language/memory_model
    */
    struct S {
        char a;     // memory location #1
        int b : 5;  // memory location #2
        int c : 11, // memory location #2 (continued)
            : 0,
            d : 8;  // memory location #3
        struct {
            int ee : 8; // memory location #4
        } e;
    } obj; // The object 'obj' consists of 4 separate memory locations

    /** 24. THREAD LOCAL STORAGE.
    * https://en.cppreference.com/w/cpp/language/storage_duration#Storage_duration
    * All objects in a program have one of the following storage durations:
    * 
    * AUTOMATIC
    * The storage for the object is allocated at the beginning of the enclosing code block and deallocated at the end. 
    * All local objects have this storage duration, 
    * except those declared static, extern or thread_local.
    * 
    * STATIC
    * The storage for the object is allocated when the program begins and deallocated when the program ends. 
    * Only one instance of the object exists.
    * 
    * THREAD
    * The storage for the object is allocated when the thread begins and deallocated when the thread ends. 
    * Each thread has its own instance of the object. 
    * Only objects declared thread_local have this storage duration. 
    * thread_local can appear together with static or extern to adjust linkage.
    * 
    * DYNAMIC
    * The storage for the object is allocated and deallocated per request by using dynamic memory allocation functions.
    */
    thread_local int val = 1;
    std::mutex mut;
    void incr() {
        for (auto i = 0; i < 1e3; ++i) {
            val++;
            std::lock_guard<std::mutex> lock(mut);
            std::cout << "incr " << val << std::endl;
        }
    }
    void decr() {
        for (auto i = 0; i < 1e3; ++i) {
            val--;
            std::lock_guard<std::mutex> lock(mut);
            std::cout << "decr " << val << std::endl;
        }
    }
    void check24() {
        std::thread t1(incr), t2(decr);
        t1.join();
        t2.join();
        std::cout << "main " << val << std::endl;
    }

    /** 25. RANGE-BASED FOR LOOP
    * https://en.cppreference.com/w/cpp/language/range-for
    */
    void check25() {
        std::vector<int> v = { 0, 1, 2, 3, 4, 5 };
      //  for (auto&& el : v)
      //      print9(el);

        int a[] = { 0, 1, 2, 3, 4, 5 };
        for (int n : a) // the initializer may be an array
            std::cout << n << ' ';
        std::cout << '\n';

        for ([[maybe_unused]] int n : a)
            std::cout << 1 << ' '; // the loop variable need not be used
        std::cout << '\n';
    }

    /** 26. STATIC_ASSERT
    * https://en.cppreference.com/w/cpp/language/static_assert
    * Performs compile-time assertion checking.
    */
    template <class T>
    struct data_structure
    {
        static_assert(std::is_scalar<T>::value,
            "Data structure should contain only scalar types.");
    };
    struct A26{};
    struct B26 { 
        B26(int a) : a_(a) {}
        int a_;
    };
    void check26() {
        //data_structure<A> da; // Compiler Error: A is not trivial type.
        data_structure<int> di;
    }

    /** 27. RVALUE
    * https://habr.com/ru/post/348198/
    * lvalue has address in memory.
    */
    struct A27 { 
        A27(char c = 'd') { cout << "A27 CTOR " << c << endl; }
        A27(const A27& other) { 
            cout << "A27 COPY CTOR" << endl; 
        } 
        const A27& operator=(const A27& other) { 
            cout << "A27 COPY =" << endl; 
            return *this; 
        }
        A27(A27&& other) { 
            cout << "A27 MOVE CTOR" << endl; 
        }
        const A27& operator=(A27&& other) { 
            cout << "A27 MOVE =" << endl; 
            return *this; 
        }
        A27 operator+(const A27& other) { 
            auto res = A27('p'); 
            res.a = a + other.a; 
            cout << "A27 PLUS" << endl;
            return res;
        }
        int a{ 1 };
    };
    struct B27 { 
        B27(char c = 'd') { cout << "B27 CTOR " << c << endl; }
        B27(const B27& other) {
            cout << "B27 COPY CTOR" << endl; 
        } 
        const B27& operator=(const B27& other) {
            cout << "B27 COPY =" << endl; 
            return *this; 
        }
        B27 operator+(const B27& other) {
            auto res = B27('p');
            res.a = a + other.a; 
            cout << "B27 PLUS" << endl;
            return res;
        }
        int a{ 1 };
    };
    int globalvar{ 2 };
    int f27() { int a{}; return a; }
    int& g27() { return globalvar; }
    void check27() {
        int a{};
        // 2 = a; // Error: asignment to temporary object is not allowed
        // (a + 1) = 4; // Error: asignment to temporary object is not allowed
        // f27() = 5; // Error: asignment to temporary object is not allowed
        /** 
        * Possibility to return lvalue reference from function is vary useful feature in C++.
        * We can change container elements with overloaded operator [].
        */
        const int b{ 5 }; // also lvalue
        // b = 5; // but it is forbidden to do like this
        A27 a1('a'), b1('b');
        auto c = a1 + b1; // move ctor is called. It is in A27
        B27 a2('a'), b2('b');
        auto c2 = a2 + b2; // copy ctor is called. No move ctor in B27

        int arr[] = { 1,2,3 };
        int* p = &arr[0];
        *(p + 1) = 5; // lvalue from rvalue
        int var = 10;
        // int* sss = &(var + 3); // ERROR: rvalue doesn't have address
        //string& str = string(); // ERROR: reference to lvalue can be only reference to lvalue
        string&& str2 = string(); // It is ok. Ref to rvalue valid.
    }
}

/** REFERENCES
* https://en.cppreference.com/w/cpp/language/reference
* Alias of already existing variable or function.
* 1. A reference is required to be initialized to refer to a valid object or function.
* 2. There are no references to void and no references to references.
* 3. References are not objects; they do not necessarily occupy storage, 
*    although the compiler may allocate storage if it is necessary to implement the desired semantics 
*    (e.g. a non-static data member of reference type usually increases the size of the class by the amount 
*     necessary to store a memory address).
* 4. Because references are not objects, there are no arrays of references, no pointers to references, 
*    and no references to references.
* 5. References collapsing.
*    It is permitted to form references to references through type manipulations in templates or typedefs, 
*    in which case the reference collapsing rules apply: 
*    rvalue reference to rvalue reference collapses to rvalue reference, 
*    all other combinations form lvalue reference.
* 6. Lvalue references can be used to alias an existing object (optionally with different cv-qualification).
*    They can also be used to implement pass-by-reference semantics in function calls.
* 7. Rvalue references can be used to extend the lifetimes of temporary objects 
*    (note, lvalue references to const can extend the lifetimes of temporary objects too, 
*     but they are not modifiable through them).
* 8. Rvalue reference variables are lvalues when used in expressions (without std::move).
* 9. Forwarding references are a special kind of references 
*    that preserve the value category of a function argument, 
*    making it possible to forward it by means of std::forward.
* 10. VALUE CATEGORY
* https://en.cppreference.com/w/cpp/language/value_category
* PRVALUE (for example, literal, or function return type by value)
*   Same as rvalue (below).
*   A prvalue cannot be polymorphic: the dynamic type of the object it denotes is always the type of the expression.
*   A non-class non-array prvalue cannot be cv-qualified. 
*   (Note: a function call or cast expression may result in a prvalue of non-class cv-qualified type, 
*   but the cv-qualifier is immediately stripped out.)
*   A prvalue cannot have incomplete type (except for type void, see below, or when used in decltype specifier)
*   A prvalue cannot have abstract class type or an array thereof.
* XVALUE (for example, result of std::move)
*   Same as rvalue (below).
*   Same as glvalue (below).
*   In particular, like all rvalues, xvalues bind to rvalue references, 
*   and like all glvalues, xvalues may be polymorphic, and non-class xvalues may be cv-qualified.
* RVALUE
*   An rvalue expression is either prvalue or xvalue.
*/
namespace references
{
    char& char_number(std::string& s, std::size_t n) {
        return s.at(n); // string::at() returns a reference to char
    }

    void f(int& x) {
        std::cout << "lvalue reference overload f(" << x << ")\n";
    }

    void f(const int& x) {
        std::cout << "lvalue reference to const overload f(" << x << ")\n";
    }

    void f(int&& x) {
        std::cout << "rvalue reference overload f(" << x << ")\n";
    }

    template<class T>
    int f(T&& x) {                    // x is a forwarding reference
        return g(std::forward<T>(x)); // and so can be forwarded
    }

    void check()
    {
        // References don't have storage.
        //int& a[3]; // error
        //int&* p;   // error
        //int&& r;   // error

        // Reference collapsing.
        {
            typedef int& lref;
            typedef int&& rref;
            int n;
            lref& r1 = n; // type of r1 is int&
            lref&& r2 = n; // type of r2 is int&
            rref& r3 = n; // type of r3 is int&
            rref&& r4 = 1; // type of r4 is int&&
        }

        // Lvalue reference
        {
            std::string s = "Ex";
            std::string& r1 = s;
            const std::string& r2 = s;
            r1 += "ample";           // modifies s
            //  r2 += "!";           // error: cannot modify through reference to const
            std::cout << r2 << '\n'; // prints s, which now holds "Example"
        }

        // Lvalue expression.
        // When a function's return type is lvalue reference, the function call expression becomes an lvalue expression.
        {
            std::string str = "Test";
            char_number(str, 1) = 'a'; // the function call is lvalue, can be assigned to
            std::cout << str << '\n';
        }

        // Lifetime extension.
        {
            std::string s1 = "Test";
            //  std::string&& r1 = s1;       // error: rvalue reference can't bind to lvalue

            const std::string& r2 = s1 + s1; // okay: lvalue reference to const extends lifetime
            //  r2 += "Test";                // error: can't modify through reference to const

            std::string&& r3 = s1 + s1;      // okay: rvalue reference extends lifetime
            r3 += "Test";                    // okay: can modify through reference to non-const
            std::cout << r3 << '\n';

            // std::string& r4 = s1 + s1;    // error: lvalue reference doesn't extends lifetime.

            int&& r5 = 1;                    // okay: extends lifetime of temp object.
        }

        // Lvalue with Rvalue function overloads
        {
            int i = 1;
            const int ci = 2;
            f(i);  // calls f(int&)
            f(ci); // calls f(const int&)
            f(3);  // calls f(int&&)
                   // would call f(const int&) if f(int&&) overload wasn't provided
            f(std::move(i)); // calls f(int&&)

            // rvalue reference variables are lvalues when used in expressions
            int&& x = 1;
            f(x);            // calls f(int& x)
            f(std::move(x)); // calls f(int&& x)
        }

        // Because rvalue references can bind to xvalues, they can refer to non-temporary objects.
        {
            int i2 = 42;
            int&& rri = std::move(i2); // binds directly to i2

            // This makes it possible to move out of an object in scope that is no longer needed.
            std::vector<int> v{ 1,2,3,4,5 };
            std::vector<int> v2(std::move(v)); // binds an rvalue reference to v
            assert(v.empty());
        }
    }

    template<class T>
    int g(const T&& x); // x is not a forwarding reference: const T is not cv-unqualified


    template<class T>
    int f3(T&& x) {                    // x is a forwarding reference
        return g(std::forward<T>(x)); // and so can be forwarded
    }

    template<class T> struct A {
        template<class U>
        A(T&& x, U&& y, int* p); // x is not a forwarding reference: T is not a
                                 // type template parameter of the constructor,
                                 // but y is a forwarding reference
    };

    // Forwarding references.
    void check2()
    {
        /** CASE 1: Function parameter of a function template declared
        *   as rvalue reference to cv-unqualified type template parameter of that same function template.
        */
        {
            int i;
          //  f3(i); // argument is lvalue, calls f<int&>(int&), std::forward<int&>(x) is lvalue.
          //  f3(0); // argument is rvalue, calls f<int>(int&&), std::forward<int>(x) is rvalue.
        }

        /** CASE 2: auto&& except when deduced from a brace-enclosed initializer list. */
        {
            auto&& z = { 1, 2, 3 }; // *not* a forwarding reference (special case for initializer lists)
                                    // in other cases forwarding reference.
        }
    }

    /** Dangling references
    * Although references, once initialized, always refer to valid objects or functions, 
    * it is possible to create a program where the lifetime of the referred-to object ends, 
    * but the reference remains accessible (dangling). 
    * Accessing such a reference is undefined behavior. 
    * A common example is a function returning a reference to an automatic variable.
    */
    std::string& f()
    {
        std::string s = "Example";
        return s; // exits the scope of s:
                  // its destructor is called and its storage deallocated
    }
    void check3()
    {
        std::string& r = f(); // dangling reference
       // std::cout << r;       // undefined behavior: reads from a dangling reference
       // std::string s = f();  // undefined behavior: copy-initializes from a dangling reference
    }
}

/** SQL: JOIN
* https://www.w3schools.com/sql/sql_join.asp
* A JOIN clause is used to combine rows from two or more tables, based on a related column between them.
* 
* 1. LEFT JOIN https://www.w3schools.com/sql/sql_join_left.asp
* The LEFT JOIN keyword returns all records from the left table (table1), and the matching records from the right table (table2). 
* The result is 0 records from the right side, if there is no match.
* 
* select count(*) from agent_time_event where agent_id=31;
* select count(*) from agent_time_event left join login_info on agent_time_event.agent_id=login_info.agent_id and agent_time_event.agent_id=31;
* 
* 2. RIGHT JOIN 
* The RIGHT JOIN keyword returns all records from the right table (table2), and the matching records from the left table (table1). 
* The result is 0 records from the left side, if there is no match.
* 
* 3. INNER JOIN
* The INNER JOIN keyword selects records that have matching values in both tables.
* select count(*) from agent_time_event where agent_id=31;
* select count(*) from agent_time_event inner join login_info on login_info.agent_id=agent_time_event.agent_id and agent_time_event.agent_id=31;
* 
* 4. FULL OUTER JOIN
* The FULL OUTER JOIN keyword returns all records when there is a match in left (table1) or right (table2) table records.
* 
*/

/** C++11 STANDARD: LIBRARY FEATURES
* https://en.cppreference.com/w/cpp/11
*/
namespace standard_cpp_11_library {
    /** 1. ATOMIC OPERATIONS LIBRARY */
    void check1() {
        {
            std::atomic_int64_t val{ 0 };
            auto func = [&]() {for (auto i = 0; i < 2e7; ++i) val.fetch_add(1, std::memory_order_relaxed); };
            std::vector<std::thread> vec;
            for (auto i = 0; i < 10; ++i)
                vec.emplace_back(func);
            const auto nowSeq = std::chrono::high_resolution_clock::now();
            for (auto& t : vec) t.join();
            const auto endSeq = std::chrono::high_resolution_clock::now();
            const auto durSeq = std::chrono::duration_cast<std::chrono::milliseconds>(endSeq - nowSeq).count();
            std::cout << "Relaxed multighreading 10 threads val=" << val << " time=" << durSeq << std::endl;
        }
    }

    /** 2. INITIALIZER LIST
    * https://en.cppreference.com/w/cpp/utility/initializer_list
    * The underlying array may be allocated in read-only memory.
    * The program is ill-formed if an explicit or partial specialization of std::initializer_list is declared.
    */
    void check2() {
        std::vector<int> vec = { 1, 3, 4, 5, 6 };
        std::set<int> set = { 1, 3, 4, 5, 6 };
        auto l = { 1, 3, 4, 5, 6, 9 };
        vec.insert(vec.begin(), l.begin(), l.end());
    }

    /** 3. NEW CONTAINER std::forward_list. */
    /** 4. EMPLACE AND OTHER MOVE SEMANTICS */
    /** 5. CHRONO LIBRARY */

    /** 6. STATEFULL AND SCOPED ALLOCATORS
    * https://en.cppreference.com/w/cpp/named_req/Allocator#Stateful_and_stateless_allocators
    * https://stackoverflow.com/questions/12545072/what-does-it-mean-for-an-allocator-to-be-stateless
    * https://en.cppreference.com/w/cpp/memory/scoped_allocator_adaptor
    *
    * Generally, a stateful allocator type can have unequal values which denote distinct memory resources,
    * while a stateless allocator type denotes a single memory resource.
    *
    * The allocator object himself is discouraged to be stateful.
    * This means if you create an instance of std::allocator (or your own),
    * this instance should not contain any info about allocated blocks etc
    * - this info must be static and shared across all std::allocator instances.
    * Breaking this rule may lead to undefined behavior in STL libraries.
    *
    * Scoped allocator gives possibility to use allocators inside allocators.
    */
    template<typename T>
    struct Mallocator {
        typedef T value_type;

        Mallocator() = default;

        template<typename U> constexpr Mallocator(const Mallocator<U>&) noexcept {}

        [[nodiscard]] T* allocate(std::size_t n) {
            if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
                throw std::bad_array_new_length();
            if (auto p = static_cast<T*>(std::malloc(n * sizeof(T)))) {
                report(p, n);
                return p;
            }
            throw std::bad_alloc();
        }

        void deallocate(T* p, std::size_t n) noexcept {
            report(p, n, 0);
            std::free(p);
        }

    private:
        void report(T* p, std::size_t n, bool alloc = true) {
            std::cout << (alloc ? "Alloc   " : "Dealloc ") << sizeof(T) * n << " bytes at "
                << reinterpret_cast<void*>(p)
                << std::endl;
        }
    };
    template <class T, class U>
    bool operator==(const Mallocator <T>&, const Mallocator <U>&) { return true; }
    template <class T, class U>
    bool operator!=(const Mallocator <T>&, const Mallocator <U>&) { return false; }
    void check6() {
        std::vector<int, Mallocator<int> > vec(8);
        for (auto i = 0; i < 1e7; ++i)
            vec.push_back(i);
     //   print9("VEC CREATED!!!");
    }

    /** 7. STD::MOVE_INTERATOR
    * https://en.cppreference.com/w/cpp/iterator/move_iterator
    */
    void check7()
    {
        std::vector<std::string> v{ "this", "_", "is", "_", "an", "_", "example" };

        auto print_v = [&](auto const rem) {
            std::cout << rem;
            for (const auto& s : v)
                std::cout << std::quoted(s) << ' ';
            std::cout << '\n';
        };

        print_v("Old contents of the vector: ");

        std::string concat = std::accumulate(std::make_move_iterator(v.begin()),
            std::make_move_iterator(v.end()),
            std::string());
        // Now it is better to not use v again.

        /* An alternative that uses std::move_iterator directly could be:
        typedef std::vector<std::string>::iterator iter_t;
        std::string concat = std::accumulate(std::move_iterator<iter_t>(v.begin()),
                                             std::move_iterator<iter_t>(v.end()),
                                             std::string()); */

        print_v("New contents of the vector: ");
        std::cout << "Concatenated as string: " << quoted(concat) << '\n';
    }

    /** 8. RATION LIBRARY
    * https://en.cppreference.com/w/cpp/numeric/ratio
    * Compile-time rational arithmetic support.
    */
    void check8()
    {
        using twoThird = std::ratio<2, 3>;
        using sevenEights = std::ratio<7, 8>;
        using sum = std::ratio_add<twoThird, sevenEights>;
        std::cout << "2/3 + 7/8 = " << sum::num << "/" << sum::den << std::endl;
    }

    /** 9. ALGORITHMS */
    void print9(const std::vector<int>& vec) {
        for (const auto& el : vec)
            std::cout << el << ", ";
        std::cout << std::endl;
    }
    void check9() {
        std::vector<int> vec(10);
        std::cout << "Initial: "; print9(vec);
        std::cout << "std::iota(vec.begin(), vec.end(), -4): ";
        std::iota(vec.begin(), vec.end(), -4); print9(vec);
        std::cout << "std::any_of(>2): " << std::any_of(vec.cbegin(), vec.cend(), [](int el) { return el > 2; }) << std::endl;
        std::cout << "std::any_of(>8): " << std::any_of(vec.cbegin(), vec.cend(), [](int el) { return el > 8; }) << std::endl;
        std::cout << "std::all_of(>-2): " << std::all_of(vec.cbegin(), vec.cend(), [](int el) { return el > 8; }) << std::endl;
        std::cout << "std::none_of(>8): " << std::none_of(vec.cbegin(), vec.cend(), [](int el) { return el > 8; }) << std::endl;
        std::cout << "std::find_if_not(<1): " << *std::find_if_not(vec.cbegin(), vec.cend(), [](int el) { return el < 1; }) << std::endl;
        std::vector<int> copy1(10);
        std::copy_if(vec.cbegin(), vec.cend(), copy1.begin(), [](int el) { return el % 2; });
        std::cout << "copy if el % 2 ---> "; print9(copy1);
        std::copy_n(vec.cbegin(), 8, copy1.begin());
        std::cout << "copy 8 elements ---> "; print9(copy1);
        std::move(vec.begin(), vec.end(), copy1.begin());
        std::cout << "move 10 elements ---> "; print9(copy1);
        std::cout << "initial vec ---> "; print9(vec);
        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(vec.begin(), vec.end(), g);
        std::cout << "random shuffle ---> "; print9(vec);
        print9(copy1);
        std::cout << " is partitioned relating to <2 ---> " << std::is_partitioned(copy1.begin(), copy1.end(), [](int el) { return el < 2; });
        std::cout << " is partitioned relating to %2 ---> " << std::is_partitioned(copy1.begin(), copy1.end(), [](int el) { return el % 2; });
        std::vector<int> even(10), odd(10);
        std::partition_copy(vec.cbegin(), vec.cend(), odd.begin(), even.begin(), [](int el) {return el % 2; });
        std::cout << "partition copy even ---> "; print9(even);
        std::cout << "partition copy odd ---> "; print9(odd);
        std::cout << "partition point <2 ---> " << *std::partition_point(copy1.cbegin(), copy1.cend(), [](int el) {return el < 2; }) << std::endl;
        print9(vec);
        std::cout << " is sorted until " << *std::is_sorted_until(vec.cbegin(), vec.cend()) << std::endl;
        print9(copy1);
        std::cout << " is sorted " << std::is_sorted(copy1.cbegin(), copy1.cend()) << std::endl;

        // std::make_heap, is_heap and.

        const auto minmax = std::minmax(5, 4);
        std::cout << "(5,4) min - " << minmax.first << " max - " << minmax.second << std::endl;
        print9(vec);
        const auto [min, max] = std::minmax_element(vec.cbegin(), vec.cend());
        std::cout << " min=" << *min << " max=" << *max << std::endl;
        std::cout << "copy1 ---> "; print9(copy1);
        std::cout << "vec   ---> "; print9(vec);
        std::cout << "copy1 is permutation of vec ---> " << std::is_permutation(vec.cbegin(), vec.cend(), copy1.cbegin(), copy1.cend()) << std::endl;
        copy1.push_back(5);
        std::cout << "copy1 ---> "; print9(copy1);
        std::cout << "vec   ---> "; print9(vec);
        std::cout << "copy1 is permutation of vec ---> " << std::is_permutation(vec.cbegin(), vec.cend(), copy1.cbegin(), copy1.cend()) << std::endl;
        // std::uninitialized_copy_n - to use unitinialized memory
    }

    /** 10. THREAD LIBRARY
    * https://en.cppreference.com/w/cpp/thread
    */
    void check10() {
        // 10.1 HARDWARE CONCURRENCY
        std::cout << "10.1 HARDWARE CONCURRENCY " << std::endl;
        std::cout << "hardware concurrency ---> " << std::thread::hardware_concurrency() << std::endl;
        std::cout << std::endl;

        // 10.2 THREAD JOINING + STRONG MEMORY ORDER
        std::cout << "10.2 THREAD JOINING + STRONG MEMORY ORDER " << std::endl;
        {
            std::cout << std::endl << "JOINING THREADS" << std::endl;
            int val{ 0 };
            std::mutex m;
            auto f1 = [&]() {
                std::lock_guard<std::mutex> lock(m);
                val++;
                std::cout << std::this_thread::get_id() << " " << val << std::endl;
            };
            std::vector<std::thread> threads;
            for (auto i = 0; i < 10; ++i)
                threads.emplace_back(f1);
            for (auto i = 0; i < 10; ++i)
                threads[i].join();
            std::cout << "JOINING THREADS FINISHED" << std::endl << std::endl;
        }
        std::cout << std::endl;

        // 10.3 THREAD DETACHING + NOT STRONG MEMORY ORDER
        std::cout << "10.3 THREAD DETACHING + NOT STRONG MEMORY ORDER " << std::endl;
        {
            // Each thread increments value sequentially due to mutex synchronization.
            std::cout << std::endl << "DETACHING THREADS" << std::endl;
            std::atomic_int val{ 0 };
            std::mutex m;
            auto f1 = [&]() {
                val++;
                std::lock_guard<std::mutex> lock(m);
                std::cout << std::this_thread::get_id() << " " << val << std::endl;
            };
            std::vector<std::thread> threads;
            for (auto i = 0; i < 10; ++i)
                threads.emplace_back(f1);
            for (auto i = 0; i < 10; ++i)
                threads[i].detach();
            {
                std::lock_guard<std::mutex> lock(mutex);
                std::cout << "DETACHING THREADS FINISHED" << std::endl << std::endl;
            }
            std::this_thread::sleep_for(500ms);
        }
        std::cout << std::endl;

        /** 10.4 CALL_ONCE */
        std::cout << "10.4 CALL_ONCE " << std::endl;
        {
            std::once_flag flag;
            auto f = [&](bool doThrow) {
                try {
                    std::call_once(flag, [&]() {
                        if (doThrow)
                        {
                            std::cout << "call once will try again " << std::endl;
                            throw std::exception();
                        }
                        std::cout << " call once !!! " << std::endl;
                        });
                }
                catch (...) {}
            };

            std::vector<std::thread> ths;
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, false);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            ths.emplace_back(f, true);
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(ths.begin(), ths.end(), g);
            for (auto& th : ths)
                th.join();
        }
        std::cout << std::endl;

        /** 10.5 SMART MUTEX */
        std::cout << "10.5 SMART MUTEX " << std::endl;
        {
            std::timed_mutex tmut;
            auto f = [&]() {
                tmut.try_lock_for(100ms); // if to increase up to 5000ms then all records will be reported sequentially
                                          // like in the next section.
                for (auto i = 0; i < 10; ++i)
                {
                    std::cout << std::this_thread::get_id() << " " << i << std::endl;
                    std::this_thread::sleep_for(15ms);
                }
                tmut.unlock();
            };
            std::vector<std::thread> threads;
            for (auto i = 0; i < 10; ++i)
                threads.emplace_back(f);
            for (auto i = 0; i < 10; ++i)
                threads[i].join();
            {
                std::lock_guard<std::mutex> lock(mutex);
                std::cout << "JOINING THREADS FINISHED" << std::endl << std::endl;
            }
        }
        std::cout << std::endl;
        
        /** 10.7 USUAL MUTEX */
        std::cout << "10.5 MUTEX " << std::endl;
        {
            std::mutex tmut;
            auto f = [&]() {
                tmut.lock();
                for (auto i = 0; i < 10; ++i)
                {
                    std::cout << std::this_thread::get_id() << " " << i << std::endl;
                    std::this_thread::sleep_for(15ms);
                }
                tmut.unlock();
            };
            std::vector<std::thread> threads;
            for (auto i = 0; i < 10; ++i)
                threads.emplace_back(f);
            for (auto i = 0; i < 10; ++i)
                threads[i].join();
            {
                std::lock_guard<std::mutex> lock(mutex);
                std::cout << "JOINING THREADS FINISHED" << std::endl << std::endl;
            }
        }
        std::cout << std::endl;
    }

    /** 11. DEDALOCK AND RECURSIVE_MUTEX */
    std::mutex mut;
    std::recursive_mutex rmut;
    void fdead(int k) {
        std::lock_guard<std::mutex> lock(mut);
        if (k == 10)
        {
            std::cout << "10 Finished!" << std::endl;
            return;
        }
        std::cout << k << std::endl;
        fdead(++k); // here is deadlock
    };

    void flive(int k) {
        std::lock_guard<std::recursive_mutex> lock(rmut);
        if (k == 10)
        {
            std::cout << "10 Finished!" << std::endl;
            return;
        }
        std::cout << k << std::endl;
        flive(++k);
    };
    void check11() {
        //fdead(0); // deadlock
        flive(0);
    }

    /** 12. TRY_LOCK */
    std::mutex m;
    int shared_value{ 0 };
    int exclusive_value{ 0 };
    void f12() {
        this_thread::sleep_for(100ms);
        if (m.try_lock()) {
            shared_value++;
            std::cout << "f thread " << this_thread::get_id() << " modified shared value " << shared_value << std::endl;
            m.unlock();
            return;
        }
        else {
            exclusive_value++;
            std::cout << "f thread " << this_thread::get_id() << " modified exclusive value " << exclusive_value << std::endl;
        }
    }
    void g12() {
        m.lock();
        shared_value++;
        std::cout << "g thread " << this_thread::get_id() << " modified shared value " << shared_value << std::endl;
        this_thread::sleep_for(100ms);
        m.unlock();
        return;
    }
    void check12() {
        vector<thread> vec;
        for (auto i = 0; i < 10; ++i) {
            vec.emplace_back(f12);
            vec.emplace_back(g12);
        }
        for (auto& t : vec)
            t.join();
    }

    /** 13. ASYNC: NOT ACTUAL MULTITHREADING */
    void check13() {
        mutex m;
        unordered_set<thread::id> ids;
        int num{ 0 };
        int size{ 10000 };
        auto f = [&]() {
            cout << this_thread::get_id() << " thread " << num << std::endl;
            ids.insert(this_thread::get_id());
            num++;
        };
        for (auto i = 0; i < size; ++i) {
            async(f);
        }
        while (num < size) {
            this_thread::sleep_for(200ms);
        }
        cout << this_thread::get_id() << " main thread! " << " number of unique threads - " << ids.size() << std::endl;
    }

    /** 14. ASYNC: NOT ACTUAL MULTITHREADING */
    void check14() {
        mutex m;
        unordered_set<thread::id> ids;
        int num{ 0 };
        int size{ 10000 };
        auto f = [&]() {
            cout << this_thread::get_id() << " thread " << num << std::endl;
            ids.insert(this_thread::get_id());
            num++;
        };
        for (auto i = 0; i < size; ++i) {
            async(launch::async, f);
        }
        while (num < size) {
            this_thread::sleep_for(200ms);
        }
        cout << this_thread::get_id() << " main thread! " << " number of unique threads - " << ids.size() << std::endl;
    }

    /** 15. FUTURE */
    void check15() {
        std::cout << "FUTURE" << std::endl;
        auto iter = []() {
            int k = 0;
            for (auto i = 0; i < 1e9; ++i)
                k++;
            return k;
        };
        auto res = async(launch::async, iter);
        cout << "waiting future..." << std::endl;;
        res.wait();
        cout << "res=" << res.get() << std::endl;
    }

    /** 16. SHARED FUTURE */
    promise<void> prom1, prom2, shared_prom;
    future<void> f1 = prom1.get_future(), f2 = prom2.get_future(), f_shared = shared_prom.get_future();
    chrono::high_resolution_clock::time_point start;
    mutex m16;

    void f16() {
        this_thread::sleep_for(2s);
        prom1.set_value();
        f_shared.wait(); // waits for a signal from main
        lock_guard<mutex> lock(m16);
        cout << "f in thread " << this_thread::get_id() << " " << chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count() << std::endl;
    }

    void g16() {
        this_thread::sleep_for(3s);
        prom2.set_value();
        f_shared.wait(); // waits for a signal from main
        lock_guard<mutex> lock(m16);
        cout << "g in thread " << this_thread::get_id() << " " << chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count() << std::endl;
    }

    void check16() {
        auto res1 = async(launch::async, f16);
        auto res2 = async(launch::async, g16);
        cout << "Start " << endl;
        f1.wait();
        cout << "f1 got" << endl;
        f2.wait();
        cout << "f2 got" << endl;
        start = chrono::high_resolution_clock::now();
        shared_prom.set_value();
    }

    /** 17. DEFERED LOCK BY UNIQUE LOCK
    * https://en.cppreference.com/w/cpp/thread/unique_lock
    */
    struct A17 {
        int num{100};
        mutex m;
    };
    A17 to, from;
    void check17() {
        cout << "17. DEFERED LOCK BY UNIQUE LOCK" << endl;
        auto transfer = [&]() {
            unique_lock<mutex> l1(to.m, defer_lock);
            unique_lock<mutex> l2(from.m, defer_lock);
            // there is still no lock

            lock(l1, l2); // lock both locks without deadlock
            to.num += 10;
            from.num -= 10;

            // lock will automatically unlock in mutex deleter
        };

        vector<thread> ths;
        for (auto i = 0; i < 10; ++i) ths.emplace_back(transfer);
        for (auto i = 0; i < 10; ++i) ths[i].join();
        cout << "FINISH TO ---> " << to.num << " FROM ---> " << from.num << std::endl;
    }

    /** 18. CONDITIONAL VARIABLE
    * https://en.cppreference.com/w/cpp/thread/condition_variable
    * https://thispointer.com/c11-multithreading-part-7-condition-variables-explained/
    * The condition_variable class is a synchronization primitive that can be used to block a thread, 
    * or multiple threads at the same time, until another thread both modifies a shared variable (the condition), 
    * and notifies the condition_variable.
    * The thread that intends to modify the shared variable has to
    * acquire a std::mutex (typically via std::lock_guard)
    * perform the modification while the lock is held
    * execute notify_one or notify_all on the std::condition_variable (the lock does not need to be held for notification).
    * Even if the shared variable is atomic, 
    * it must be modified under the mutex in order to correctly publish the modification to the waiting thread.
    * 
    * Any thread that intends to wait on std::condition_variable has to 
    * acquire a std::unique_lock<std::mutex>, on the same mutex as used to protect the shared variable 
    * either check the condition, in case it was already updated and notified
    * execute wait, wait_for, or wait_until. 
    * The wait operations atomically release the mutex and suspend the execution of the thread.
    * When the condition variable is notified, a timeout expires, or a spurious wakeup occurs, 
    * the thread is awakened, and the mutex is atomically reacquired. 
    * The thread should then check the condition and resume waiting if the wake up was spurious. 
    * or
    * use the predicated overload of wait, wait_for, and wait_until, which takes care of the three steps above.
    * std::condition_variable works only with std::unique_lock<std::mutex>; this restriction allows for maximal efficiency on some platforms. 
    * std::condition_variable_any provides a condition variable that works with any BasicLockable object, such as std::shared_lock.
    */
    condition_variable cv18;
    mutex m18;
    bool ready{ false };
    bool processed{ false };
    string data;
    void thread_worker() {
        // Wait until signal
        unique_lock<mutex> lk(m18);
        cv18.wait(lk, []() {return ready; }); // if ready true then it is not spurious wake up

        // After the wait we own the lock
        cout << "Worker thread is processing data" << endl;
        data += " after processing ";

        // Send data back to other thread
        processed = true;
        cout << "Worker thread signals data processing completed" << endl;

        // intbu
        // Manual unlocking is done before notifying, to avoid waking up
        // the waiting thread only to block again (see notify_one for details)
        lk.unlock();
        cv18.notify_one();
    }
    void check18() {
        cout << "18. CONDITIONAL VARIABLE " << std::endl;
        thread worker(thread_worker);

        data = "Example data ";
        // Send data to the worker thread
        {
            lock_guard<mutex> lk(m18);
            ready = true;
            cout << "check18() siganls data ready for processing" << endl;
        }
        cv18.notify_one();

        // wait for the worker
        {
            unique_lock<mutex> lk(m18);
            cv18.wait(lk, [] {return processed; }); // if processed true then it is not spurious wake up
        }
        cout << "Back in main(), data = " << data << endl;
        worker.join();
    }

    /** 19. UNIQUE_LOCK vs LOCK_GUARD
    * https://stackoverflow.com/questions/20516773/stdunique-lockstdmutex-or-stdlock-guardstdmutex
    */

    /** 20. ATOMIC  */
    /** 21. CSTDINT */
    /** 22. RANDOM  */

    /** 23. REGEX  
    * https://www.regular-expressions.info/refcapture.html
    * About regex groups.
    */
    void check23() {
        {
            cout << "23. REGEX" << endl;
            string str = "She is a good girl.";
            regex reg("g.*l");
            smatch m;
            regex_search(str, m, reg);
            cout << "String matches the pattern" << endl;
            for (const auto& x : m)
                cout << x << endl;
        }
        {
            // It iterates through main result and each group.
            string str = "She is a good girl.";
            regex reg("(g)(.*)(l)");
            smatch m;
            regex_search(str, m, reg);
            cout << "String matches the pattern" << endl;
            for (const auto& x : m)
                cout << x << endl;
        }
    }

    /** 24. SYSTEM_ERROR */
    /** 25. UNORDERED_SET, UNORDERED_MAP */

    /** 26. TUPLE */
    void check26() {
        const auto t = make_tuple(1, 2.0f, "lalala", string("aaaa"));
        print(get<0>(t));
        print(get<1>(t));
        print(get<2>(t));
        print(get<3>(t));
    }

    /** 27. STD::FUNCTION 
    * https://en.cppreference.com/w/cpp/utility/functional/function
    */
    struct Foo {
        int operator()() {
            return 43;
        }
    };
    void printFromFunc(std::function<int()> func) {
        print(func());
    }
    void check27() {
        printFromFunc([]() {return 42; });
        printFromFunc(Foo());
    }
}