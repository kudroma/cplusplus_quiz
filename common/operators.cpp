#include "operators.hpp"

#include <compare>
#include <iostream>
#include <typeinfo>
#include <vector>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
    ((cout << args << " "), ...);
    cout << endl;
}

void common::Operators::auto_in_for()
{
    struct A {
        A() { print("Construction"); }
        A(const A& a) { print("Copy"); }
        virtual void f() { print("f()"); }
    };
    A a[2];
    print("Cycle with copy.");
    for (auto x : a) {
        x.f();
    }
    print("Cycle with ref.");
    for (auto& x : a) {
        x.f();
    }
}

/**
* New operator that allows to get three returned values: less, equal or greater.
*/
void common::Operators::spaceship_operator_20()
{
    int x = 5;
    int y = 10;

    std::strong_ordering result = x <=> y;

    if (result == std::strong_ordering::less) {
        std::cout << "x is less than y" << std::endl;
    }
    else if (result == std::strong_ordering::equal) {
        std::cout << "x is equal to y" << std::endl;
    }
    else {
        std::cout << "x is greater than y" << std::endl;
    }
}

/**
    The evaluation order of function argument expressions is unspecified,
    all we know is that they will all happen before ("be sequenced before") the contents of the called function.

    In particular, in the expression g(f1(), f2()), we don't know whether f1 or f2 will be sequenced first,
    we only know that they will both be sequenced before the body of g.

    Also: There isn't even a requirement on the implementation that f1 and f2
    gets evaluated in the same order each time. So after calling h twice, v can contain 1212, 2121, 1221 or 2112.
    �8.2.2: in the C++ standard:
    "The initialization of a parameter, including every associated value computation and side effect,
    is indeterminately sequenced with respect to that of any other parameter."
    And the helpful note in �4.6�17:
    "In an expression that is evaluated more than once during the execution of a program,
    unsequenced and indeterminately sequenced evaluations of its subexpressions
    need not be performed consistently in different evaluations."
*/
std::vector<int> v;
int f1() {
    v.push_back(1);
    return 0;
}
int f2() {
    v.push_back(2);
    return 0;
}
void g(int, int) {}
void h() {
    g(f1(), f2());
}
void common::Operators::args_evaluation_order()
{
    // Visual Studio makes the order sequential.
    for (auto i = 0; i < 200; ++i) {
        h();
        h();
        print(v[0], v[1], v[2], v[3]);
        v.clear();
    }
}

/*
   If ? operator has one option as lavalue (i) and another as prvalue (1) then it will be rvalue in general.
   So reference a won't actually reference i.
*/
void common::Operators::ternary_lvalue_rvalue()
{
    int i = 1;
    int const& a = i > 0 ? i : 1; // a is not reference to i
    const int& b = i;
    i = 2;
    std::cout << i << a << b;
}

/*
    Whether you post-increment or pre-increment i,  
    its value does not change until after the loop body has executed.
*/
void common::Operators::inc_and_dec_are_the_same_in_for()
{
    for (int i = 0; i < 3; i++)
        print(i);
    for (int i = 0; i < 3; ++i)
        print(i);
}
