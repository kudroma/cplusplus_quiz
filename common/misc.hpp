#pragma once

namespace common {

	class Misc
	{
	public:

		/** Things connected with extern keyword. */
		static void extern_keyword();

		/** Alternative tokens. */
		static void alternative_tokens();

		/** Namespace lookup. */
		static void namespace_lookup();

		/** Variable resolution. */
		static void variable_resolution();

		/** Function or object? */
		static void func_or_object();

		/** RVO. */
		static void rvo();

		/** Which exception will be thrown many times. */
		static void exception_during_stack_unwinding();
	};
}