#include "misc.hpp"

#include <iostream>
#include <typeinfo>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
    ((cout << args << " "), ...);
    cout << endl;
}

/*
    Due to the extern "C" specifications, A::x and B::x actually refer to the same variable.
    x is first initialized to 0, then main() starts, 0 is printed, x is incremented to 1, and finally 1 is printed.
    10.5 in the C++ standard:
    "Two declarations for a variable with C language linkage with the same name
    (ignoring the namespace names that qualify it)
    that appear in different namespace scopes refer to the same variable."
*/
namespace A {
    extern "C" int x;
};

namespace B {
    extern "C" int x;
};
int A::x = 0;
void common::Misc::extern_keyword()
{
    std::cout << B::x;
    A::x = 1;
    std::cout << B::x;
}

/*
    C++ provides alternative tokens for some punctuators. The two weird lines are exactly equivalent to
    int a[] = {1};
    std::cout << a[0];
    [lex.digraph]�5.5�1-2 in the C++ standard explains this:
    "Alternative token representations are provided for some operators and punctuators."
    "In all respects of the language, each alternative token behaves the same, respectively, as its primary token, except for its spelling."
    Then, a table of alternative tokens is provided, which includes
    - <% and %> for { and }
    - <: and :> for [ and ]
*/
void common::Misc::alternative_tokens()
{
    int a[] = <%1%>;
    print(a);
    print(a<:0:>);
}

/*
    Since the functions f are declared inside namespaces, and the call f(x::C()) is unqualified (not preceded by x:: or y::), this would normally not compile.
    However, due to argument-dependent name lookup a.k.a. "Koenig lookup", the behavior is well defined.
    With argument-dependent name lookup, the namespaces of the arguments to a function is added to the set of namespaces to be searched for that function. Since we're passing an x::C to f, the namespace x is also searched, and the function x::f is found.
*/
namespace x {
    class C {};
    void f(const C& i) {
        std::cout << "1";
    }
}

namespace y {
    void f(const x::C& i) {
        std::cout << "2";
    }
}
void common::Misc::namespace_lookup()
{
    f(x::C()); // namespace is deduced
}

/** is references to the global j.
* Then local j is used in cout.
*/
int j = 1;
void common::Misc::variable_resolution()
{
    int& i = j, j;
    j = 2;
    std::cout << i << j;
}

void common::Misc::func_or_object()
{
    struct X {
        X() { std::cout << "X"; }
    };

    struct Y {
        Y(const X& x) { std::cout << "Y"; }
        void f() { std::cout << "f"; }
    };
    Y y(X());
    //y.f();  // Compile errors as above the function was defined but not an object.
}

/** 
* RETURN VALUE OPTIMIZATION
*   No copy or other except ctor and dtor will be called.
*/
struct E
{
    E() { std::cout << "1"; }
    E(const E&) { std::cout << "2"; }
    E(E&&) { std::cout << "5"; }
    E& operator=(const E&) { std::cout << "4"; }
    E& operator=(E&&) { std::cout << "6"; }
    ~E() { std::cout << "3"; }
};

E f()
{
    auto ob = E();
    return ob;
}

void common::Misc::rvo()
{
    E e = f();
}

/**
* When exception is thrown during stack unwinding it will cause undefined behavior.
* The proper solution is to handle exception in destructor.
* Destructor call can be done during stack unwinding.
*/
void foo()
{
    throw std::exception("Exception in foo()");
}
void common::Misc::exception_during_stack_unwinding()
{
    class A
    {
    public:
        A() = default;
        ~A() { 
            throw std::runtime_error("Exception in ~A()"); 
        }
    };

    // Runtime error
    //try {
    //    A a;
    //}
    //catch (const std::exception& ex)
    //{
    //    print(ex.what());
    //    foo();
    //}
    //catch (...)
    //{
    //    print("Hello!");
    //}

    class B
    {
    public:
        B() = default;
        ~B() {
            try {
                throw std::runtime_error("Exception in ~B()");
            }
            catch (const std::runtime_error& ex)
            {
                print(ex.what());
            }
        }
    };

    int k = 0;

    // Works!
    try {
        B b;
        foo();
    }
    catch (const std::exception& ex)
    {
        print(ex.what());
    }
    catch (...)
    {
        print("Hello!");
    }
}
