#include "standard.hpp"

#include <iostream>
#include <typeinfo>
#include <vector>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
	((cout << args << " "), ...);
	cout << endl;
}

/**
* If both elements are equal min and max return reference to the first element.
* Therefore 11 11 will be shown in the example below.
*/
void common::Standard::min_max()
{
    int x = 10;
    int y = 10;

    const int& max = std::max(x, y);
    const int& min = std::min(x, y);

    x = 11;
    y = 9;

    print(max, min);
}

/** 
* The end string symbols will be returned in this case.
*/
void common::Standard::string_end()
{
    std::string out{ "Hello world" };
    std::cout << (out[out.size()] == '\0');
    std::cout << out[out.size()];
}

/*
    Even though these constructor invocations look strikingly similar,
    the one that takes a C-style string and the one that takes a
    basic_string behave differently with respect to the second argument.
    For the former, it means "until this position",
    but for the latter it means "from this position".
*/
void common::Standard::basic_vs_cstyle_string()
{
    using namespace std::string_literals;
    std::string s1("hello world", 5);
    std::string s2("hello world"s, 5);

    print(s1); // hello
    print(s2); // world
}

/*
    Since C++11, std::vector has a one parameter constructor ( + allocator). [vector.cons]�26.3.11.2�3 in the standard):
    explicit vector(size_type n, const Allocator& = Allocator())
    which constructs a vector with n value-initialized elements.
    Each value-initialization calls the default Foo constructor.
*/
struct Foo
{
    Foo() { std::cout << "a"; }
    Foo(const Foo&) { std::cout << "b"; }
};
void common::Standard::vector_construction()
{
    std::vector<Foo> bar(5); // no copy ctor will be called
}
