#pragma once

/** 1. DEFINITION
* https://www.informatica.com/services-and-training/glossary-of-terms/low-latency-definition.html
* 
* 1. DEFINITION
* ow latency describes a computer network that is optimized to process a very high volume of data messages with minimal delay (latency). 
* These networks are designed to support operations that require near real-time access to rapidly changing data.
* 
* 2. USE-CASES
* Low latency is desirable in online gaming as it contributes to a more realistic gaming environment. 
* However, the term low latency is most often used to describe specific business use cases, 
* in particular high-frequency trading in capital markets.
*/