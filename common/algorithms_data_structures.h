#pragma once

#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <random>
#include <string>
#include <type_traits>
#include <vector>

using namespace std;

template<typename T>
void print(const vector<T>& a) {
    for (const auto& e : a)
        cout << e << " ";
    cout << endl;
}

template<typename T, typename = enable_if<is_scalar_v<T>>>
void print(T a) {
    cout << a << " " << endl;
}

template<typename T>
bool sorted(const vector<T>& a) {
    for (int i = 0; i < a.size() - 1; ++i) {
        if (a[i + 1] < a[i]) {
            return false;
        }
    }
    return true;
}

template<typename T>
vector<T> randomVec(size_t size) {
    vector<float> a;
    a.resize(size);
    random_device r;
    mt19937 e2(r());
    normal_distribution<> n(0, 2e3);
    for (auto& e : a)
        e = n(e2);
    return a;
}

template<typename T>
using Algs = map<string, std::function<void(vector<T>&&)>>;

template<typename T>
void test(const Algs<T>& algs = Algs<T>()) {
    using namespace chrono;
    static const map<size_t, string> desc = {
        { 1000, "1000"}, 
        { 5000, "5000"}, 
        { 1e4, "10000" },
        { 2e4, "20000" },
        { 5e4, "50000" },
        { 1e5, "100000" },
        { 5e5, "500000" },
        { 1e6, "1000000" },
        { 5e6, "5000000" },
        { 1e7, "10000000" },
    };
    static const int timeThreshold{ 6000 };
    const int w = 10;
    cout << setw(w) 
        << "name" << setw(w) 
        << "1000" << setw(w) 
        << "5000" << setw(w) 
        << "1e4" << setw(w) 
        << "2e4" << setw(w)
        << "5e4" << setw(w)
        << "1e5" << setw(w)
        << "5e5" << setw(w)
        << "1e6" << setw(w)
        << "5e6" << setw(w)
        << "1e7" << setw(w)
        << endl << endl;

    for (const auto& algIt : algs) {
        cout << setw(w) << algIt.first;
        for (auto it = desc.cbegin(); it != desc.cend(); ++it) {
            int attempts{ 10 };
            int totalTime{};
            bool tooSlow{ false };
            for (auto i = 0; i < attempts; ++i) {
                auto a = randomVec<T>(it->first);
                const auto start = high_resolution_clock::now();
                algIt.second(move(a));
                const auto end = high_resolution_clock::now();
                const auto curTime = duration_cast<milliseconds>(end - start).count();
                totalTime += curTime;
                if (!sorted(a)) {
                    cout << endl << "NOT SORTED!!!" << endl;
                    return;
                }
                if (curTime > timeThreshold) {
                    tooSlow = true;
                    break;
                }
            }
            if (tooSlow) {
                cout << setw(w) << "SLOW";
                break;
            }
            totalTime /= attempts;
            cout << setw(w) << totalTime;
        }
        cout << endl << endl;
    }
}

/** 2. SORTING ALGORITHMS */
namespace sorting {
    /** BUBBLE SORT
    * https://en.wikipedia.org/wiki/Bubble_sort
    * O(n^2)
    */
    template<typename T>
    void bubleSort(vector<T>&& a) {
        const size_t size = a.size();
        bool sorted{ false };
        while (!sorted) {
            sorted = true;
            for (size_t i = 0; i < size - 1; ++i) {
                if (a[i] > a[i + 1]) {
                    sorted = false;
                    swap(a[i], a[i + 1]);
                }
            }
        }
    }

    /** INSERTION SORT
    * https://en.wikipedia.org/wiki/Insertion_sort
    * O(n2) but more efficient in practice than other n2 algorithms
    */
    template<typename T>
    void insertionSort(vector<T>&& a) {
        vector<float> b;
        b.reserve(a.size());
        auto it = a.cbegin();
        int ind{};
        while (it != a.cend()) {
            b.push_back(*it);
            ind = b.size() - 1;
            while (ind > 0) {
                if (b[ind] < b[ind - 1])
                    swap(b[ind], b[ind - 1]);
                ind--;
            }

            ++it;
        }
        swap(a, b);
    }

    /** MERGE SORT
    * https://en.wikipedia.org/wiki/Merge_sort
    */
    template<typename T>
    using It = decltype(vector<T>().begin());
    template<typename T>
    void merge(int iL, int iM, int iR, vector<T>& vec, vector<T>& buf) {
        auto itL = iL, itR = iM + 1, it = iL;
        while (it <= iR) {
            if (itL > iM) {
                buf[it] = vec[itR];
                itR++;
            }
            else if (itR > iR) {
                buf[it] = vec[itL];
                itL++;
            }
            else if (vec[itL] < vec[itR]) {
                buf[it] = vec[itL];
                itL++;
            }
            else {
                buf[it] = vec[itR];
                itR++;
            }
            it++;
        }
    }

    template<typename T>
    void mergeSortImpl(int ind1, int ind2, vector<T>& a, vector<T>& buf) {
        if (ind1 == ind2)
            return;
        else {
            auto mid = (ind1 + ind2) / 2;
            mergeSortImpl(ind1, mid, a, buf);
            mergeSortImpl(mid + 1, ind2, a, buf);
            merge(ind1, mid, ind2, a, buf);
            auto itS = buf.begin() + ind1;
            auto itE = buf.begin() + ind2 + 1;
            auto itD = a.begin() + ind1;
            copy(itS, itE, itD);
        }
    }
    template<typename T>
    void mergeSort(vector<T>&& vec) {
        auto buf = vec;
        mergeSortImpl(0, vec.size() - 1, vec, buf);
    }

    /** QUICK SORT */
    template<typename T>
    int partition(vector<T>& a, int in, int out, int mid) {
        auto refId = mid;
        auto tempId = refId;
        auto i = in;
        while(i < refId) {
            if (a[i] >= a[refId]) {
                if (i == refId - 1) {
                    swap(a[i], a[refId]);
                    refId = i;
                    break;
                }
                tempId = refId;
                refId--;
                swap(a[tempId], a[refId]);
                swap(a[tempId], a[i]);
            }
            else
                i++;
        }

        i = mid + 1;
        while (i <= out) {
            if (a[i] < a[refId]) {
                tempId = refId;
                refId++;
                swap(a[tempId], a[refId]);
                if(i != refId)
                    swap(a[tempId], a[i]);
            }
            i++;
        }
        return refId;
    }
    template<typename T>
    void quickSortImpl(vector<T>& vec, int indS, int indE) {
        if (indE <= indS)
            return;
        if (indE - indS <= 1) {
            if (vec[indE] < vec[indS])
                swap(vec[indE], vec[indS]);
            return;
        }
        const int mid = (indS + indE) / 2;
        const int refId = partition<T>(vec, indS, indE, mid);
        if(refId - 1 >= indS)
            quickSortImpl<T>(vec, indS, refId - 1);
        if(refId + 1 <= indE)
            quickSortImpl<T>(vec, refId + 1, indE);
    }
    template<typename T>
    void quickSort(vector<T>&& vec) {
        quickSortImpl<T>(vec, 0, vec.size() - 1);
    }

    /** HEAP SORT 
    * https://en.wikipedia.org/wiki/Heapsort
    * https://ru.wikipedia.org/wiki/%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D0%B0%D1%8F_%D0%BA%D1%83%D1%87%D0%B0#:~:text=%D0%92%20%D0%B4%D0%B0%D0%BB%D1%8C%D0%BD%D0%B5%D0%B9%D1%88%D0%B5%D0%BC%20%D1%80%D0%B0%D1%81%D1%81%D0%BC%D0%B0%D1%82%D1%80%D0%B8%D0%B2%D0%B0%D1%8E%D1%82%D1%81%D1%8F%20%D1%82%D0%BE%D0%BB%D1%8C%D0%BA%D0%BE%20max%2Dheap.
    * https://habr.com/ru/company/otus/blog/551254/
    * https://habr.com/ru/company/otus/blog/460087/
    */
    template<typename T>
    void heapify(vector<T>& v, int n, int i) { // i - �������� ����, n - ������ ����
        auto largest = i;
        const auto l = 2 * i + 1;
        const auto r = 2 * i + 2;
        if (l < n && v[l] > v[largest])
            largest = l;
        if (r < n && v[r] > v[largest])
            largest = r;
        if (largest != i) {
            swap(v[i], v[largest]);
            heapify(v, n, largest);
        }
    }
    template<typename T>
    void heapSort(vector<T>&& vec) {
        const auto n = vec.size();

        // build heap
        for (int i = n / 2 - 1; i >= 0; --i)
            heapify(vec, n, i);

        // move elements from heap one by one
        for (int i = n - 1; i >= 0; --i) {
            swap(vec[0], vec[i]); // move max element to the end of the vec
            heapify(vec, i, 0); // rebuild max heap for the rest of the vector
        }
    }

    void compare() {
        const Algs<float> algs = {
            {"buble", bubleSort<float>},
            {"insertion", insertionSort<float>},
            {"merge", mergeSort<float>},
            {"quick", quickSort<float>},
            {"heap", heapSort<float>},
        };
        test(algs);
    }
}