

namespace common {

	class Containers {
	public:

		/** Demonstrate how additional copy is called in map instantiation. */
		static void map_unnecessary_copy();

		/** Why iteration over vector is faster. */
		static void iterations_over_vector_vs_over_list();

		/** noexcept optimization. */
		static void noexcept_optimization();
	};

}