#pragma once

/**
* Documentation - https://en.cppreference.com/book/intro/smart_pointers
* unique_ptr    - https://en.cppreference.com/w/cpp/memory/unique_ptr
* shared_ptr    - https://en.cppreference.com/w/cpp/memory/shared_ptr
* 
*/
namespace common {
	class SmartPointers {
	public:

		static void check();

		/** What is the size of the unique_ptr. */
		static void unique_ptr_size();

		/** What is the weak ptr. */
		static void weak_ptr_17();

		/** Comparison of smart pointers. */
		static void compare_ptr_20();

		/** Atomic with shared pointer. */
		static void atomic_with_shared_20();

		/** Make_shared with custom allocator. */
		static void make_shared_custom_allocator_20();

		/** make_shared for arrays. */
		static void make_shared_for_arrays_20();

		/** Shared pointer memory leak. */
		static void ring_memory_leak_with_shared_pointers();

		/** Unique ptr moving. */
		static void move_unique_ptr();

		/** Make shared efficiency and safety. */
		static void make_shared_efficiency_and_safety();

		/** When control block is removed. */
		static void make_shared_control_block_removed();

		/** Custom deallocator. */
		static void custom_deallocator();
	};
}