#pragma once

/**
* Documentation - https://en.cppreference.com/w/cpp/language/lambda
* 
* 1. DEFINITION
*   Constructs a closure: an unnamed function object capable of capturing variables in scope.
* 
* 2. GENERAL SYNTAX
*   [ captures ] <tparams>(optional) ( params ) lambda-specifiers { body }
* 
* 3. CAPTURES
*   If the lambda-expression captures anything by copy (either implicitly with capture clause [=] 
*   or explicitly with a capture that does not include the character &, e.g. [a, b, c]), 
*   the closure type includes unnamed non-static data members, declared in unspecified order, 
*   that hold copies of all entities that were so captured. 
*   Those data members that correspond to captures without initializers are direct-initialized 
*   when the lambda-expression is evaluated. Those that correspond to captures with initializers are initialized 
*   as the initializer requires (could be copy- or direct-initialization). If an array is captured, 
*   array elements are direct-initialized in increasing index order. 
*   The order in which the data members are initialized is the order in which they are declared (which is unspecified).
*    
*/
namespace common {
    class Lambdas
    {
    public:
        static void check();

        /** Study what lambda can capture by default. */
        static void default_capture();

        /** Mutable lambdas. */
        static void mutable_lambda();

        /** Constexpr lambdas. �++17 */
        static void constexpr_lambda();

        /** Consteval lambdas. �++20 */
        static void consteval_lambdas();

        /** Trailing return type. */
        static void trailing_return_type();

        /** Template lambdas. C++14 */
        static void template_lambda();

        /** Nullary lambda. */
        static void nullary_lambda();

        /** Generic lambda with auto. C++14. */
        static void generic_lambda();

        /** Mixed lambda with auto and template. C++20. */
        static void generic_template_lambda();

        /** this pointer scope in lambda. */
        static void this_in_lambda();

        /** Lambda as function argument. */
        static void lambda_as_function_argument();

        /** Lambda capture rules. C++11 - C++20. */
        static void lambda_capture_rules();

        /** Lambda capture types. C++11 - C++20. */
        static void lambda_capture_types();

        /** Lambda capture of this pointer. C++17. */
        static void lambda_capture_this();

        /** Lambda as return type. */
        static void lambda_return_type();

        /** Lambda as parameter of function. */
        static void lambda_as_argument();

        /** Lambda structured bindings. */
        static void lambda_structured_bindings();

        /** Capture of non-static class members. */
        static void capture_non_static_class_members();
    };
}