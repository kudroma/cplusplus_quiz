#include "containers.hpp"

#include <chrono>
#include <iostream>
#include <list>
#include <map>
#include <vector>


using namespace std;

template<typename ... T>
void print(T&& ... args) {
    ((cout << args << " "), ...);
    cout << endl;
}

/**
* m[7] calls default constructor.
* C(1) calls another constructor.
* = calls assignment operator.
*/
bool default_constructed = false;
bool constructed = false;
bool assigned = false;
void common::Containers::map_unnecessary_copy()
{
    class C {
    public:
        C() { default_constructed = true; }
        C(int) { constructed = true; }
        C& operator=(const C&) { assigned = true; return *this; }
    };

    map<int, C> m;

    m[7] = C(1);
    print(default_constructed, constructed, assigned);

    default_constructed = constructed = assigned = false;

    // It is more effective.
    m.insert({ 7, C(1) });
    print(default_constructed, constructed, assigned);
}

/**
* Processor loads to the memory cash line that is usually 64 bytes.
* Vector saves several elements in that data chunk. So iteration over it will be faster.
* As cpu loads several elements to the cash.
*/
void common::Containers::iterations_over_vector_vs_over_list()
{
    {
        // vector
        std::vector<char> v(1e8);
        auto start = std::chrono::high_resolution_clock::now();
        for (auto it = v.begin(); it != v.end(); ++it)
        {
            (*it)++;
        }
        auto end = std::chrono::high_resolution_clock::now();
        print("char vector - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());

        // list
        std::list<char> l(1e8);
        start = std::chrono::high_resolution_clock::now();
        for (auto it = l.begin(); it != l.end(); ++it)
        {
            (*it)++;
        }
        end = std::chrono::high_resolution_clock::now();
        print("char list - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
    }

    {
        // vector
        std::vector<int> v(1e8);
        auto start = std::chrono::high_resolution_clock::now();
        for (auto it = v.begin(); it != v.end(); ++it)
        {
            (*it)++;
        }
        auto end = std::chrono::high_resolution_clock::now();
        print("int vector - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());

        // list
        std::list<int> l(1e8);
        start = std::chrono::high_resolution_clock::now();
        for (auto it = l.begin(); it != l.end(); ++it)
        {
            (*it)++;
        }
        end = std::chrono::high_resolution_clock::now();
        print("int list - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
    }



    {
        struct A
        {
            double a;
            double b;
            double c;
            double d;
            double e;
            double f;
            double g;
            double h;
            double i;
        };
        print("sizeof(A) = ", sizeof(A));

        // vector
        std::vector<A> v(1e8);
        auto start = std::chrono::high_resolution_clock::now();
        for (auto it = v.begin(); it != v.end(); ++it)
        {
            (*it).a++;
        }
        auto end = std::chrono::high_resolution_clock::now();
        print("A vector - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());

        // list
        std::list<A> l(1e8);
        start = std::chrono::high_resolution_clock::now();
        for (auto it = l.begin(); it != l.end(); ++it)
        {
            (*it).a++;
        }
        end = std::chrono::high_resolution_clock::now();
        print("A list - ", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
    }
}

/**
* If move ctor is marked as noexcept then it is used by vector implicitely when additional copying occurs.
* If move ctor is not marked as noexcept then no optimization occurs.
* Difference could be dramatical.
*/
class A
{
    std::vector<int> data;
public:
    static int copies;
    static int moves;
    A() { data = std::vector<int>(1e4); }
    A(const A& other) { this->data = other.data; copies++; }
    A(A&& other) { this->data = std::move(other.data); moves++; }
};
int A::copies = 0;
int A::moves = 0;

class B
{
    std::vector<int> data;
public:
    static int copies;
    static int moves;

    B() { data = std::vector<int>(1e4); }
    B(const B& other) { this->data = other.data; copies++; }
    B(B&& other) noexcept { this->data = std::move(other.data); moves++; }
};
int B::copies = 0;
int B::moves = 0;

void common::Containers::noexcept_optimization()
{
    
    {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<A> vector;
        for (auto i = 0; i < 1e5; ++i)
            vector.push_back(A());
        auto end = std::chrono::high_resolution_clock::now();
        print("Without noexcept: time:", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
        print("copies:", A::copies);
        print("moves:", A::moves);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<B> vector;
        for (auto i = 0; i < 1e5; ++i)
            vector.push_back(B());
        auto end = std::chrono::high_resolution_clock::now();
        print("Without noexcept: time:", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
        print("copies:", B::copies);
        print("moves:", B::moves);
    }
}
