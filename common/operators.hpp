#pragma once

namespace common {

	class Operators
	{
	public:
		/** Auto keyword in for. */
		static void auto_in_for();

		/** Spaceship operator. */
		static void spaceship_operator_20();

		/** Function args evaluation order. */
		static void args_evaluation_order();

		/** Ternary operator lvalue or rvalue. */
		static void ternary_lvalue_rvalue();

		/** Inc and dec are the same in for. */
		static void inc_and_dec_are_the_same_in_for();

	};
}