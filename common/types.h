#pragma once

namespace common {

	class Types
	{
	public:
		/** Decltype. */
		static void decltype_keyword();

		/** Functions overload order. */
		static void functions_overload_order();

        /** What is sizeof of an array. */
        /**
            This question compares three ways for a function to take an array as parameter, while two of them are actually the same.
            In main, the array is of array type, therefore the sizeof operator returns the size of the array in terms of bytes.
        */
        static void array_sizeof();

		/** Implicit conversion sequence. */
		static void implicit_conversion_sequence();

		/** nullptr is not actually pointer. */
		static void nullptr_is_not_pointer();

		/** sizeof() trick. */
		static void sizeof_trick();

		/** Typedef for function. */
		static void typedef_for_function();

		/** Ref with other type. */
		static void ref_with_other_type();

		/** Char diff for int. */
		static void char_diff_for_int();

		/** Const of params is not function type. */
		static void const_of_params_is_not_function_type();

		/** Char to int implicit. */
		static void char_to_int_implicit();

		/** Type conversion rang. */
		static void type_conversion_rang();
	};
}