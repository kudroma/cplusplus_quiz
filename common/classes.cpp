#include "classes.hpp"

#include <iostream>
#include <memory>
#include <typeinfo>
#include <vector>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
    ((cout << args << " "), ...);
    cout << endl;
}

/** 
* Member variables are initialized before the constructor is called. 
* The destructor is called before member variables are destroyed.
*/
void common::Classes::initialization_order()
{
    class A {
    public:
        A() { std::cout << 'a'; }
        ~A() { std::cout << 'A' << std::endl; }
    };

    class B {
    public:
        B() { std::cout << 'b'; }
        ~B() { std::cout << 'B'; }
        A a;
    };

    class C {
    public:
        C() : a(A()) { std::cout << 'c'; }
        ~C() { std::cout << 'C'; }
        A a;
    };

    {
        B b;
    }
    C c;
}

/** 
* From me: B over-defines f() and it is not forbidden by compiler. 
*/
void common::Classes::virtual_and_member_function_resolution()
{
    class A {
    public:
        virtual void f() { std::cout << "A"; }
    };

    class B : public A {
    private:
        void f() { std::cout << "B"; }
    };

    auto g = [](A& a) { a.f(); };

    B b;
    g(b);
}

/*
    �11.3.1 in the C++ Standard, 
    "The cv-qualifiers [e.g., const] apply to the pointer and not to the object pointed to."
*/
void common::Classes::pointers_with_const()
{
    class C {
    public:
        void foo() { print("foo()"); }
        void foo() const { print("foo() const"); }
    };

    struct S {
        std::vector<C> v;
        std::unique_ptr<C> u;
        C* const p;

        S()
            : v(1)
            , u(new C())
            , p(u.get())
        {}
    };
    
    S s;
    const S& r = s;

    s.v[0].foo();
    s.u->foo();
    s.p->foo();

    r.v[0].foo(); // Only this will call foo() const;
    r.u->foo();
    r.p->foo();
}

/*
    �6.6.3 in the standard:
    "It is implementation-defined whether the dynamic initialization of a non-local non-inline variable with static storage duration 
    is sequenced before the first statement of main or is deferred. 
    If it is deferred, it strongly happens before any non-initialization odr-use of any non-inline function 
    or non-inline variable defined in the same translation unit as the variable to be initialized."

    Since A() is not constexpr, the initialization of a is dynamic. 
    There are two possibilities:
    - a is initialized before main() is called, i.e. before b is initialized.
    - a is not initialized before main(). 
    It is however guaranteed to be initialized before the the use of any function defined in the same translation unit, 
    i.e. before the constructor of b is called.

    When execution reaches B b, it is initialized as normal. 
    Static local variables are initialized the first time control passes through their declaration, 
    so c is initialized next. 
    As main() is exited, its local variable b goes out of scope, and is destroyed. 
    Finally, all static variables are destroyed in reverse order of their initialization, first c, then a.
*/
class A {
public:
    A(const char * name) { print("A()", name); }
    ~A() { print("~A()"); }
};

class B {
public:
    B(const char* name) { print("B()", name); }
    ~B() { print("~B()"); }
};

class C {
public:
    C(const char* name) { print("C()", name); }
    ~C() { print("~C()"); }
};
void foo() { static C cs("cs"); } // Last initialized and destructed as first static member before bs and as.
void common::Classes::instantiation_destruction_order()
{
    static B bs("bs"); // Second initialized and destructed before as.
    A a("a"); // Third initialized and destructed second;
    B b("b"); // Second initialized and desctructed first.
    foo();
}
//static A as("as"); // First initialized and last destructed.


/** It is used to effectively govern by resources. */
class MyString {
private:
    char* data;
    char* name;

public:
    // Constructor to create a MyString object from a C-style string
    MyString(const char* name2, const char* str) {
        // Allocate memory for the string and copy the data
        if (str) {
            data = new char[strlen(str) + 1];
            strcpy(data, str);
            name = new char[strlen(name2) + 1];
            strcpy(name, name2);
        }
        else {
            data = nullptr;
        }
    }

    // Move constructor
    MyString(MyString&& other) noexcept {
        // Steal the data from 'other' and set its data pointer to nullptr
        data = other.data;
        name = other.name;
        other.data = nullptr;
        other.name = const_cast<char*>("default");
    }

    // Move Assignment Operator
    MyString& operator=(MyString&& other) {
        data = other.data;
        name = other.name;
        other.data = nullptr;
        other.name = const_cast<char*>("default");
        return *this;
    }

    // Destructor to release the allocated memory
    ~MyString() {
        delete[] data;
    }

    // Print the string
    void print() const {
        if (data) {
            std::cout << name << " : " << data;
        }
        else {
            std::cout << name << " : (empty)";
        }
        std::cout << std::endl;
    }
};
struct NoMoveString {
    char* data;
    NoMoveString(const char* str) {
        // Allocate memory for the string and copy the data
        if (str) {
            data = new char[strlen(str) + 1];
            strcpy(data, str);
        }
        else {
            data = nullptr;
        }
    }

    // Print the string
    void print() const {
        if (data) {
            std::cout << data;
        }
        else {
            std::cout << " : (empty)";
        }
        std::cout << std::endl;
    }
};
void common::Classes::move_constructor()
{
    MyString str1("str1", "Hello");
    str1.print();
    MyString str2(std::move(str1));
    str1.print(); // use of moved - undefined
    str2.print();
    MyString str22 = std::move(str2);
    str2.print(); // use of moved - undefined
    str22.print();

    NoMoveString str3("Hello");
    str3.print();
    NoMoveString str4(std::move(str3));
    str3.print();
    str4.print(); // there was actually copying.
}


/** 
* Compiler optimization to not call additional copy and move constructors.
*/
class MyObject {
public:
    MyObject() {
        std::cout << "Default constructor called." << std::endl;
    }

    MyObject(const MyObject&) {
        std::cout << "Copy constructor called." << std::endl;
    }
};
MyObject createObject() {
    MyObject obj;
    return obj;
}
void common::Classes::guaranteed_copy_elision_17() 
{
    // Copy constructor or assignemnt operator won't be actually called.
    // Only constructor will be called.
    MyObject result = createObject();
}

/** 
* Cast doesn't change call of virtual function. 
* It doesn't work for virtual function.
*/
class A1 {
public:
    virtual void f() { print("A::f()"); }
    void g() { print("A::g()"); }
};

class B1 : A1 {
public:
    void f() override { print("B::f()"); }
    void g() { print("B::g()"); }
};
void common::Classes::cast_doesnt_change_virtual_function()
{
    B1 b;
    ((A1&)b).f(); // f from B1 will be called
    ((A1&)b).g(); // g from A1 will be called
}

/**
* According to the standard X(object) actually is the same as X object.
*/
struct X {
    X() { std::cout << "1"; }
    X(const X&) { std::cout << "3"; }
    ~X() { std::cout << "2"; }

    void f() { std::cout << "4"; }

} object;
void common::Classes::tricky_way_to_create_an_object()
{
    X(object);
    object.f();
}

/**
* Initialization order is as data members declared in the class.
* a member will be undefined as it will be initialized first.
*/
class A3 {
private:
    int a;
    int b;
public:
    A3(int x) : b(x), a(b) {}
    void dump() {
        std::cout << "a=" << a << " b=" << b << std::endl;
    }
};
void common::Classes::initialization_order_of_data_members()
{
    A3 a(5);
    a.dump();
}

/** 
* Virtual destructor allows to call destructor of derived class if it is reference of base class.
* In the example below if destructor is not virtual then m_data in B4 won't be cleaned up.
*/
class A4 {
public:
    A4() { print("A()"); }
    A4(const A4&) = default;
    A4& operator=(const A4&) = default;
    /*virtual*/ ~A4() { print("~A()"); }
};
class B4 : public A4 {
private:
    const char* m_data{ nullptr };
public:
    B4(const char* data) : A4() { print("B()"); m_data = data; }
    B4(const B4&) = default;
    B4& operator=(const B4&) = default;
    ~B4() { print("~B()"); }
};
void common::Classes::virtual_destructor()
{
    A4* ob = new B4("hello");
    delete ob; // if destructor of A is virtual then both destructors will be called. 
               // otherwise only destructor of A.
}

/** 
* SLICING
* g(A a) takes an object of type A by value, not by reference or pointer. 
    This means that A's copy constructor is called on the object passed to g() 
    (no matter if the object we passed was of type B), 
    and we get a brand new object of type A inside g(). 
    This is commonly referred to as slicing.
*/
class As {
public:
    virtual void f() { std::cout << "A"; }
};
class Bs : public As {
public:
    void f() { std::cout << "B"; }
};
void g(As a)  { a.f(); }
void g1(As& a) { a.f(); }
void common::Classes::slicing()
{
    Bs b;
    g(b); // Copy ctor of A is used to create new object A.
    g1(b); // This doesn't call copy ctor so version of B::f() is called.
}

/**
*   It turns out that void f(int) isn't even in scope, and is not considered for overload resolution at all.
*   When the name f is introduced in Derived, it hides the name f that was introduced in Base.
*/
void common::Classes::hidden_method_in_derived_class()
{
    struct Base {
        void f(int) { std::cout << "i"; }
    };

    struct Derived : Base {
        void f(double) { std::cout << "d"; }
    };

    Derived d;
    int i = 0;
    d.f(i);
}

/** 
* In order to use dynamic_cast method should have vtable.
* dynamic_cast takes class hierarchy information from the vtable.
*/
void common::Classes::dynamic_cast_vs_virtuality()
{
    class A
    {
    public:
        void do_a() { print("do_a"); }
    };

    class B : A
    {
    public:
        void do_b() { print("do_b"); }
    };

    A* a = new A();
    // B* b = dynamic_cast<B*>(a); // Compiler error as dynamic_cast can't define information about class hierarchy

    class A_V
    {
        virtual void do_a() { print("do_a"); } // A_V is polymorhic and dynamic_cast is appliable.
    };

    class B_V
    {
        void do_b() { print("do_b"); }
    };

    A_V* av = new A_V();
    B_V* bv = dynamic_cast<B_V*>(av);
    print(bv == nullptr); // will return nullptr as casting is impossible.
}
