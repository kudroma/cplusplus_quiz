#include "types.h"

#include <iostream>
#include <typeinfo>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
	((cout << args << " "), ...);
	cout << endl;
}

/**
*  decltype is used to deduce type of one variable in order to use as type of another variable.
*/
void common::Types::decltype_keyword()
{
	auto add = [](const auto& a, const auto& b) { return a + b; };
	decltype(add(3, 4.0)) b;
	print(typeid(b).name());
}

/**
   A string literal is not a std::string, but a const char[] .
   If the compiler was to choose f(const std::string&),
   it would have to go through a user defined conversion and create a temporary std::string.
   Instead, it prefers f(const void*), which requires no user defined conversion.
*/
void f(const std::string&) { std::cout << 1; }
void f(const void*) { std::cout << 2; }
void common::Types::functions_overload_order()
{        
    f("foo");
    const char* bar = "bar";
    f(bar);
}

/**
    This question compares three ways for a function to take an array as parameter, while two of them are actually the same.
    In main, the array is of array type, therefore the sizeof operator returns the size of the array in terms of bytes.
*/
size_t get_size_1(int* arr) { return sizeof arr; }
size_t get_size_2(int arr[]) { return sizeof arr; }
size_t get_size_3(int(&arr)[10]) { return sizeof arr; }
void common::Types::array_sizeof()
{
    int array[10];
    //Assume sizeof(int*) != sizeof(int[10])
    cout << sizeof(array) << " " << get_size_1(array) << std::endl;
    cout << sizeof(array) << " " << get_size_2(array) << std::endl;
    cout << sizeof(array) << " " << get_size_3(array) << std::endl;
}


/** 
* There is a concept of promoted arithmetic type.
* There is no overloaded operator+ in A struct.
* But there is conversion to bool. Bool doesn't support operator+ so there is further conversion to int.
*/
struct A{
    A(int i) : m_i(i) {}
    operator bool() const { return m_i > 0; }
    int m_i;
};
void common::Types::implicit_conversion_sequence()
{
    A a1(1), a2(2);
    std::cout << a1 + a2 << (a1 == a2); // The result is 1+1=2 and 1==1=True
}

/*
    nullptr is a prvalue of type std::nullptr_t, which is not actually a pointer type.
    Instead, nullptr is a "null pointer constant" which can be converted to a pointer.
*/
void common::Types::nullptr_is_not_pointer()
{
    print(std::is_pointer_v<decltype(nullptr)>);
}

/** 
    We have three cases and the one that applies here is sizeof unary-expression. 
    The unary expression is (0)["abcdefghij"], which looks odd but is just array indexing of string literal which is a const char array.
    We can see that (0)["abcdefghij"] is identical to ("abcdefghij")[0] 
*/
void common::Types::sizeof_trick()
{
    int n = sizeof(0)["abcdefghij"]; // the same as ("absdefghij")[0]
    print(n); // size of the first element of the array or char
}

/** 
* Demonstrates how to use typedef for a function. 
* Func defines function that returns int and doesn't take arguments.
*/
using Func = int();
struct S { Func f; };
int S::f() { return 1; }
void common::Types::typedef_for_function()
{
    S s;
    std::cout << s.f();
}

/**
* char const& b with int a creates temporal object on which b references.
* Therefore b is not the reference for a.
* It is standard point.
*/
void common::Types::ref_with_other_type()
{       
    int a = '0';
    char const& b = a;
    cout << b;
    a++;
    cout << b;
}

/** 
* Each char is converted to int and difference is returned.
*/
void common::Types::char_diff_for_int()
{
    int i = '6' - '2';
    print(i);
}

/**
* The constness of parameters are not part of the function type.
*/
void common::Types::const_of_params_is_not_function_type()
{
    print(std::is_same_v<void(int), void(const int)>);
    print(std::is_same_v<void(int*), void(const int*)>); // They are not ignored for pointers. Why?
}

/** 
* It will show i as two chars will be converted to int.
*/
void f(unsigned int) { std::cout << "u"; }
void f(int) { std::cout << "i"; }
void f(char) { std::cout << "c"; }
void common::Types::char_to_int_implicit()
{
    char x = 1;
    char y = 2;
    f(x + y);
}

/** TYPE CONVERSION IS AMBIGIOUS
* double -> int and double -> unsigned has the same rank so overload is ambigious.
*/
void fs(int) { std::cout << 1; }
void fs(unsigned) { std::cout << 2; }
void common::Types::type_conversion_rang()
{
   //fs(-2.5); // uncomment here
}