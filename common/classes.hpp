#pragma once

namespace common {

	class Classes
	{
	public:
		/** Initialization order of the class. */
		static void initialization_order();

		/** Resolution of virtual and member function. */
		static void virtual_and_member_function_resolution();

		/** Pointers with const. */
		static void pointers_with_const();

		/** Intantiation destruction order. */
		static void instantiation_destruction_order();

		/** Move constructor. */
		static void move_constructor();

		/** Guaranteed copy elision. */
		static void guaranteed_copy_elision_17();

		/** Cast doesn't change virtual function. */
		static void cast_doesnt_change_virtual_function();

		/** Tricky way to create an object. */
		static void tricky_way_to_create_an_object();

		/** Initialization order. */
		static void initialization_order_of_data_members();

		/** Virtual destructor. */
		static void virtual_destructor();

		/** Slicing. */
		static void slicing();

		/** Hidden method in derived class. */
		static void hidden_method_in_derived_class();

		/** When dynamic_cast is appliable. */
		static void dynamic_cast_vs_virtuality();
	};
}