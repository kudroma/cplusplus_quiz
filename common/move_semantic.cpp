#include "move_semantic.h"

#include <iostream>
#include <string>

using namespace std;

template<typename ... T>
void print(T&& ... args) {
	((cout << args << " "), ...);
	cout << endl;
}

namespace {

	struct TestStruct {
		TestStruct() { print("TestStruct ctor"); }
		TestStruct(const TestStruct&) { print("TestStruct copy ctor."); }
		TestStruct(TestStruct&& ) { print("TestStruct move ctor"); }
	};
}

/*
	c is an lvalue char. 
	&c returns a pointer to the lvalue c, but that pointer itself is an rvalue, 
	since it's just a nameless temporary returned from operator&.
	The first overload of f takes an rvalue reference to char *, 
	the second takes an lvalue reference to char*. 
	Since the pointer is an rvalue, the first overload is selected, and 1 is printed.
*/
void f(char*&&) {
	print(1);
}

void f(char*&) { 
	print(2); 
}

/*
* It is imposible to get address of temporal object because it doesn't have address. 
* But adress of object referenced by rvalue ref is possible to take.
* 
*/
int stub() { return 2; }
void common::MoveSemantic::why_rvalues_are_useful()
{
	int&& a = 2;
	*(&a) = 5;   // rvalue can be modified
	print(&a);   // The lifetime of a temporary object extends only until the end of the full expression in which it was created. 
	             // This means that temporary objects are automatically destroyed at the end of the expression.
	
	//int& a = 2; // impossible to assign to not const ref
	//print(&stub()); // it is impossible to take address of temporal object
	const int& b = 2; // But it is ok to create const ref to temp object.
	print(&b);        // b has address but it is not modifiable.
					  // If a temporary object is bound to a const reference, 
					  // its lifetime may be extended to match the lifetime of the reference.
	                  // The same is for rvalue reference.
}





void common::MoveSemantic::rvalue_pointer()
{
	char c = 'a';
	f(&c);  // f(char*&&)

	char* d = &c;
	f(d); // f(char*&)
}





struct Temp
{
	int a = 0;
	Temp() { print("CTOR"); }
	Temp(const Temp&) { print("COPY_CTOR"); }
	Temp(Temp&&) { print("MOVE_CTOR"); }
	~Temp() { print("DTOR"); }
};
void checkMove(Temp&& temp)
{
	print("checkMove(Temp&&)");
	temp.a = 5;
}
void checkMove(const Temp& temp)
{
	print("checkMove(const Temp&)");
}
void test(Temp&& val)
{
	// Despite being declared as an rvalue reference, val itself is an lvalue within the test function scope 
	// because it has a name and can be referenced multiple times.
	print("test(Temp&&)");
	checkMove(val);  // checkMove(const Temp&) will be called because val is lvalue in this function
	checkMove(std::move(val)); // checkMove(Temp&&) is called
}
void common::MoveSemantic::when_rvalue_overload_is_called()
{
	Temp t;
	//test(t); // Compile error. Rvalue reference could not be bound to lvaule.

	test(std::move(t)); // it is ok. We should not use t in this method anymore.
}





void common::MoveSemantic::when_move_ctor_is_called()
{
	Temp val;
	Temp val2 = val; // copy constructor will be called because val is lvalue in this function
	Temp val3 = std::move(val); // Move constructor is called

	Temp&& val11 = Temp(); 
	Temp val21 = val11; // copy ctor is called because val11 is identifier and it is treated as lvalue then.
	Temp val22 = std::move(val11); // it is okey. Move ctor is called.
}

Temp&& generateTemp()
{
	return Temp();
}
Temp generateTemp2()
{
	auto t = Temp();
	return t;
}
void common::MoveSemantic::which_copy_ctor_is_called()
{
	checkMove(generateTemp());   // Move overload is called because it is temporary object
	checkMove(generateTemp2());  // Move overload is called because it is temporary object
	Temp t = generateTemp();     // Move ctor is called because rvalue ref is returned and no identifier.
	Temp t2 = generateTemp2();   // No move ctor is called. Copy ctor or simple ctor (RVO).
	Temp&& t3 = generateTemp();  // No move ctor is called. We simple increase lifetime of temporal object.
	Temp&& t4 = generateTemp2(); // No move ctor is called. We simple increase lifetime of temporal object.
	t3.a = 10; // we could modify this temporal object.
	print(&t2); // it is ok
	print(&t3); // it is also ok as rvalue reference has address 
}







/** const & could accept both lvalue and rvalue. */
void doSmth2(const TestStruct& str) { print("doSmth2(const TestStruct&)"); }
void common::MoveSemantic::const_lvalue_ref_accepts_temporal_objects_too()
{
	TestStruct ob;
	doSmth2(ob);
	doSmth2(TestStruct());
}







/**
* In C++, there are reference collapsing rules that dictate how references are combined when deducing types. 
* These rules ensure that references are correctly handled, and you don't end up with overly complicated reference types:
* When you have an lvalue reference to an lvalue reference or an rvalue reference to an lvalue reference, the result is an lvalue reference.
* When you have an rvalue reference to an rvalue reference, the result is an rvalue reference.
* These rules help preserve reference qualifiers and avoid issues with multiple levels of references.
* 
* If we path A& type to doSmth3, then we call doSmth3(A& ob) because of rule & && = & and T = A&.
* If we path A&& type to doSmth3, then we call doSmth3(A&& ob) because of rule && && = && and T becomes equal to A.
*
*/
void doSmth4(TestStruct& str) { print("doSmth4(TestStruct&)"); }
void doSmth4(TestStruct&& str) { print("doSmth4(TestStruct&&)"); }
template<typename T> void doSmth3(T&& ob) { doSmth4(ob); }
template<typename T> void doSmth3_forward(T&& ob) { doSmth4(std::forward<T>(ob)); }
void common::MoveSemantic::perfect_forwarding()
{
	TestStruct ob;
	TestStruct& ref = ob;
	doSmth3(ref);
	doSmth3(TestStruct()); // TestStruct() is an rvalue (temporary) object, 
						   // but it binds to the lvalue reference parameter of doSmth3 as an lvalue
						   // as it has identifier.
	doSmth3_forward(ref);
	doSmth3_forward(TestStruct());
}






/** 
* If move constructor does not exist as for primitive type then actual copying occurs.
*/
void common::MoveSemantic::move_is_absent_where_no_move_ctor()
{
	int x = 42;
	int y = std::move(x); // Using std::move to convert lvalue 'x' into an rvalue reference
	

	print("x after std::move: ", x); // Outputs 42
	print("y after std::move: ", y); // Outputs 42
	y++;
	print("x after y++: ", x);
	print("y after y++: ", y);
	x++;
	print("x after x++: ", x); // as int doesn't have move constructor then it is actually copied.
	print("y after x++: ", y);
}