#pragma once

namespace common {

	class MemoryModel
	{
	public:

		/** About const reference. */
		static void const_reference();

		/** Address can't be applied to a bitfield. */
		static void address_cant_be_applied_to_bitfield();

		/** Refererence with pointer. */
		static void ref_vs_pointer();

		/** Data race. */
		static void data_race();

		/** Atomic variables. */
		static void atomic_variables_11();

		/** Difference between volatile and atomic. */
		static void volatile_vs_atomic();

		/** Which operations of atomic are atomic. */
		static void atomic_operations();

		/** Lock-free stack. */
		static void lock_free_stack();

		/** Lock-free linked list. */
		static void lock_free_linked_list();

		/** Memory barriers. */
		static void memory_barriers();
		static void spin_lock();

		/** Atomic interface. */
		static void atomic_interface();

		/** Memory alignment. */
		static void memory_alignment();

		/** Class in memory. */
		static void class_in_memory();

		/** Structures. */
		static void structures();

		/** Join and detach. */
		static void join_vs_detach();
	};

}