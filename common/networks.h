#pragma once

/** 1. SUBNET MASK
* https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%81%D0%BA%D0%B0_%D0%BF%D0%BE%D0%B4%D1%81%D0%B5%D1%82%D0%B8
* /28 means that:
*   - 28 bits of the address are network adress;
*   - 4 remaining bits are inside local-subnet;
*   - there are totally 2^4 = 16 addresses.
*/

/** 2. TCP (Transmission Control Protocol)
* 
* 1. TSP 3-WAY HANDSHAKE PROCESS
* https://www.geeksforgeeks.org/tcp-3-way-handshake-process/
* This could also be seen as a way of how TCP connection is established.
*   - ������� ���������� - ��� ������� ����� ����� ������ TCP / IP, ������ ������� ����������, 
*      ����� ��� ���-�������, �� ������� ������� ������������� ���������� � ��������;
*   - � ����������� ������ ���������� ���������� �� ������������ �������;
*   - ��� ������ ��������� ����� ������ - TCP, UDP (User Datagram Protocol); 
*     �� ������� TCP �������� ���������������� (��������� �� ������������ ���������� �������������� ����������).
*   - TCP ������������ �������� ����� � ��� ���������� Positive Acknowledgement with Re-transmission(PAR)
*      �� ������ ���������� �����;
*   - The Protocol Data Unit(PDU) �� ������������ ������ �������� ���������. ����������, ������������ PAR, 
*     ���������� ������� ������, ���� �� ������� �������������. ���� ��� ��������� ������ ����������, 
*     ��� ����������� � ������� checksum functionality �� ������������ ������, ������������ ��� Error Detection), 
*     �� ���������� ��������� �������. ���������� �������� ���������� ������, ���� �� ����� ������� ������������� �����.
*     ����� ������������ (client) � ����������� (receiver) � �������� TCP ����������� ������������ ��� �������� ��� ��������� ����������.
* 
*    STEP 1 (SYN): 
*    In the first step, client wants to establish a connection with server, 
*    so it sends a segment with SYN(Synchronize Sequence Number) which informs server that 
*    client is likely to start communication and with what sequence number it starts segments with.
*    
*    STEP 2 (SYN + ACK): Server responds to the client request with SYN-ACK signal bits set. 
*    Acknowledgement(ACK) signifies the response of segment it received and SYN signifies with what sequence number 
*    it is likely to start the segments with.
* 
*    STEP 3 (ACK): In the final part client acknowledges the response of server and they both establish a reliable connection 
*    with which they will start the actual data transfer.
*    The steps 1, 2 establish the connection parameter (sequence number) for one direction and it is acknowledged. 
*    The steps 2, 3 establish the connection parameter (sequence number) for the other direction and it is acknowledged. 
*    With these, a full-duplex communication is established.
*    Note � Initial sequence numbers are randomly selected while establishing connections between client and server.
*/

/** 3. DNS (DOMAIN NAME SERVER)
* https://en.wikipedia.org/wiki/Domain_Name_System
* The Domain Name System (DNS) is a hierarchical and decentralized naming system for computers, services, 
* or other resources connected to the Internet or a private network. 
* It associates various information with domain names assigned to each of the participating entities.
* The Domain Name System delegates the responsibility of assigning domain names 
* and mapping those names to Internet resources by designating authoritative name servers for each domain. 
* Network administrators may delegate authority over sub-domains of their allocated name space to other name servers.
* 
* DNS primarily uses the User Datagram Protocol (UDP) on port number 53 to serve requests.
* [3] DNS queries consist of a single UDP request from the client followed by a single UDP reply from the server. 
* When the length of the answer exceeds 512 bytes and both client and server support Extension Mechanisms for DNS (EDNS), 
* larger UDP packets are used. Otherwise, the query is sent again using the Transmission Control Protocol (TCP). 
* TCP is also used for tasks such as zone transfers. Some resolver implementations use TCP for all queries.
*/

/** 4. HTTP
* HyperText Transfer Protocol
* https://habr.com/ru/post/215117/#:~:text=HTTP%20%E2%80%94%20%D1%88%D0%B8%D1%80%D0%BE%D0%BA%D0%BE%20%D1%80%D0%B0%D1%81%D0%BF%D1%80%D0%BE%D1%81%D1%82%D1%80%D0%B0%D0%BD%D1%91%D0%BD%D0%BD%D1%8B%D0%B9%20%D0%BF%D1%80%D0%BE%D1%82%D0%BE%D0%BA%D0%BE%D0%BB%20%D0%BF%D0%B5%D1%80%D0%B5%D0%B4%D0%B0%D1%87%D0%B8,%D0%BE%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D1%8C%20%D0%BF%D0%B5%D1%80%D0%B5%D1%85%D0%BE%D0%B4%20%D0%BA%20%D0%B4%D1%80%D1%83%D0%B3%D0%B8%D0%BC%20%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D0%BC).
* 1. ���������� ���������� ��������� ������ � ���������� ��� �� ������, 
* ����� ���� ��������� ����������� ����������� ������������ ������ ������, 
* ��������� ����� � ������� ��� ������� �������.
* 
* 2. ������, ������� ����������� �������� � ������� ��������� HTTP � ����� ������� ����� ���������������� �����������, 
*    �������������� ������ � ���-�������� (������ ��� ���-�������) � ���-��������.
* 
* 3. ��� �������, �������� ������ �� ��������� HTTP �������������� ����� TCP/IP-����������. 
*    ��������� ����������� ����������� ��� ���� ������ ���������� TCP-���� 80.
* 
* 4. ��� ����, ����� ������������ HTTP-������, ���������� ��������� ��������� ������, 
*    � ����� ������ �� ������� ���� ���� ��������� � ��� ��������� Host, ������� �������� ������������, 
*    � ������ �������������� � ������ �������. 
*    ���� � ���, ��� �������������� ��������� ����� � IP-����� �������������� �� ������� �������, �, 
*    ��������������, ����� �� ���������� TCP-����������, �� �������� ������ �� �������� ������� ����������� � ���, 
*    ����� ������ ����� ������������� ��� ����������: ��� ��� ����, ��������, ����� alizar.habrahabr.ru, habrahabr.ru ��� m.habrahabr.ru 
*    � � �� ���� ���� ������� ����� ����� ����������. 
*    ������ ���������� ������� ���������� �� ���� ������� ����������� � ����� 212.24.43.44, 
*    � ���� ���� ������������� ��� �������� ���������� ��� ����� �� ���� IP-�����, 
*    � �����-���� �������� ���, �� ������ �� ���� ����� �� ������������� � 
*    � ������ ������� ���� ����� ���������� �������� � ��������� Host.
*    
*/