#include "rvo.h"

#include "utils.h"

#include <thread>

using namespace std;

namespace {

	void print(const char* str) { std::cout << str << std::endl; }

	struct TestStruct {
		TestStruct() { print("TestStruct constructor."); }
		TestStruct(const TestStruct&) { print("TestStruct copy constructor."); }
		int k{};
	};

	TestStruct withoutRVO() {
		TestStruct res1;
		TestStruct res2;

		const auto now = chrono::high_resolution_clock::now();
		for (auto i = 0; i < 3; ++i) {
			this_thread::sleep_for(100ms);
		}
		const auto end = chrono::high_resolution_clock::now();
		const auto dur = chrono::duration_cast<chrono::microseconds>(end - now).count();
		if (dur % 2) {
			return res1;
		}
		else {
			return res2;
		}
	}

	TestStruct rvo() {
		return TestStruct();
	}

	// For a some reason I have copy ctor call in debug mode but don't have in release mode in visual studio compiler.
	TestStruct nrvo() {
		TestStruct res;

		const auto now = chrono::high_resolution_clock::now();
		for (auto i = 0; i < 3; ++i) {
			this_thread::sleep_for(100ms);
		}
		const auto end = chrono::high_resolution_clock::now();
		const auto dur = chrono::duration_cast<chrono::microseconds>(end - now).count();
		if (dur % 2) {
			return res;
		}
		else {
			res.k = 2;
			return res;
		}
	}
}

void common::RVO::check()
{
	print("\n");
	print( "NO-RVO CALL: there is copy consturctor call." );
	withoutRVO();
	print("\n");
	print("Classical RVO. Memory for the returning object is create in calling function and this memory is filled directly in the called function.");
	rvo();
	print("\n");
	print("NRVO: No copy constructor call.");
	nrvo();
	print("\n");
}