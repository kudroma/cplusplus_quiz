# run in power shell
hostname # name of the host
ipconfig # Info about ip addresses
ipconfig/all # Includes MAC (physical) address too
netstat -a # Enumerates all used ports
nslookup google.com # Make DNS lookup
