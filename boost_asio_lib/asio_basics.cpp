#include "asio_basics.h"

#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/log/trivial.hpp>

#include <ctime>
#include <memory>
#include <thread>

/**
* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/tutorial/tutdaytime1.html
* All programs that use asio need to have at least one boost::asio::io_service object.
* We need to turn the server name that was specified as a parameter to the application, into a TCP endpoint. 
* To do this we use an ip::tcp::resolver object.
* The list of endpoints is returned using an iterator of type ip::tcp::resolver::iterator.
* The list of endpoints obtained above may contain both IPv4 and IPv6 endpoints, 
* so we need to try each of them until we find one that works. This keeps the client program independent of a specific IP version. 
* The boost::asio::connect() function does this for us automatically.
* We use a boost::array to hold the received data. 
* The boost::asio::buffer() function automatically determines the size of the array to help prevent buffer overruns. 
* Instead of a boost::array, we could have used a char [] or std::vector.
* When the server closes the connection, the ip::tcp::socket::read_some() function will exit with the boost::asio::error::eof error, 
* which is how we know to exit the loop.
* Finally, handle any exceptions that may have been thrown.
*/
void asio_basics::Basics::syncTcpDatetimeClient(const string& host)
{
    io_context context;
    ip::tcp::resolver resolver(context);
    ip::tcp::resolver::results_type endpoints = resolver.resolve(host, "daytime");
    ip::tcp::socket sock(context);

    connect(sock, endpoints);

    for (;;) {
        this_thread::sleep_for(1s);
        boost::array<char, 128> buf;
        boost::system::error_code err;
        const auto len = sock.read_some(buffer(buf), err);
        if (err == error::eof) {
            BOOST_LOG_TRIVIAL(debug) << "Connection lost ";
            connect(sock, endpoints);
        }
        else if (err)
            throw boost::system::system_error(err);

        BOOST_LOG_TRIVIAL(debug).write(buf.data(), len);
    }
}

void asio_basics::Basics::simpleAsynchroneousClient(unsigned port)
{
    auto handler = [](const boost::system::error_code& er) {
        if (er.value() == 0)
            cout << "Connected successfully" << endl;
    };
    io_service service;
    ip::tcp::endpoint ep(ip::address::from_string("127.0.0.1"), port);
    ip::tcp::socket sock(service);
    sock.async_connect(ep, handler);
    service.run();
}

/**
* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/tutorial/tutdaytime2.html
* We define the function make_daytime_string() to create the string to be sent back to the client.
* A ip::tcp::acceptor object needs to be created to listen for new connections. 
* It is initialised to listen on TCP port 13, for IP version 4.
* This is an iterative server, which means that it will handle one connection at a time. 
* Create a socket that will represent the connection to the client, and then wait for a connection.
* Finally, handle any exceptions.
*/
void asio_basics::Basics::syncTcpDatetimeServer(unsigned port)
{
    try {
        io_context context;
        ip::tcp::acceptor acc(context, ip::tcp::endpoint(ip::tcp::v4(), port));

        for (;;) {
            ip::tcp::socket sock(context);
            acc.accept(sock);
            BOOST_LOG_TRIVIAL(debug) << "Connection established on port " << port;
            boost::system::error_code err;
            write(sock, buffer(make_datetime_string()), err);
        }
    }
    catch (const exception& e) {
        BOOST_LOG_TRIVIAL(debug) << "Error " << e.what();
    }
}

using socket_ptr = shared_ptr<ip::tcp::socket>;

io_service service;
ip::tcp::acceptor acc(service);

void handle_accept(socket_ptr sock, const boost::system::error_code& err);

void start_accept(socket_ptr sock)
{
    acc.async_accept(*sock, boost::bind(handle_accept, sock, _1));
}

void handle_accept(socket_ptr sock, const boost::system::error_code& err)
{
    if (err) return;
    // at this point, you can read/write to the socket
    socket_ptr sock1(new ip::tcp::socket(service));
    start_accept(sock1);
}

void asio_basics::Basics::simpleAsynchroneousServer(unsigned port)
{
    io_service service;
    ip::tcp::endpoint ep(ip::tcp::v4(), port);
    ip::tcp::acceptor acc(service, ep);
    socket_ptr sock(new ip::tcp::socket(service));
    start_accept(sock);
    service.run();
}

string asio_basics::Basics::make_datetime_string()
{
    time_t now = time(0);
    return ctime(&now);
}

asio_basics::tcp_connection::ptr asio_basics::tcp_connection::create(io_context& context)
{
    BOOST_LOG_TRIVIAL(debug) << __FUNCTION__;
    return std::make_shared<tcp_connection>(context);
}

void asio_basics::tcp_connection::start()
{
    BOOST_LOG_TRIVIAL(debug) << __FUNCTION__;
    message_ = Basics::make_datetime_string();
    async_write(socket_, buffer(message_), boost::bind(&tcp_connection::handle_write, shared_from_this(),
        boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void asio_basics::tcp_connection::handle_write(const boost::system::error_code& err, std::size_t n)
{
    BOOST_LOG_TRIVIAL(debug) << __FUNCTION__ << " " << n << " bytes";
}

void asio_basics::tcp_server::start_accept()
{
    BOOST_LOG_TRIVIAL(debug) << __FUNCTION__;
    tcp_connection::ptr conn = tcp_connection::create(static_cast<io_context&>(acceptor_.get_executor().context()));
    acceptor_.async_accept(conn->socket(), boost::bind(&tcp_server::handle_accept, this, conn, boost::asio::placeholders::error));
}

void asio_basics::tcp_server::handle_accept(tcp_connection::ptr conn, const boost::system::error_code& err)
{
    BOOST_LOG_TRIVIAL(debug) << __FUNCTION__;
    if (!err) conn->start();
    start_accept(); // start accept again after assync write handler in order to continue accept.
}

void asio_strand::check()
{
    {
        BOOST_LOG_TRIVIAL(debug) << "WITHOUT STRAND";
        atomic_int val{ 0 };
        vector<thread> ths;
        for (auto i = 0; i < 50; ++i) {
            ths.emplace_back([&, ind = i]() {BOOST_LOG_TRIVIAL(debug) << ind << " " << val++; });
        }
        for (auto&& t : ths)
            t.join();
    }
    BOOST_LOG_TRIVIAL(debug) << "";
    {
        BOOST_LOG_TRIVIAL(debug) << "WITH STRAND";
        boost::asio::io_service io_service;
        boost::asio::io_service::strand strand(io_service);
        atomic_int val{ 0 };
        vector<thread> ths;
        for (auto i = 0; i < 50; ++i) {
            strand.post([&, ind = i]() {BOOST_LOG_TRIVIAL(debug) << ind << " " << val++; });
        }
        io_service.run();
    }
}

void asio_buffers::check()
{
}

std::pair<asio_line_based_operations::iterator, bool> asio_line_based_operations::http_connection::match_whitespace(iterator begin, iterator end)
{
    iterator i = begin;
    while (i != end)
        if (std::isspace(*i++))
            return std::make_pair(i, true);
    return std::make_pair(i, false);
}

void asio_line_based_operations::http_connection::start()
{

    // ... There should be the code.

    /**
    * The delimiters may be specified as a single char, a std::string or a boost::regex.
    * he read_until() and async_read_until() functions also include overloads
    * that accept a user-defined function object called a match condition
    */
    // boost::asio::async_read_until(socket_, data_, "\r\n", boost::bind(&http_connection::handle_request_line, this, _1));
    boost::asio::async_read_until(socket_, data_, &http_connection::match_whitespace, boost::bind(&http_connection::handle_request_line, this, _1));

}

void asio_line_based_operations::http_connection::handle_request_line(boost::system::error_code ec)
{
    if (!ec)
    {
        std::string method, uri, version;
        char sp1, sp2, cr, lf;
        std::istream is(&data_);
        is.unsetf(std::ios_base::skipws);
        is >> method >> sp1 >> uri >> sp2 >> version >> cr >> lf;
    }
}

/*
asio_stackless_coroutines::session::session(boost::shared_ptr<tcp::socket> socket) 
    : socket_(socket), buffer_(new std::vector<char>(1024))
{
}
*/

/*
void asio_stackful_coroutines::do_echo(boost::asio::yield_context yield)
{
    
    try
    {
        char data[128];
        for (;;)
        {
            std::size_t length =
                my_socket.async_read_some(
                    boost::asio::buffer(data), yield);

            boost::asio::async_write(my_socket,
                boost::asio::buffer(data, length), yield);
        }
    }
    catch (std::exception& e)
    {
        // ...
    }
    
}
*/

/**
* The first argument to spawn() may be a strand, io_context, or completion handler. 
* This argument determines the context in which the coroutine is permitted to execute. 
* For example, a server's per-client object may consist of multiple coroutines; 
* they should all run on the same strand so that no explicit synchronisation is required.
*/
/*
void asio_stackful_coroutines::check() {
    boost::asio::spawn(my_strand, do_echo);
}
*/