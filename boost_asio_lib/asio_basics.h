#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <iostream>

using namespace boost::asio;
using namespace std;

/** 1. ASIO OVERVIEW 
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview.html
* 
* RATIONALE
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/rationale.html
* Most programs interact with the outside world in some way, whether it be via a file, a network, 
* a serial cable, or the console. Sometimes, as is the case with networking, 
* individual I/O operations can take a long time to complete. 
* This poses particular challenges to application development.
* 
* Boost.Asio provides the tools to manage these long running operations, 
* without requiring programs to use concurrency models based on threads and explicit locking.
* 
* GOALS
* The Boost.Asio library is intended for programmers using C++ for systems programming, 
* where access to operating system functionality such as networking is often required. 
* In particular, Boost.Asio addresses the following goals:
* The Boost.Asio library is intended for programmers using C++ for systems programming, 
* where access to operating system functionality such as networking is often required. 
* In particular, Boost.Asio addresses the following goals:
*  - Portability. The library should support a range of commonly used operating systems, and provide consistent behaviour across these operating systems.
*  - Scalability. The library should facilitate the development of network applications that scale to thousands of concurrent connections. 
*    The library implementation for each operating system should use the mechanism that best enables this scalability.
*  - Efficiency. The library should support techniques such as scatter-gather I/O, and allow programs to minimise data copying.
*
* ANATOMY
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/basics.html
* Perform both synchronous and asynchronous operations on I/O objects such as sockets.
* 
* CONTEXT
* Your program will have at least one I/O execution context, 
* such as an boost::asio::io_context object, boost::asio::thread_pool object, or boost::asio::system_context. 
* This I/O execution context represents your program's link to the operating system's I/O services.
* 
* IO OBJECT
* To perform I/O operations your program will need an I/O object such as a TCP socket.
* 
* SYNC SEQUENCE
* 1) Your program initiates the connect operation by calling the I/O object: socket.connect(server_endpoint, err)
* 2) The I/O object forwards the request to the I/O execution context.
* 3) The I/O execution context calls on the operating system to perform the connect operation.
* 4) The operating system returns the result of the operation to the I/O execution context.
* 5) The I/O execution context translates any error resulting from the operation into an object of type boost::system::error_code. 
*    An error_code may be compared with specific values, or tested as a boolean (where a false result means that no error occurred). 
*    The result is then forwarded back up to the I/O object.
* 6) The I/O object throws an exception of type boost::system::system_error if the operation failed.
* 
* ASYNC SEQUENCE
* 1) Your program initiates the connect operation by calling the I/O object: 
*     socket.async_connect(server_endpoint, your_completion_handler);
* 2) The I/O object forwards the request to the I/O execution context.
* 3) The I/O execution context signals to the operating system that it should start an asynchronous connect.
* ... Time passes. (In the synchronous case this wait would have been contained entirely within the duration of the connect operation.)
* 4) The operating system indicates that the connect operation has completed by placing the result on a queue, ready to be picked up by the I/O execution context.
* 5) When using an io_context as the I/O execution context, your program must make a call to io_context::run() 
*    (or to one of the similar io_context member functions) in order for the result to be retrieved. 
*    A call to io_context::run() blocks while there are unfinished asynchronous operations, 
*    so you would typically call it as soon as you have started your first asynchronous operation.
* 6) While inside the call to io_context::run(), the I/O execution context dequeues the result of the operation, 
*    translates it into an error_code, and then passes it to your completion handler.
* 
* PROACTOR DESIGN PATTERN: CONCURENCY WITHOUT THREADS
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/async.html
* https://en.wikipedia.org/wiki/Proactor_pattern
* https://en.wikipedia.org/wiki/Reactor_pattern (intbu)
* The asynchronous support is based on the Proactor design pattern.
*   - Portability. 
*     Many operating systems offer a native asynchronous I/O API (such as overlapped I/O on Windows) as the preferred option for developing high performance network applications.
*   
*   - Decoupling threading from concurrency. 
*     Long-duration operations are performed asynchronously by the implementation on behalf of the application. Consequently applications do not need to spawn many threads in order to increase concurrency.
* 
*   - Performance and scalability.
*     Implementation strategies such as thread-per-connection (which a synchronous-only approach would require) can degrade system performance, 
*     due to increased context switching, synchronisation and data movement among CPUs. 
*     With asynchronous operations it is possible to avoid the cost of context switching by minimising the number of operating system threads 
*     - typically a limited resource � and only activating the logical threads of control that have events to process.
* 
*   - Simplified application synchronisation.
*     Asynchronous operation completion handlers can be written as though they exist in a single-threaded environment, and so application logic can be developed with little or no concern for synchronisation issues.
* 
*   - Function composition (intbu)
*     For example, consider a protocol where each message consists of a fixed-length header followed by a variable length body, 
*     where the length of the body is specified in the header. 
*     A hypothetical read_message operation could be implemented using two lower-level reads, 
*     the first to receive the header and, once the length is known, the second to receive the body.
*     To compose functions in an asynchronous model, asynchronous operations can be chained together. 
*     That is, a completion handler for one operation can initiate the next. 
*     Starting the first call in the chain can be encapsulated so that the caller need not be aware 
*     that the higher-level operation is implemented as a chain of asynchronous operations.
* 
*   DISADVANTAGES
*   - It is more difficult to develop applications using asynchronous mechanisms due to the separation in time and space 
*     between operation initiation and completion. 
*     Applications may also be harder to debug due to the inverted flow of control.
* 
*   - Buffer space must be committed for the duration of a read or write operation, which may continue indefinitely, 
*     and a separate buffer is required for each concurrent operation. 
*     The Reactor pattern, on the other hand, does not require buffer space until a socket is ready for reading or writing.
* 
* MULTITHREADING
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/threads.html
*		THREAD SAFETY. 
*			Most of the classes are not thread-safe. But some (io_context) are thread safe.
*		THREAD POOL. 
*			Multiple threads may call io_context::run() to set up a pool of threads from which completion handlers may be invoked. 
*			This approach may also be used with post() as a means to perform arbitrary computational tasks across a thread pool.
*           Note that all threads that have joined an io_context's pool are considered equivalent, 
*			and the io_context may distribute work across them in an arbitrary fashion.
*       INTERNAL THREADS.
*		    The implementation of this library for a particular platform may make use of one or more internal threads to emulate asynchronicity. 
*           As far as possible, these threads must be invisible to the library user.
*		COMPLETION HANDLERS CONTROLLING.
*			Asynchronous completion handlers will only be called from threads that are currently calling io_context::run().
*			Consequently, it is the library user's responsibility to create and manage all threads to which the notifications will be delivered.
* 
*/

/** 2. ASIO USAGE
* https://habr.com/ru/post/192284/
* 1. PROTOCOL SUPPORT
* ��������������� API ��� ������ � TCP (Transmission Control Protocol) ��������, 
* UDP (User Datagram Protocol) ��������, IMCP(Internet Control Message Protocol) ��������, 
* ��� �� ���������� �������� �����������, 
* ���, ���� �� ������, �� ������ ������������ �� �� ���� ����������� ��������.
* 
* 2. SYNCHRONEOUS AND ASYNCHRONEOUS APIs
* � ���������� ���������������� ��� �������� �� ������� � ���������������� �������, 
* ����� ��� ������ (������) �� ������ S, � ����� �������� (�����) � �����. ������ �� �������� �������� �����������. 
* ��� ��� �������� �����������, �� ����� �� ��������� �������� ���������, � �� ����� ��� �� ������� ��� ����������� � �����, 
* �� ������ �������� ���� ��� ��������� �������, 
* ������� ����� ���� � �������� �����/������. ����� �������, ���������� ������/�������, ��� �������, ������������.
* 
* � ������ �������, ����������� ��������� ����������� ���������. 
* �� ��������� ��������, �� �� �� ������, ����� ��� ����������; 
* �� �������������� callback �������, ������� ����� ���������� API � ����������� ��������, ����� �������� ����������.
* 
* 3. IO_SERVICE
*  - Boost.Asio ���������� io_service ��� ������� � �������� �����/������ ������������ �������.
*  - �������� ��������, ��� ���� service.run() ����� ����������� �� ��� ��� ���� ������� ������������� ����������� ��������.
*  - 
*/
namespace asio_basics{
	class Basics {
	public:
		static void syncTcpDatetimeClient(const string& host);
		static void simpleAsynchroneousClient(unsigned port);

		static void syncTcpDatetimeServer(unsigned port);
		static void simpleAsynchroneousServer(unsigned port);

		static string make_datetime_string();
	};

	/**
	* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/tutorial/tutdaytime3.html
	*/
	class tcp_connection : public std::enable_shared_from_this<tcp_connection> {
	public:
		using ptr = shared_ptr<tcp_connection>;
		tcp_connection(io_context& context) : socket_(context) {}
		static ptr create(io_context& context);
		ip::tcp::socket& socket() { return socket_; }
		void start();

	private:
		void handle_write(const boost::system::error_code& err, std::size_t n);

		ip::tcp::socket socket_;
		string message_;
	};

	/**
	* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/tutorial/tutdaytime3.html
	*/
	class tcp_server {
	public:
		tcp_server(io_context& context, unsigned port) : acceptor_(context, ip::tcp::endpoint(ip::tcp::v4(), port)) {
			start_accept();
		}
	private:
		void start_accept();
		void handle_accept(tcp_connection::ptr conn, const boost::system::error_code& err);

		ip::tcp::acceptor acceptor_;
	};
}

/** 3. ASYNC OPERATIONS
* SYNC vs ASYNC OPERATIONS
* https://habr.com/ru/post/453192/
* https://habr.com/ru/company/jugru/blog/446562/
*    
*    In sync operations next runs only when previous have finished.
*    In async there is point to go to the parallel operation (in the same or other process) and returning back.
* 
*    Read/write operations (for sockets too) take enough long time because uses I/O service.
*    There is packet exchange over network and other low-level stuff.
*    
*    Could we do some stuff during read/write operation in order to use processor power for other work.
* 
*    Main purpose: when we have several execution threads and one of them waits for we can use it for some task.
*  
*    Multitasking: when do multiple tasks in the context of single thread of execution. This is a property of program.
* 
*    Parallelism: when several operations can be done during one time unit. It is a property of hardware (environment).
* 
*    Asynchroneous code - is to write multitasking systems.
*    
*    Synchroneous code can be blocked while waiting for some event. It can wait I/O operations, other process or timer.
*    Event tracking and thread planning is OS responsibility.
*    Execution thread is connected with one task.
* 
*    Switching context from app to the OS kernel costs microseconds (2e4 - 1e5 operations per second on server if system time is 10 %).
* 
*    select/epoll and lebuv libraries use asynchroneous approach.
* 
*    In async approach application is responsible for definition of moment of time where events are tracked.
*    Explicit calls select/epoll are points where we ask OS information about events.
*    Also app is responsible on choice what task to do next.
*    Notify mechanism about events: pull ~ queues, push ~ interruptions
*    
*    MULTITASKING MECHANISMS
*    POLL method:
*    We got multiple events that we expect and then we react on them. It allows to process many events and provide high network bandwidth.
*    PUSH method:
*    When we say to the execution thread: "Now please handle this event!". Exapmle is callback. We use this method when want to decrease reaction period on event.
* 
*    C++ CODE
*    If we control multitasking in the code and not switch context to the kernel it allows to accelerate the program.
*    Such code is much more complicated because different fragments are distributed across the system.
*    ����� ������������ ���������� ������, ����� ���������������� ������ ��������� ������ �������.
*    ����� �������, �������� � ���� ��� ��������������� (������ ������ ������� ����� � �� �������������������), 
*    �� �������� ����������� ��� � �������� ��� �������������� �����, �� ����� ������ ������ � ����������. 
*    ���� ��� ������ ������ � ������ ������������.
*    It is convinient to think about async code in terms of state machine.
*    � 1988 ���� ����, ������������ ��������������� ���������, ������� ������ � ���� ������, �������� ��������, 
*    � ������ � ������������� ��������������� ���������. � ���� ���������������� ����� �������� �����������, 
*    ����������� �������� ������������ ����� ����� ��������� � � ������� ������� ������� � � �������������� ������� � �������.
*    future, promises.
*    
*    FUTURE and PROMISE
*    Future - interface for reading.
*    Promise - interface for writing.
*    I don't know why but article recommends to use boost::future instead of std::future.
*    ��������, � ���� ������ ���� �������� � future --, 
*    ��� � ������� �������� �����-�� �������� t � �� ��������� ��� ����� ������ ������ ���������. 
*    � ���� ��� ������, ���, ����� �������� ��������, �� � ���� ��������� � ���� ������� f, ������� ���-�� ������� � ������ �������� r.
*    � ���� �������������� ��� �������� � ������� �������� �������� t � ������� ��������� ������������� f. 
*    ����� � ������ �������� � ��������� ������� �������, � ���, ��� � ������� �������� �������� r.
*    �������� ����� ��������: ����� ����� �������� t, �� � ������� � ���� �������, 
*    ������ �������� r � ������ � ������� �������������� ���������.
*    
*    ���� �������� �������� �� ������, �� ����� ��������, ��� � ���������� �������� � ������ ������������ �� ��������� ��������, 
*    �� ����� �� ������� �������-�����������. ����� ����������� ����������, ����� ����������, ����� �������� �������-�����������, 
*    ���������� � Subscribe. ����� ����� ��� ��� ������� � ����, � ������� �� �������, ��� ����������� �������, 
*    ������������� �������-����������� �� ����������. 
*    ��� ����� ���������, � ��� ����������� ���������.
*    
*    Any combinator: if any of futures is available set promise.
*    All combinator: set promise only if all futures are available.
*    It is easy use promise-future with old callback-driven libraries.
*    Coroutines are usefull for futures.
*    - future allows to write async code in sync manner;
*    - points of result waiting are explicitely marked;
*    - we can select waiting strategy: block (LVL0) or not-blocking (LVL1) using coroutines.
*    
*    COROUTINES
*        Functions with multiple points of entering/exiting.
*        When we go to the upper context from the function we define what to do. boost::asio, boost::fiber provide scheduler for this.
*        
*        Context switching mechanisms
*			���� ������ �����������, ��������, boost::context, ������� � �������� �������� � ���������� ������ ����: 
*           ���� ���������, ���������� �� ��������; ���� �������, ������� ��������� ���� �������� ��� ���������� �������� ��������� 
*           � ���� �������� ��� �������� ����������. � ���������� ��� x86/64 �������� ���������� ���������� ��� �������� �� ����, 
*			��������� � ��������� ������� ��������� �� ����, ����� �������� ��������� �� ���� ����� � ��������������� ��������.
*        
*        https://habr.com/ru/company/jugru/blog/446562/
*			It is a perfect article demonstrating coroutine essentials.
*			Main magic of coroutines is in switching context low-level function that allow switch execution inside stack.
*           By saving and copying registers.
*    
* 
* 
* 
* 
*/
namespace multitasking {
}

/**
* 4. STRANDS
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/strands.html
*	A strand is defined as a strictly sequential invocation of event handlers(i.e.no concurrent invocation).
*	Use of strands allows execution of code in a multithreaded program without the need for explicit locking
*	(e.g. using mutexes).
*
* EXPLICIT STRANDS.
*	An explicit strand is an instance of strand<> or io_context::strand.
*	All event handler function objects need to be bound to the strand using boost::asio::bind_executor()
*	or otherwise posted / dispatched through the strand object.
*
* IMPLICIT STRANDS.
*	 Calling io_context::run() from only one thread.
*	 Where there is a single chain of asynchronous operations associated with a connection.
*
* BIND_EXECUTOR
*	 The boost::asio::bind_executor() function is a helper to bind a specific executor object,
*	 such as a strand, to a completion handler.This binding automatically associates an executor as shown above.
*	 
* https://stackoverflow.com/questions/25363977/what-is-the-advantage-of-strand-in-boost-asio
* 
* Think of a system where a single io_service manages sockets for hundreds of network connections. 
* To be able to parallelize workload, the system maintains a pool of worker threads that call io_service::run.
* Now most of the operations in such a system can just run in parallel. But some will have to be serialized. 
* For example, you probably would not want multiple write operations on the same socket to happen concurrently. 
* You would then use one strand per socket to synchronize writes: 
* Writes on distinct sockets can still happen at the same time, while writes to same sockets will be serialized. 
* The worker threads do not have to care about synchronization or different sockets, 
* they just grab whatever io_service::run hands them.
* One might ask: Why can't we just use mutex instead for synchronization? 
* The advantage of strand is that a worker thread will not get scheduled in the first place 
* if the strand is already being worked on. With a mutex, the worker thread would get the callback 
* and then would block on the lock attempt, preventing the thread from doing any useful work 
* until the mutex becomes available.
* 
*/
namespace asio_strand {
	void check();
}

/**
* 5. BUFFERS
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/buffers.html
* 
* SCATTER-GATHER
*	Fundamentally, I/O involves the transfer of data to and from contiguous regions of memory, called buffers. 
*   These buffers can be simply expressed as a tuple consisting of a pointer and a size in bytes. 
*   However, to allow the development of efficient network applications, Boost.Asio includes support for scatter-gather operations. 
*   These operations involve one or more buffers:
*   A scatter-read receives data into multiple buffers.
*   A gather-write transmits multiple buffers.
* 
*   Therefore we require an abstraction to represent a collection of buffers. 
*   The approach used in Boost.Asio is to define a type (actually two types) to represent a single buffer. 
*   These can be stored in a container, which may be passed to the scatter-gather operations.
* 
* MODIFIABLE vs NOT-MODIFIABLE
*	Boost.Asio makes a distinction between modifiable memory (called mutable) and non-modifiable memory.
*   mutable_buffer and const_buffer. 
*   The goal of these is to provide an opaque representation of contiguous memory.
*   Finally, multiple buffers can be passed to scatter-gather operations 
*   (such as read() or write()) by putting the buffer objects into a container. 
* 
* BUFFERS_ITERATOR
*	The buffers_iterator<> class template allows buffer sequences 
*   (i.e. types meeting MutableBufferSequence or ConstBufferSequence requirements) 
*   to be traversed as though they were a contiguous sequence of bytes.
* 
* ITERATOR VALIDITY DEBUGGING
*	Some standard library implementations, such as the one that ships with Microsoft Visual C++ 8.0 and later, 
*   provide a feature called iterator debugging. What this means is that the validity of iterators is checked at runtime. 
*   If a program tries to use an iterator that has been invalidated, an assertion will be triggered.
*   When you call an asynchronous read or write you need to ensure that the buffers for the operation are valid 
*   until the completion handler is called.
*   When buffer debugging is enabled, Boost.Asio stores an iterator into the string until the asynchronous operation completes, 
*   and then dereferences it to check its validity. 
*   In the above example you would observe an assertion failure just before Boost.Asio tries to call the completion handler.
*	BOOST_ASIO_ENABLE_BUFFER_DEBUGGING is used to enable this feature.
* 
*/
namespace asio_buffers {
	void check();
}

/** 
* 6. STREAMS, SHORT READS and SHORT WRITES
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/streams.html
* 
*	Many I/O objects in Boost.Asio are stream-oriented. This means that:
*		- There are no message boundaries. The data being transferred is a continuous sequence of bytes.
*       - Read or write operations may transfer fewer bytes than requested. This is referred to as a short read or short write.
*   
*    SyncReadStream, AsyncReadStream, SyncWriteStream, AsyncWriteStream.
*    
*    Programs typically want to transfer an exact number of bytes. 
*    When a short read or short write occurs the program must restart the operation, 
*    and continue to do so until the required number of bytes has been transferred. 
*    Boost.Asio provides generic functions that do this automatically: read(), async_read(), write() and async_write().
* 
*    Why EOF is an Error (intbu)
*		The end of a stream can cause read, async_read, read_until or async_read_until functions to violate their contract. E.g. a read of N bytes may finish early due to EOF.
*		An EOF error may be used to distinguish the end of a stream from a successful read of size 0.
*/
namespace asio_streams {

}

/**
* 7. REACTOR-STYLE OPERATIONS
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/reactor.html
* 
*	Sometimes a program must be integrated with a third-party library that wants to perform the I/O operations itself. 
*	To facilitate this, Boost.Asio includes synchronous and asynchronous operations 
*	that may be used to wait for a socket to become ready to read, ready to write, or to have a pending error condition.
* 
*/
namespace asio_reactor_operations {

}

/**
* 8. LINE BASED OPERATIONS
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/line_based.html
* 
*	Many commonly-used internet protocols are line-based, which means 
*	that they have protocol elements that are delimited by the character sequence "\r\n". 
*	Examples include HTTP, SMTP and FTP.
* 
*/
namespace asio_line_based_operations {

	using iterator = boost::asio::buffers_iterator<boost::asio::streambuf::const_buffers_type>;

	struct http_connection
	{
		static std::pair<iterator, bool> match_whitespace(iterator begin, iterator end);

		void start();

		void handle_request_line(boost::system::error_code ec);

		boost::asio::ip::tcp::socket socket_;

		/** 
		* The streambuf data member serves as a place to store the data 
		* that has been read from the socket before it is searched for the delimiter. 
		* 
		* It is important to remember that there may be additional data after the delimiter. 
		* This surplus data should be left in the streambuf so that it may be inspected by a subsequent call to read_until() 
		* or async_read_until().
		*/
		boost::asio::streambuf data_;
	};
}

/** 
* 9. CUSTOM MEMORY ALLOCATION
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/allocation.html
* 
*	Many asynchronous operations need to allocate an object to store state associated with the operation.
*   Furthermore, programs typically contain easily identifiable chains of asynchronous operations.
* 
*   Given a copy of a user-defined Handler object h, if the implementation needs to allocate memory associated with 
*   that handler it will obtain an allocator using the get_associated_allocator function.
*   In more complex cases, the associated_allocator template may be partially specialised directly.
*   The implementation guarantees that the deallocation will occur before the associated handler is invoked, 
*   which means the memory is ready to be reused for any new asynchronous operations started by the handler.
*/

/**
* 10. HANDLER TRACKING
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/handler_tracking.html
* 
* When enabled by defining BOOST_ASIO_ENABLE_HANDLER_TRACKING, Boost.Asio writes debugging output to the standard error stream.
* Programs may augment the handler tracking output's location information by using the macro BOOST_ASIO_HANDLER_LOCATION in the source code.
* 
*/

/**
* 11. CONCURENCY HINT
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/concurrency_hint.html
*	The io_context constructor allows programs to specify a concurrency hint. 
*	This is a suggestion to the io_context implementation as to the number of active threads 
*	that should be used for running completion handlers.
* 
*	- BOOST_ASIO_CONCURRENCY_HINT_SAFE,
*   - BOOST_ASIO_CONCURRENCY_HINT_UNSAFE_IO,
*   - BOOST_ASIO_CONCURRENCY_HINT_UNSAFE,
*   - 1.
* 
*/

/**
* 12. STACKLESS COROUTINES
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/coroutine.html
*	
*	The coroutine class provides support for stackless coroutines. 
*   Stackless coroutines enable programs to implement asynchronous logic in a synchronous manner, with minimal overhead.
* 
*   The coroutine class is used in conjunction with the pseudo-keywords reenter, yield and fork. 
*   These are preprocessor macros, and are implemented in terms of a switch statement using a technique similar to Duff's Device.
*/
namespace asio_stackless_coroutines {
	using namespace boost::asio::ip;

	struct session : boost::asio::coroutine
	{
		boost::shared_ptr<tcp::socket> socket_;
		boost::shared_ptr<std::vector<char> > buffer_;

		session(boost::shared_ptr<tcp::socket> socket);
		void operator()(boost::system::error_code ec = boost::system::error_code(), std::size_t n = 0);
	};
}

/**
* 13. STACKFUL COROUTINES
* https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio/overview/core/spawn.html
* 
*	The spawn() function is a high-level wrapper for running stackful coroutines. It is based on the Boost.Coroutine library. 
*   The spawn() function enables programs to implement asynchronous logic in a synchronous manner.
*/
namespace asio_stackful_coroutines {

	//boost::asio::io_service io_service;
	//boost::asio::ip::tcp::socket my_socket(io_service);
	//boost::asio::io_service::strand my_strand(io_service);

	//void do_echo(boost::asio::yield_context yield);
	//void check();
}

/**
* 14. TIMERS
* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/overview/timers.html
* 
*	Long running I/O operations will often have a deadline by which they must have completed. 
*	These deadlines may be expressed as absolute times, but are often calculated relative to the current time.
*   boost::deadline_timer is used for deadline timers in order to wait for completion handlers and continue.
*/

/**
* 15. SERIAL PORTS
* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/overview/serial_ports.html
* 
*	Boost.Asio includes classes for creating and manipulating serial ports in a portable manner.
* 
*	serial_port port(my_io_context, name);
* 
*   where name is something like "COM1" on Windows, and "/dev/ttyS0" on POSIX platforms.
*   Once opened, the serial port may be used as a stream.
*/

/**
* 16. SIGNAL HANDLING
* https://www.boost.org/doc/libs/1_66_0/doc/html/boost_asio/overview/signals.html
* 
*	Boost.Asio supports signal handling using a class called signal_set. 
*	Programs may add one or more signals to the set, and then perform an async_wait() operation. 
*	The specified handler will be called when one of the signals occurs.
* 
*/