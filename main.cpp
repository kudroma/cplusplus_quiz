#include "move_semantic.h"
#include "classes.hpp"
#include "containers.hpp"
#include "lambdas.hpp"
#include "misc.hpp"
#include "smartpointers.hpp"
#include "template.hpp"

#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

string solution(string a, string b) {
    const size_t a_len = a.size();
    const size_t b_len = b.size();
    const string& max_s = (a_len > b_len ? a : b);
    const size_t min_len = std::min(a.size(), b.size());
    const size_t max_len = std::max(a.size(), b.size());
    const size_t res_len = max_len + 1;
    std::string res;
    res.resize(res_len);
    int diff = 0;
    for (auto i = 0; i < min_len; ++i)
    {
        if (diff >= 2)
            diff = 1;
        diff += a[a_len - i - 1] - '0' + b[b_len - i - 1] - '0';
        if (diff == 2)
            res[res_len - i - 1] = '0';
        else if (diff == 1)
            res[res_len - i - 1] = '1';
        else
            res[res_len - i - 1] = '0';
        cout << res << endl;
    }

    for (auto i = min_len; i < max_len; ++i)
    {
        cout << i << " " << diff << endl;
        if (diff < 2)
        {
            res[res_len - i - 1] = max_s[max_len - i - 1];
            continue;
        }
        else {
            diff = 1;
            diff += max_s[max_len - i - 1] - '0';
            if (diff == 2)
                res[res_len - i - 1] = '0';
            else if (diff == 1)
                res[res_len - i - 1] = '1';
            else
                res[res_len - i - 1] = '0';
        }
    }

    if (diff == 2)
    {
        res[0] = '1';
        return res;
    }
    else
    {
        return res.substr(1);
    }
}

int main(int argc, char** argv) 
{
    common::Template::sfinae_concept_20();
	return 0;
}