#include "asio_basics.h"

#include <boost/log/trivial.hpp>
#include <boost/locale.hpp>

// CLIENT

int main(int argc, char** argv)
{
    SetThreadUILanguage(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)); // make error code in english

    try
    {
        if (argc != 2)
        {
            std::cerr << "Usage: boost_asio_client <host>" << std::endl;
            return 1;
        }
        asio_basics::Basics::syncTcpDatetimeClient(argv[1]);
    }
    catch (std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Exception " << e.what() << std::endl;
        return 1;
    }
    return 0;
}