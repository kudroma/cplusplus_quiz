#include "asio_basics.h"

// SERVER

int main(int argc, char** argv)
{
    SetThreadUILanguage(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)); // make error code in english

    try
    {
        if (argc != 2)
        {
            std::cerr << "Usage: boost_asio_server <port>" << std::endl;
            return 1;
        }
        io_context context;
        asio_basics::tcp_server server(context, atoi(argv[1]));
        context.run();
        
        // Start synchroneous iterative server.
        //asio_basics::Basics::syncTcpDatetimeServer(atoi(argv[1]));
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}