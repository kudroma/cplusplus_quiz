cmake_minimum_required(VERSION 3.10)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

file(GLOB SOURCES "*.cpp")
file(GLOB HEADERS "*.h")

set(BUILD_DIR ../build)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BUILD_DIR})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${BUILD_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${BUILD_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BUILD_DIR}/bin)
set(LIBRARY_OUTPUT_DIRECTORY_DEBUG ${BUILD_DIR}/bin)

add_executable(boost_asio_server ${HEADERS} ${SOURCES})

# Check if there are all necessary external dependencies
# Boost includes
if(DEFINED ENV{BOOST_DIR_INCLUDE})
	list(APPEND EXTRA_INCLUDES $ENV{BOOST_DIR_INCLUDE})
else()
    message(FATAL_ERROR "No BOOST_DIR_INCLUDE environment variable!")
endif()

# Boost libs
if(DEFINED ENV{BOOST_DIR_LIB})
	file(GLOB BOOST_DEBUG_LIBS "$ENV{BOOST_DIR_LIB}/*gd*")
	file(GLOB BOOST_RELEASE_LIBS "$ENV{BOOST_DIR_LIB}/*mt-x64*")
else()
    message(FATAL_ERROR "No BOOST_DIR_LIB environment variable!")
endif()

list(APPEND EXTRA_LIBS boost_asio_lib)
list(APPEND EXTRA_INCLUDES "${CMAKE_SOURCE_DIR}/boost_asio_lib")

# Add linked libraries
target_link_libraries(boost_asio_server PUBLIC 
		          "${EXTRA_LIBS}"
		debug     "${BOOST_DEBUG_LIBS}"
		optimized "${BOOST_RELEASE_LIBS}"
)

# Add target include directories
target_include_directories(boost_asio_server PUBLIC 
		"${CMAKE_BINARY_DIR}"
		"${EXTRA_INCLUDES}"
)
